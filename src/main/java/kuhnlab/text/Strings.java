package kuhnlab.text;

/*
 * Strings.java
 *
 * Created on April 28, 2005, 10:37 PM
 */



/**
 *
 * @author drjrkuhn
 */
public class Strings {
    
    /** HELPER: create a string and fill it with replicas of a single character. */
    public static String String(char c, int length) {
        if (length < 1) return "";
        char cc[] = new char[length];
        java.util.Arrays.fill(cc, c);
        return new String(cc);
    }
}
