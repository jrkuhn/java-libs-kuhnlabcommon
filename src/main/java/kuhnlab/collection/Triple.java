/*
 * Pair.java
 *
 * Created on April 28, 2005, 5:40 PM
 */

package kuhnlab.collection;

/** Class to contain three objects
 *
 * @author drjrkuhn
 */

public class Triple <LEFT, CENTER, RIGHT> {
    /** Left object of triple to be used directly. */
    public LEFT left;
    /** Center object of triple to be used directly. */
    public CENTER center;
    /** Right object of triple to be used directly. */
    public RIGHT right;

    /** Creates a new instance of Pair containing two objects*/
    public Triple(LEFT left, CENTER center, RIGHT right) {
        this.left = left;
        this.center = center;
        this.right = right;
    }
}
