/*
 * Pair.java
 *
 * Created on April 28, 2005, 5:40 PM
 */

package kuhnlab.collection;

/** Class to contain two objects
 *
 * @author drjrkuhn
 */

public class Pair <LEFT, RIGHT> {
    /** left object of pair to be used directly. */
    public LEFT left;
    /** Right object of pair to be used directly. */
    public RIGHT right;

    /** Creates a new instance of Pair containing two objects*/
    public Pair(LEFT left, RIGHT right) {
        this.left = left;
        this.right = right;
    }
}
