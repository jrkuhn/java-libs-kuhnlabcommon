package kuhnlab.collection.mesh;

import ij.IJ;
import ij.gui.PolygonRoi;
import ij.gui.Roi;
import ij.gui.Wand;
import ij.process.ByteProcessor;
import ij.process.ImageProcessor;
import java.awt.Color;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import kuhnlab.coordinates.KPoint2D;
import kuhnlab.coordinates.KPolygon2D;

public class SkeletonMesh2D extends QuadMesh2D {

    public double dAvgWidth;

    //=======================================================================
    //============= Medial Axis Transform (Skeletonization) =================
    //=======================================================================
    /** locate an edge that has this endpoint */
    public QuadEdge2D locateEndpointExact(KPoint2D a) {
        for (QuadEdge2D e : aMesh) {
            if (a.equals(e.getOrg())) {
                return e;
            } else if (a.equals(e.getDest())) {
                return e.Sym();
            }
        }
        return null;
    }

    /** clear the marks from the edge list */
    public static void clearEdgeMarks(List<QuadEdge2D> aEdges) {
        for (QuadEdge2D e : aEdges) {
            e.clearMark();
        }
    }

    /** clear the parent information for all edges */
    public static void clearParents(List<QuadEdge2D> aEdges) {
        for (QuadEdge2D e : aEdges) {
            e.clearParents();
        }
    }

    /** add a new skeleton edge to the skeleton mesh */
    public QuadEdge2D addSkelEdgeExact(KPoint2D pA, EdgeType iTypeA, QuadEdge2D eParentA,
            KPoint2D pB, EdgeType iTypeB, QuadEdge2D eParentB) {
        QuadEdge2D e, eOld;
        e = QuadEdge2D.makeEdge(pA, pB, true);
        e.setType(iTypeA, iTypeB);
        e.setParents(eParentA, eParentB);

        eOld = locateEndpointExact(pA);
        if (eOld != null) {
            QuadEdge2D.splice(eOld, e);
        }

        eOld = locateEndpointExact(pB);
        if (eOld != null) {
            QuadEdge2D.splice(e.Sym(), eOld);
        }

        aMesh.add(e);
        return e;
    }

    /** perform a simple skeletonization of a mesh. */
    public void skeletonize(QuadMesh2D src) {
        aMesh.clear();

        KPoint2D pA, pB;
        QuadEdge2D eA, eB;
        double dSumWidth = 0;
        int iSumWidthCount = 0;
//        QuadEdge2D e = null;
//        boolean bSym = false;
//        ListIterator<QuadEdge2D> iter = src.aMesh.listIterator();
//        do {
//            e = bSym ? e.Sym() : iter.next().InvRot();
//            bSym = !bSym;
        for (QuadEdge2D e : new EdgeSymIterator<QuadEdge2D>(src.aMesh)) {

            if (e.bOutside || e.isMarked()) {
                // the left-hand face is outside or was already marked as processed
                continue;
            }

            // count the number of edges in the left-hand face
            int nEdges = e.countONext();
            //IJ.log("  "+nEdges+" edges in face");
            if (nEdges < 3) {
                // found a non-triangle
                continue;
            }


            // retriev a list of unconstrained (internal) edges in the face
            // constrained edges are edges from the original outline
            // unconstrained edges are added during triangulation
            // the number of internal edges tells us where in the outline we
            // are.
            List<QuadEdge2D> internalEdges = getUnconstrainedEdgesInFace(e);
            int nInternalEdges = internalEdges.size();

            //IJ.log("  "+nEdges+" edges, "+nInternalEdges+" internal edges in face");

            // determine the type of triangle (face) from the number of internal edges
            KPoint2D pCentroid = e.getFaceCentroid();
            if (pCentroid == null) {
                IJ.log("Bad centroid");
            }

            int j;
            switch (nInternalEdges) {
                case 0:	// ISOLATED: this is an isolated triangle. Don't measure it
                    //IJ.log("  ISOLATED");
                    break;

                case 1: // END: this is an endpoint triangle.
                    //IJ.log("  END");
                    eB = internalEdges.get(0);
                    pB = eB.Rot().getMidpoint();

                    addSkelEdgeExact(pCentroid, EdgeType.END, e, pB, EdgeType.INTERNAL, eB);
                    break;

                case 2:	// INTERNAL: this is a normal triangle
                    //IJ.log("  INTERNAL");
                    eA = internalEdges.get(0);
                    pA = eA.Rot().getMidpoint();
                    eB = internalEdges.get(1);
                    pB = eB.Rot().getMidpoint();

                    // Calculate the average skeleton width from the
                    // length of the splines. Use the shortest spline
                    double dLenA = eA.Rot().getLength();
                    double dLenB = eB.Rot().getLength();
                    dSumWidth += Math.min(dLenA, dLenB);
                    iSumWidthCount++;

                    addSkelEdgeExact(pA, EdgeType.INTERNAL, eA, pB, EdgeType.INTERNAL, eB);
                    break;

                default: // JUNCTION: this is a junction triangle
                    //IJ.log("  JUNCTION");
                    for (j = 0; j < nInternalEdges; j++) {
                        eA = internalEdges.get(j);
                        pA = eA.Rot().getMidpoint();

                        addSkelEdgeExact(pA, EdgeType.INTERNAL, eA, pCentroid, EdgeType.JUNCTION, e);
                    }
            }

            e.markAllONext();
        }
//        } while (iter.hasNext() || bSym);
        dAvgWidth = dSumWidth / iSumWidthCount;

        clearEdgeMarks(src.aMesh);
    }
    
    /** determine a skeleton edge type based on the face type of both of its ends */
    public EdgeType getSkelEdgeType(QuadEdge2D e) {
        EdgeType faceType = EdgeType.ISOLATED;

        // determine a skeleton edge type based on the type of both ends
        if (e.type == EdgeType.END || e.Sym().type == EdgeType.END) {
            faceType = EdgeType.END;
        } else if (e.type == EdgeType.JUNCTION || e.Sym().type == EdgeType.JUNCTION) {
            faceType = EdgeType.JUNCTION;
        } else if (e.type == EdgeType.INTERNAL && e.Sym().type == EdgeType.INTERNAL) {
            faceType = EdgeType.INTERNAL;
        }
        return faceType;
    }

    /** follow a branch to its junction point, its end, or its start site (in
     *	the case of a circular branch). */
    public List<QuadEdge2D> followBranch(QuadEdge2D eStart) {
        List<QuadEdge2D> aBranch = new ArrayList<QuadEdge2D>();
        QuadEdge2D e = eStart;

        aBranch.add(e);
        //IJ.log("FOLLOW: "+e.getOrg().toString()+" T"+e.iType+" --> "+e.getDest().toString()+" T"+e.Sym().iType);
        do {
            e = e.Sym().ONext();
            if (e.type != EdgeType.INTERNAL) {
                break;
            }
            if (e == eStart) {
                break;
            }
            aBranch.add(e);
            //IJ.log("FOLLOW: "+e.getOrg().toString()+" T"+e.iType+" --> "+e.getDest().toString()+" T"+e.Sym().iType);
        } while ((e.Sym()).type == EdgeType.INTERNAL);
        return aBranch;
    }

    /** Get the average width of the branch as the average length of each
     *	internal edge that the branch crosses in the original mesh. */
    public double getBranchAverageWidth(List<QuadEdge2D> aBranch) {
        double dSumWidth = 0;
        int nWidth = 0;
        for (QuadEdge2D e : aBranch) {
            if (e.type == EdgeType.INTERNAL) {
                dSumWidth += e.eParent.Rot().getLength();
                nWidth += 1;
            }
            if (e.Sym().type == EdgeType.INTERNAL) {
                dSumWidth += e.Sym().eParent.Rot().getLength();
                nWidth += 1;
            }
        }
        if (nWidth > 0) {
            return dSumWidth / nWidth;
        } else {
            return 0;
        }
    }

    /** Sum the lengths of all of the branch segments */
    public double getBranchLength(List<QuadEdge2D> aBranch, boolean bExcludeJunctions) {
        double dLength = 0;
        for (QuadEdge2D e : aBranch) {
            if (bExcludeJunctions && getSkelEdgeType(e) != EdgeType.JUNCTION) {
                dLength += e.getLength();
            }
        }
        return dLength;
    }

    /** Follow a branch for a certain distance and return the location of a 
     *	point on the branch line at that distance. */
    public KPoint2D getLocationOnBranch(List<QuadEdge2D> aBranch, double dDistance) {
        KPoint2D pO, pD, pA, vOA, vN = new KPoint2D(0, 0);
        double dEdgeLength = 0;
        int n = 0;
        QuadEdge2D e = aBranch.get(0);
        pO = e.getOrg();
        pD = e.getDest();
        // average the normalized direction vectors from the origin to each
        // endpoint to get the average line direction.
        ListIterator<QuadEdge2D> iter = aBranch.listIterator();
        while (iter.hasNext()) {
            e = iter.next();
            dEdgeLength += e.getLength();
            if (dEdgeLength > dDistance) {
                // this endpoint would put us above our distance
                break;
            }
            pA = e.getDest();
            vOA = KPoint2D.newDiffOf(pA, pO);
            vN.add(vOA);
            n++;
        }

        if (n == 0) {
            return new KPoint2D(pD);
        }

        vN.norm();
        //vN.divide(n);
        vN.scale(dDistance);
        vN.add(pO);
        return vN;
    }

    /** Prune small branches from the skeleton. Returns 'true' if the original
     *	triangulation was changed. */
    public boolean prune(QuadMesh2D src, double dMinRatio) {
        if (aMesh.isEmpty()) {
            return false;
        }

        List<QuadEdge2D> vToRemove = new ArrayList<QuadEdge2D>();

        QuadEdge2D eEnd = null;
        Iterator<QuadEdge2D> iter = aMesh.iterator();
        boolean sym;
        for (sym=false; iter.hasNext() || sym; sym = !sym) {
            eEnd = sym ? eEnd.Sym() : iter.next();

            if (eEnd.isMarked() || eEnd.type != EdgeType.END) {
                // not an endpoint or a previously marked endpoint
                continue;
            }

            // measure the length of the line and the average width;
            double dLength, dRatio = 0;

            List<QuadEdge2D> aBranch = followBranch(eEnd);
            if (aBranch.isEmpty()) {
                continue;
            }

            dLength = getBranchLength(aBranch, true);
            dAvgWidth = getBranchAverageWidth(aBranch);

            if (dAvgWidth > 0) {
                dRatio = dLength / dAvgWidth;
            }

            //IJ.log("branch length="+dLength+" width="+dAvgWidth+" ratio="+dRatio);

            // The branch needs to be pruned if the ratio of the length to the 
            // average width is less than some threshold (usually 0.5)
            if (dRatio <= dMinRatio) {
                //IJ.log("pruning");
                // add the internal edges this branch crosses to the list of edges
                // to be removed from the original triangulation.
                for (QuadEdge2D e : aBranch) {
                    if (e.type == EdgeType.INTERNAL && e.eParent != null) {
                        vToRemove.add((e.eParent).Rot());
                    }
                }
            }

        }

        clearEdgeMarks(aMesh);

        // is the triangulation going to change?		
        if (vToRemove.size() == 0) {
            return false;
        }

        // Now detach and remove all of the edges from the original triangulation
        // the edge could be hooked into the List<QuadEdge2D> in either direction. Try both
        detachAllEdges(vToRemove);
        src.aMesh.removeAll(vToRemove);
        symAll(vToRemove);
        src.aMesh.removeAll(vToRemove);
        return true;
    }

    /** Attempt to snap nearby junctions by estimating the real junction centers
     *	and removing any internal edges between nearby junctions */
    public boolean snapJunctions(QuadMesh2D src, double dSearchLength, double dSnapFraction) {
        if (aMesh.size() == 0) {
            return false;
        }

        List<QuadEdge2D> vToAdd = new ArrayList<QuadEdge2D>();
        List<QuadEdge2D> vToRemove = new ArrayList<QuadEdge2D>();
        int j, k, meshlen;
        QuadEdge2D eJunc = null, eBranch, e;
        boolean bSym = false;

        final int OLD = 0;	// array index of old center
        final int NEW = 1;	// array index of new center
        List<KPoint2D[]> vCenters = new ArrayList<KPoint2D[]>();
        double dAvgBranchWidth = 0;
        int nBW = 0;
        Iterator<QuadEdge2D> iter = aMesh.iterator();
        Iterator<QuadEdge2D> kter;
        do {
            // search for junction points
            eJunc = bSym ? eJunc.Sym() : iter.next();
            bSym = !bSym;

            if (eJunc.isMarked() || eJunc.type != EdgeType.JUNCTION) {
                // not a junction or a previously marked junction
                continue;
            }

            // Go around the junction and find the average intersection
            // point of all of the lines coming out of the branch.
            eBranch = eJunc;
            int nC = 0;
            KPoint2D[] apCenter = new KPoint2D[2];
            apCenter[OLD] = eJunc.getOrg();
            apCenter[NEW] = new KPoint2D(0, 0);
            do {
                // create a straight line representation of the branch
                QuadEdge2D e1 = eBranch.Sym().ONext();
                double dWidth1 = getBranchAverageWidth(followBranch(eBranch));
                double dW = (dWidth1 > 0) ? dWidth1 : 1.5;
                KPoint2D pA1 = e1.getOrg();
                KPoint2D pB1 = getLocationOnBranch(followBranch(e1), dSearchLength * dW);
                e1 = QuadEdge2D.makeEdge(pA1, pB1, false);

                dAvgBranchWidth += dWidth1;
                nBW++;

                //QuadEdge2D eShow = QuadEdge2D.makeEdge(pA1, pB1, false);
                //eShow.setType(QuadEdge2D.INTERNAL, QuadEdge2D.INTERNAL);
                //eShow.setOrg(KPoint2D.newDiffOf(KPoint2D.multiply(2, pA1), pB1));
                //eShow.specialColor=Color.green;
                //vToAdd.add(eShow);

                QuadEdge2D e2 = (eBranch.ONext()).Sym().ONext();
                //double dWidth2 = getBranchAverageWidth(followBranch(eBranch.ONext()));
                KPoint2D pA2 = e2.getOrg();
                KPoint2D pB2 = getLocationOnBranch(followBranch(e2), dSearchLength);
                e2 = QuadEdge2D.makeEdge(pA2, pB2, false);

                //IJ.log("width1="+dWidth1+" width2="+dWidth2);

                KPoint2D pIntersect = e1.findIntersection(e2);
                if (pIntersect == null) {
                    IJ.log("Bad junction");
                    return false;
                }
                apCenter[NEW].add(pIntersect);
                nC++;

                //IJ.log("Intersect="+pIntersect.toString());
                //eShow = QuadEdge2D.makeEdge(pIntersect, pIntersect, true);
                //eShow.setType(QuadEdge2D.END, QuadEdge2D.END);
                //eShow.specialColor=Color.magenta;
                //vToAdd.add(eShow);

                eBranch.setMark();
                eBranch = eBranch.ONext();
            } while (eBranch != eJunc);

            if (nC == 0) {
                IJ.log("Bad junction");
                return false;
            }


            //IJ.log("Averaged "+nC+" centers");
            apCenter[NEW].scale(1.0 / nC);

            //IJ.log("Old center="+apCenter[OLD].toString()+"  New center="+apCenter[NEW].toString());

            // add the new center to the list. This list will be checked later for
            // junction snapping
            vCenters.add(apCenter);

            //QuadEdge2D eShow = QuadEdge2D.makeEdge(apCenter[OLD], apCenter[NEW], true);
            //eShow.setType(QuadEdge2D.JUNCTION, QuadEdge2D.JUNCTION);
            //eShow.specialColor=Color.orange;
            //vToAdd.add(eShow);

            //addSkelEdge(pOldCenter, QuadEdge2D.JUNCTION, null, pNewCenter, QuadEdge2D.JUNCTION, null);

            // see if the line from the old center to the new center crosses any internal
            // edges in the original mesh. If so, they are marked for removal
            if (false) {
                kter = src.aMesh.iterator();
                while (kter.hasNext()) {
                    e = kter.next();
                    if (!e.isConstrained() && e.intersects(apCenter[OLD], apCenter[NEW])) {
                        vToRemove.add(e);
                    }
                }
            }
        } while (iter.hasNext() || bSym);

        // calculate the average branch width
        if (nBW > 0) {
            dAvgBranchWidth /= nBW;
        }

        // center points closer than dSnapDistance will be merged
        double dSnapDistance = dSnapFraction * dAvgBranchWidth;
        //IJ.log("AvgWidth = "+dAvgBranchWidth+" SnapDist="+dSnapDistance);

        // check every pair of center centers to see if they can be snapped together
        KPoint2D[] ap1;
        KPoint2D[] ap2;
        Iterator<KPoint2D[]> jter;
        Iterator<KPoint2D[]> lter;
        lter = vCenters.iterator();
        while (lter.hasNext()) {
            ap1 = lter.next();
            jter = vCenters.iterator();
            while (jter.hasNext()) {
                ap2 = (KPoint2D[]) jter.next();
                if (ap1 == ap2) {
                    continue;
                }
                if (KPoint2D.distBetween(ap1[NEW], ap2[NEW]) < dSnapDistance) {
                    //IJ.log("SNAPPING "+p1.toString()+" to "+p2.toString());

                    boolean bConstrainedCross = false;
                    List<QuadEdge2D> aCrossings = new ArrayList<QuadEdge2D>();
                    // search for crossing edges to remove
                    kter = src.aMesh.iterator();
                    while (kter.hasNext()) {
                        e = kter.next();
                        if (e.intersects(ap1[OLD], ap2[OLD])) {
                            if (e.isConstrained()) {
                                // the line between p1 and p2 crosses an external edge,
                                // so we discount it.
                                bConstrainedCross = true;
                                break;
                            } else {
                                aCrossings.add(e);
                            }
                        }
                    }
                    if (!bConstrainedCross) {
                        vToRemove.addAll(aCrossings);
                    }
                }
            }
        }

        aMesh.addAll(vToAdd);
        clearEdgeMarks(aMesh);
        // is the triangulation going to change?		
        if (vToRemove.size() == 0) {
            return false;
        }

        // Now detach and remove all of the edges from the original triangulation
        // the edge could be hooked into the List<QuadEdge2D> in either direction. Try both
        detachAllEdges(vToRemove);
        src.aMesh.removeAll(vToRemove);
        symAll(vToRemove);
        src.aMesh.removeAll(vToRemove);

        return true;
    }

    /** Make a list of all of the internal edges in a face */
    public List<QuadEdge2D> getUnconstrainedEdgesInFace(QuadEdge2D eStart) {
        QuadEdge2D e = eStart;
        List<QuadEdge2D> internal = new ArrayList<QuadEdge2D>();
        do {
            if (!e.isConstrained()) {
                internal.add(e);
            }
            e = e.ONext();
        } while (e != eStart);
        return internal;
    }

    //=======================================================================
    //====================== Image Skeletonization ==========================
    //=======================================================================
    /**	Search recursively through an in image looking for continuous regions with
     *	intensities above the minimum threshold (for outlines) or below the minimum
     *	threshold (for holes). The image may be masked to only search a specific
     *	region.	 */
    public List<KPolygon2D> findOutlines(ImageProcessor ip, int xoff, int yoff, double dMinArea,
            boolean bFindHoles, double dDownsample) {

        // planar straight-line graph of outline
        List<KPolygon2D> aPSLG = new ArrayList<KPolygon2D>();

        int xy, w, h, ss;
        w = ip.getWidth();
        h = ip.getHeight();
        ss = w * h;

        int x, y, yline;

        boolean bValue;
        double dArea;

        byte[] abPix = (byte[]) ip.getPixels();

        for (y = 0, yline = 0; y < h; y++, yline += w) {
            for (x = 0; x < w; x++) {
                bValue = (abPix[yline + x] & 0xff) > 0;
                //bValue = (abPix[yline + x]) != 0;

                if (bValue) {
                    Wand wand = new Wand(ip);
                    //wand.autoOutline(x, y, 127, 255);
                    wand.autoOutline(x, y, 2.0, Wand.FOUR_CONNECTED);
                    if (wand.npoints == 0) {
                        IJ.log("wand error: " + (x + xoff) + ", " + (y + yoff));
                    }
                    int[] xpoints = new int[wand.npoints];
                    int[] ypoints = new int[wand.npoints];
                    System.arraycopy(wand.xpoints, 0, xpoints, 0, wand.npoints);
                    System.arraycopy(wand.ypoints, 0, ypoints, 0, wand.npoints);

                    Roi roi = new PolygonRoi(xpoints, ypoints, wand.npoints, Roi.TRACED_ROI);
                    Rectangle r = roi.getBounds();
                    ImageProcessor ipmask = roi.getMask();

                    // create a copy of this image to pass down the chain					
                    ImageProcessor ipCopy = ip.duplicate();

                    // mask off this region in the main					
                    ip.setRoi(r);
                    ip.setMask(ipmask);
                    ip.setColor(Color.black);
                    ip.fill(ipmask);
                    ip.resetRoi();

                    // mask off everything but this region in the copy
                    ipmask.invert();
                    //invertMask(ipmask);
                    ipCopy.setRoi(r);
                    ipCopy.setMask(ipmask);
                    ipCopy.setColor(Color.white);
                    ipCopy.fill(ipmask);
                    // crop the copy down to size and pass on to the next stage
                    ipCopy = ipCopy.crop();
                    ipCopy.resetRoi();
                    ipCopy.invert();

                    List<KPolygon2D> vNextStage = findOutlines(ipCopy, r.x, r.y, dMinArea, !bFindHoles, dDownsample);

                    // add the results of the next stage to the PSLG
                    if (vNextStage.size() > 0) {
                        aPSLG.addAll(vNextStage);
                    }

                    // convert the wand outline to a complete polygon

                    KPolygon2D aPoly = wandToOutline(wand, xoff, yoff);
                    // downsample the outline to smooth it
                    aPoly = aPoly.fourierDownsampledCopy(1.0 / dDownsample);
                    // The fourier downsampling can lead to self crossings.
                    // That means the polygon will go from being inside to
                    // being outside (like a mobius strip).
                    // So, we remove any crossings (much like topoisomerase
                    // unwinds supercoiled DNA).
                    aPoly.unwind();
                    dArea = aPoly.polygonArea();
                    if (dArea >= dMinArea) {
                        // Test the orientation of the polygon. Outlines should be CCW,
                        // holes should be CW.
                        if (!bFindHoles && aPoly.isClockwise()) {
                            aPoly.reverse();
                        }
                        if (bFindHoles && aPoly.isCounterClockwise()) {
                            aPoly.reverse();
                        }
                        // add the results of this stage to the PSLG	
                        aPSLG.add(aPoly);
                    }

                }
            }
        }
        return aPSLG;
    }

    /** help function for findOutlines. Invert the bits of the ROI mask. */
    void invertMask(int[] mask) {
        int i, len = mask.length;
        for (i = 0; i < len; i++) {
            if (mask[i] != ImageProcessor.BLACK) {
                mask[i] = ImageProcessor.BLACK;
            } else {
                mask[i] = ~ImageProcessor.BLACK;
            }
        }
    }

    /** The wand can return a lines that are more than one pixel in length.
     *	For the triangulation, we want to catch every pixel if possible.
     *	so we look for these horizontal or vertical lines and expand them. */
    KPolygon2D wandToOutline(Wand wand, int xoff, int yoff) {
        int nPoints = wand.npoints;
        KPolygon2D poly = new KPolygon2D(true);

        int i, n, xs, ys, dir, xnew, ynew, xold, yold, dx, dy;

        xold = wand.xpoints[0] + xoff;
        yold = wand.ypoints[0] + yoff;
        poly.add(new KPoint2D(xold, yold));
        for (i = 1; i < nPoints; i++) {
            xnew = wand.xpoints[i] + xoff;
            ynew = wand.ypoints[i] + yoff;
            dx = xnew - xold;
            dy = ynew - yold;

            if (true) {
                if (dx == 0 && Math.abs(dy) > 1) {
                    // fill in y direction
                    xs = xold;
                    ys = yold;
                    dir = (dy < 0) ? -1 : +1;
                    if (dy < 0) {
                        dy = -dy;
                    }
                    for (n = 0; n < dy; n++) {
                        ys += dir;
                        poly.add(new KPoint2D(xs, ys));
                    }
                } else if (dy == 0 && Math.abs(dx) > 1) {
                    // fill in x direction
                    xs = xold;
                    ys = yold;
                    dir = (dx < 0) ? -1 : +1;
                    if (dx < 0) {
                        dx = -dx;
                    }
                    for (n = 0; n < dx; n++) {
                        xs += dir;
                        poly.add(new KPoint2D(xs, ys));
                    }
                } else {
                    poly.add(new KPoint2D(xnew, ynew));
                }
            } else {
                poly.add(new KPoint2D(xnew, ynew));
            }
            xold = xnew;
            yold = ynew;
        }

        return poly;
    }

    /** Main image skeletonization entry point. Convert an 8-bit thresholded image to
     *	a vector skeleton. */
    public boolean skeletonize(ByteProcessor bp, double dImageScale, double dDownsample, double dMinArea,
            double dPruneLength, double dSnapDist, double dFollowDist, List<QuadEdge2D> aOutline) {

        if (bp == null) {
            return false;
        }

        //**
        //** Outline thresholded image
        //**
        List<KPolygon2D> aPSLG = findOutlines(bp, 0, 0, dMinArea, false, dDownsample);

        // convert the vector of polygons to an array of polygons		
        int nPolys = aPSLG.size();
        if (nPolys == 0) {
            return false;
        }
        // Scale the image points
        double dScaleFactor = 1.0 / dImageScale;
        for (KPolygon2D poly : aPSLG) {
            for (KPoint2D p : poly.points) {
                p.x *= dScaleFactor;
                p.y *= dScaleFactor;
            }
        }
        List<KPolygon2D> allPolys = new ArrayList<KPolygon2D>();
        allPolys.addAll(aPSLG);

        // Triangulate the PSLG	(convert it to a mesh)	
        QuadMesh2D outlineMesh = new QuadMesh2D();


        IJ.showStatus("Triangulating");
        outlineMesh.createCDT(allPolys);

        if (outlineMesh.aMesh.size() == 0) {
            return false;
        }

        //**
        //** Skeletonize
        //**
        skeletonize(outlineMesh);

        //**
        //** Prune
        //**
        if (dPruneLength > 0) {
            IJ.showStatus("Pruning");
            while (prune(outlineMesh, dPruneLength)) {
                IJ.showStatus("Skeleton was pruned. Reskeletonizing");
                skeletonize(outlineMesh);
            }
        }

        //**
        //** Snap junctions together
        //**
        if (dSnapDist > 0) {
            IJ.showStatus("Snapping junctions");
            while (snapJunctions(outlineMesh, dFollowDist, dSnapDist)) {
                IJ.showStatus("Junctions changed. Reskeletonizing");
                skeletonize(outlineMesh);
            }
        }

        if (aOutline != null) {
            // the calling routine wants a copy of the outline triangulation
            aOutline.addAll(outlineMesh.aMesh);
        }
        outlineMesh = null;

        IJ.showStatus("done");

        return true;
    }

    public void disconnectFromMesh() {
        //**
        //** Disconnect skeleton from CDT outlineMesh so that it can be freed
        //**
        clearParents(aMesh);
    }
}
