/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package kuhnlab.collection.mesh;

/** Defines a few standard shared operators for edges. These are shared
 *  by both HalfEdge and QuadEdge
 *
 * @author jrkuhn
 */
public interface Edge {
    /** Return the edge from the destination to the origin of the current edge. */
    public Edge Sym();

    /** Return the next ccw edge around (from) the origin of the current edge */
    public Edge ONext();

    /** Return the ccw edge around the left face following the current edge. */
    public Edge LNext();
}
