/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package kuhnlab.collection.mesh;

import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;

/** Iterates counter-clockwise around all of the edges connected to the left
 *  face of this edge.
 *
 * @author jrkuhn
 */
public class EdgeLeftFaceIterator<T extends Edge> implements Iterable<T> {

    protected T start;
    protected T current;
    protected boolean hasNext;
    public EdgeLeftFaceIterator(T start) {
        this.start = this.current = start;
    }

    public Iterator<T> iterator() {
        return new Iterator() {

            public boolean hasNext() {
                return hasNext;
            }

            public Object next() {
                if (!hasNext) {
                    throw new NoSuchElementException();
                }
                T old = current;
                current = (T)current.LNext();
                if (current == start) {
                    hasNext = false;
                }
                return old;
            }

            public void remove() {
                throw new UnsupportedOperationException();
            }

        };
    }

}
