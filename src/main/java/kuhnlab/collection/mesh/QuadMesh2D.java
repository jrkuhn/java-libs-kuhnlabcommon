package kuhnlab.collection.mesh;

import ij.IJ;
import ij.ImagePlus;
import ij.gui.Roi;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.util.*;
import kuhnlab.coordinates.KPoint2D;
import kuhnlab.coordinates.KPolygon2D;

public class QuadMesh2D {

    public List<QuadEdge2D> aMesh = new ArrayList<QuadEdge2D>();
    final double MARGIN = 1;
    final double EPSILON = 0.001;

    protected void initCDT(double dMinX, double dMinY, double dMaxX, double dMaxY) {
        // create the bounding quadrangle
        QuadEdge2D e0, e1, e2, e3, e4;
        KPoint2D p0, p1, p2, p3;

        p0 = new KPoint2D(dMinX - MARGIN, dMinY - MARGIN);
        p1 = new KPoint2D(dMaxX + MARGIN, dMinY - MARGIN);
        p2 = new KPoint2D(dMaxX + MARGIN, dMaxY + MARGIN);
        p3 = new KPoint2D(dMinX - MARGIN, dMaxY + MARGIN);
        e0 = QuadEdge2D.makeEdge(p0, p1, false);
        e1 = QuadEdge2D.makeEdge(p1, p2, false);
        e2 = QuadEdge2D.makeEdge(p2, p3, false);
        e3 = QuadEdge2D.makeEdge(p3, p0, false);

        aMesh.add(e0);
        aMesh.add(e1);
        aMesh.add(e2);
        aMesh.add(e3);

        QuadEdge2D.splice(e0.Sym(), e1);
        QuadEdge2D.splice(e1.Sym(), e2);
        QuadEdge2D.splice(e2.Sym(), e3);
        QuadEdge2D.splice(e3.Sym(), e0);

        // split the bounding quadrangle into two triangles
        insertEdge(p0, p2, false, false, null);
    }

    //=======================================================================
    //=============== Constrained Delaunay Triangulation ====================
    //=======================================================================
    /** Create a constrained Delaunay triangulation of a set of closed polygons.
     *	outside edges should be in counterclockwise order, while hole edges
     *	should be in clockwise order. */
    void createCDT(List<KPolygon2D> aPSLG) {
        int i, j, p, nPoly = aPSLG.size(), len;

        // scan through the polygon list and find the bounding box;	
        double dMinX = Double.MAX_VALUE, dMaxX = Double.MIN_VALUE;
        double dMinY = Double.MAX_VALUE, dMaxY = Double.MIN_VALUE;

        for (KPolygon2D aPoly : aPSLG) {
            for (KPoint2D pt : aPoly.points) {
                if (pt.x < dMinX) {
                    dMinX = pt.x;
                }
                if (pt.x > dMaxX) {
                    dMaxX = pt.x;
                }
                if (pt.y < dMinY) {
                    dMinY = pt.y;
                }
                if (pt.y > dMaxY) {
                    dMaxY = pt.y;
                }
            }
        }

        initCDT(dMinX, dMinY, dMaxX, dMaxY);

        // add all of the polygon points
        IJ.showStatus("Adding points");
        for (KPolygon2D aPoly : aPSLG) {
            for (KPoint2D pt : aPoly.points) {
                insertSite(pt);
            }
        }

        // add all of the constrained edges
        IJ.showStatus("Adding edges");
        for (KPolygon2D aPoly : aPSLG) {
            len = aPoly.points.size();
            for (i = 0; i < len; i++) {
                j = (i + 1) % len;
                //IJ.write(""+i+" "+aPoly[i].toString()+" to "+aPoly[j].toString());
                insertEdge(aPoly.points.get(i), aPoly.points.get(j), true, true, null);
            }
        }

        IJ.showProgress(1.0);


        IJ.showStatus("triangulating");
        // perform a basic retriangulation
        retriangulate();

        IJ.showStatus("flipping");
        // convert the basic triangulation to a Delaunay triangulation
        flipAll();

        //if(true)return;

        IJ.showStatus("Removing outside edges");
        // propagate the 'outside' face mark to all of the edges outside of the polygon
        // or within holes. 
        propagateOutside();

        //if(true)return;

        // remove all edges that have both left-hand and right-hand faces marked as outside
        removeOutsideEdges();
    }

    /** locate the edge nearest to a given point */
    public static QuadEdge2D locate(KPoint2D x, QuadEdge2D eFirst) {
        QuadEdge2D e = eFirst;
        do {
            if (x.equals(e.getOrg()) || x.equals(e.getDest())) {
                return e;
            } else if (QuadEdge2D.isRightOf(x, e)) {
                e = e.Sym();
            } else if (!QuadEdge2D.isRightOf(x, e.ONext())) {
                e = e.ONext();
            } else if (!QuadEdge2D.isRightOf(x, e.DPrev())) {
                e = e.DPrev();
            } else {
                return e;
            }
        } while (true);
    }

    /** find all edges that have either their destination or origin as this point and
     *	safely delete them, thus deleting the point from the edge list. */
    public boolean deleteSite(KPoint2D x) {
        // find an edge with x as the origin or its destination
        QuadEdge2D e = null;
        for (QuadEdge2D t : aMesh) {
            if (x.equals(t.getOrg())) {
                e = t;
                break;
            }
            if (x.equals(t.getDest())) {
                e = t.Sym();
                break;
            }
        }
        if (e == null) {
            // could not find the point
            return false;
        }

        // make a list of edges to remove by followin the ONext chain around the
        // edges origin point.
        QuadEdge2D f = e;
        List<QuadEdge2D> toRemove = new ArrayList<QuadEdge2D>();
        do {
            toRemove.add(f);
            f = f.ONext();
        } while (f != e);

        // remove these edges from the mesh
        detachAllEdges(toRemove);
        aMesh.removeAll(toRemove);
        return true;
    }

    /** Insert a new site in the triangulation, connect it to the three nearest edges,
     *	and flip all of the resulting edges until the new triangulation is Delaunay. */
    public QuadEdge2D insertSite(KPoint2D x) {

        // must have created a bounding triangle first
        if (aMesh.size() < 3) {
            return null;
        }

        QuadEdge2D e = locate(x, aMesh.get(0));
        if (x.equals(e.getOrg()) || x.equals(e.getDest())) {
            // the site already exists.
            return e;
        }
        // Connect X to vertices around it
        QuadEdge2D eBase = QuadEdge2D.makeEdge(false);
        KPoint2D pOrg = e.getOrg();
        eBase.setOrg(pOrg);
        eBase.setDest(x);
        QuadEdge2D.splice(eBase, e);
        aMesh.add(eBase);
        do {
            eBase = QuadEdge2D.connect(e, eBase.Sym(), false);
            aMesh.add(eBase);
            e = eBase.OPrev();
        } while (e.getDest() != pOrg);
        e = eBase.OPrev();

        // The suspect edges (from top to bottom) are e(.ONext.LPrev)^k for k=0,1,...
        // The bottom edge has .Org == first
        do {
            QuadEdge2D t = e.OPrev();
            if (QuadEdge2D.isRightOf(t.getDest(), e) && KPoint2D.isInCircle(e.getOrg(), t.getDest(), e.getDest(), x)) {
                e.flip();
                e = t;
            } else if (pOrg.equals(e.getOrg())) {
                // Nor more suspect edges
                return eBase.Sym();
            } else {
                // pop a suspect edge
                e = e.ONext().LPrev();
            }
        } while (true);
    }

    /** Insert a new edge into the triangulation. If the new edge is constrained (i.e.
     *	part of the planar-straight-line-graph) then all existing edges it crosses
     *	are removed from the triangulation. NOTE: both endpoints must already exist
     *	within the triangulation. If not, use insertSite() before calling this routine. */

    public QuadEdge2D insertEdge(KPoint2D a, KPoint2D b, boolean constrain, boolean removeCrosses, ListIterator<QuadEdge2D> itRecrusive) {
        QuadEdge2D eOrg = null, eDest = null, eOld = null;

//        if (a.epsilonEquals(46.4, 135.9, .1) && b.epsilonEquals(46.1, 131.0, .1)) {
//            IJ.log("found debug insertEdge");
//        }
        // search for edges that have the same endpoint as 'a' or 'b'. While we are at it,
        // we see if this edge already exists, and also look for edge crossings.
        ListIterator<QuadEdge2D> it = (itRecrusive == null) ? aMesh.listIterator() : itRecrusive;
        LinkedList<QuadEdge2D> toRemove = new LinkedList<QuadEdge2D>();
        while (it.hasNext()) {
            QuadEdge2D e = it.next();
            
//            final int debugList[] = {476,601,603};
//            if (e.isInDebugList(debugList)) {
//                IJ.log("Found debug test edge "+e.debugID);
//            }
            if (e.equals(a, b)) {

                // This edge already exists in the correct direction.
                if (constrain) {
                    e.constrain();
                    // mark the right-hand face as outside
                    e.Rot().bOutside = true;
                }
                eOld = e;
                break;

            } else if (e.equals(b, a)) {
                // This edge already exists in the opposite direction.
                if (constrain) {
                    e.constrain();
                    //e.constrain();
                    // mark the left-hand face as outside
                    e.InvRot().bOutside = true;
                }
                eOld = e;
                break;
            }
            if (eOrg == null) {
                if (e.getOrg().equals(a)) {
                    eOrg = e;
                } else if (e.getDest().equals(a)) {
                    eOrg = e.Sym();
                }
            }

            if (eDest == null) {
                if (e.getOrg().equals(b)) {
                    eDest = e;
                } else if (e.getDest().equals(b)) {
                    eDest = e.Sym();
                }
            }

            if (removeCrosses && e.intersects(a, b)) {
                
                if (!e.isConstrained()) {
                    toRemove.add(e);
                } else {
                    // Split this edge at the intersection point
                    KPoint2D pCross = e.findIntersection(a, b);
                    if (pCross.isNaN()) {
                        continue;
                    }

                    QuadEdge2D e2 = divideEdge(e, pCross, constrain, it);
                    rewind(it);
                    if (pCross.epsilonEquals(a, EPSILON)) {
                        // Point a was on edge e
                        a = pCross;
                        eOrg = e2;
                    } else if (pCross.epsilonEquals(b, EPSILON)) {
                        b = pCross;
                        eDest = e2;
                    } else {
                        // recursively add a new edge from pCross to b
                        QuadEdge2D eNew2 = insertEdge(pCross, b, constrain, removeCrosses, it);
                        it.add(eNew2);
                        rewind(it);
                        b = pCross;
                        eDest = eNew2;
                    }
                }
            }
        } // END for
        
        while(!toRemove.isEmpty()) {
            QuadEdge2D eToRemove = toRemove.removeFirst();
            rewind(it);
            while (it.hasNext()) {
                QuadEdge2D e = it.next();
                if (e == eToRemove) {
                    it.remove();
                    break;
                }
            }
        }

        if (eOld != null) {
            return eOld;
        }

        if (eOrg == null || eDest == null) {
            System.out.println("insertEdge could not find one of the endpoints in the graph");
            return null;
        }

        QuadEdge2D eNew = makeConnectingEdge(eOrg, eDest, constrain);
        
        // If this is a constrained edge, then it is part of a polygon. Mark the right-hand
        // face as outside. For this to work, the polygon must be in counterclockwise order
        // (or clockwise order for holes.
        if (constrain) {
            eNew.Rot().bOutside = true;
        }
        
        it.add(eNew);
        return eNew;
    }

    public QuadEdge2D divideEdge(QuadEdge2D e, KPoint2D pCross, boolean bConstrain, ListIterator<QuadEdge2D> it) {

        // remove the destination point connections (and save them)
        QuadEdge2D eSym = e.Sym();
        QuadEdge2D eSymOPrev = eSym.OPrev();
        QuadEdge2D.splice(eSym, eSymOPrev);
        assert (eSym.eNext == eSym);

        // Move the old edge's destination to the intersection point
        KPoint2D pDest = e.getDest();
        e.setDest(pCross);
        
        // Make a new edge starting at the intersection point and going to the original destination
        QuadEdge2D e2 = QuadEdge2D.makeEdge(pCross, pDest, e.isConstrained());
        
        // Copy the "outside" states of the old edge
        e2.bOutside = e.bOutside;
        e2.Rot().bOutside = e.Rot().bOutside;
        e2.Sym().bOutside = e.Sym().bOutside;
        e2.InvRot().bOutside = e.InvRot().bOutside;

        // Spice the new edge's destination into the mesh where the old destination was
        QuadEdge2D.splice(e2.Sym(), eSymOPrev);
        // Splice the old edge's destnation into the new edge's origin
        QuadEdge2D.splice(eSym, e2);

        if (it == null) {
            it = aMesh.listIterator();
        }
        
        it.add(e2);
        return e2;
    }
    
    protected static void rewind(ListIterator<QuadEdge2D> it) {
        while (it.hasPrevious()) {
            it.previous();
        }
    }


    /** Splice a new edge to the an old edge by finding the best place in the ONext chain
     *	to insert into. Assume the origin points of eOrg and eNew are the same. */
    public void spliceBest(QuadEdge2D eOrg, QuadEdge2D eNew) {
        KPoint2D a = eNew.getOrg();
        KPoint2D b = eNew.getDest();
        if (!a.equals(eOrg.getOrg())) {
            //IJ.write("Attempting to splice a new edge to the incorrect point");
            return;
        }

        // test for polygon status
        if (eOrg.ONext() == eOrg) {
            QuadEdge2D.splice(eOrg, eNew);
        } else {
            QuadEdge2D e = eOrg;
            do {
                if (e.isRightOf(b) && e.ONext().isLeftOf(b)) {
                    eOrg = e;
                    break;
                }
                e = e.ONext();
            } while (e != eOrg);
            QuadEdge2D.splice(eOrg, eNew);
        }
    }

    /** there might be many different edges attached to the origin point and destination. 
     *  We need to find the most appropriate edges to connect to. Imagine two existing 
     *	edges connected to the origin point. If a new edge were to be inserted between
     *  them, then the new destination should lie to the right of one edge and to the
     *  left of the other. A similar argument holds for the existing edges already at
     *	the destination point. */
    public QuadEdge2D makeConnectingEdge(QuadEdge2D eOrg, QuadEdge2D eDest, boolean bConstrain) {
        KPoint2D a = eOrg.getOrg();
        KPoint2D b = eDest.getOrg();

        // search through the origin edge's ONext list for the best place to
        // insert a new edge.
        QuadEdge2D e = eOrg;
        do {
            if (e.isRightOf(b) && e.ONext().isLeftOf(b)) {
                eOrg = e;
                break;
            }
            e = e.ONext();
        } while (e != eOrg);

        // search through the destination edge's ONext list for the best place to
        // insert a new edge.
        e = eDest;
        do {
            if (e.isRightOf(a) && e.ONext().isLeftOf(a)) {
                eDest = e;
                break;
            }
            e = e.ONext();
        } while (e != eDest);

        // create a new edge to connect the origin and destination and splice it
        // into the mesh.
        e = QuadEdge2D.makeEdge(a, b, bConstrain);
        QuadEdge2D.splice(eOrg, e);
        QuadEdge2D.splice(e.Sym(), eDest);

        return e;
    }

    /** Go through the edge list and check each (unconstrained) edge for Delaunay criteria. 
     *	If it is not a Delaunay edge, then it is flipped within its quadrangle and the
     *  four edges of the bounding quadrangle are added to the end of the queue to be
     *	checked. This process proceeds until the queue is empty and all of the edges
     *	have been checked. */
    public void flipAll() {
        LinkedList<QuadEdge2D> lToFlip = new LinkedList<QuadEdge2D>();

        // begin by placing all of the non-constrained edges on the queue
        for (QuadEdge2D t : aMesh) {
            if (!t.isConstrained()) {
                lToFlip.addLast(t);
            }
        }

        // go through each edge in the list and test to see if it needs to 
        // be swapped (rotated within its bounding quadrilateral).
        KPoint2D a, b, c, d;
        QuadEdge2D e;
        while (lToFlip.size() > 0) {
            e = lToFlip.removeFirst();
            if (e.isConstrained()) {
                continue;
            }
            a = e.getOrg();
            b = e.OPrev().getDest();
            c = e.ONext().getDest();
            d = e.getDest();
            if (!KPoint2D.isInCircle(a, b, c, d) && !KPoint2D.isOnCircle(a, b, c, d)) {
                lToFlip.addLast(e.OPrev());
                lToFlip.addLast(e.ONext());
                lToFlip.addLast(e.Sym().OPrev());
                lToFlip.addLast(e.Sym().ONext());
                e.flip();
            }
        }
    }

    /** Each face to the right of the bounding polygon should have previously
     *	been marked as 'outside'. Since there might be edges completely outside
     *	of the polygon but not immediately adjacent to it, they also need to be
     *	marked. Here the edge list is searched for edges marked as outside. If
     *	found, the other edges that form the triangle are maked as outside and
     *	they are added to the queue of edges to check. If the edge is both
     *  unconstrained and outside, the outside mark is propagated to the opposite
     *  face and the edge is added to the end of the queue of edges to check.
     *	the process repeats until the queue is empty. */
    public void propagateOutside() {
        LinkedList<QuadEdge2D> lNewRegion = new LinkedList<QuadEdge2D>();

        // start with a subset of all edges
        lNewRegion.addAll(aMesh);

        QuadEdge2D f;
        QuadEdge2D e = null;
        boolean bSym = false;
        while (lNewRegion.size() > 0) {
            // go through each edge, looking for an outside mark. If it exists,
            // mark all of the other edges in that face. Each edge is checked
            // from origin to destination and from destination to origin.
            e = bSym ? e.Sym() : lNewRegion.removeFirst();
            bSym = !bSym;

            if (e.Rot().bOutside) {
                f = e.RNext();
                // mark all of the other edges in this face. add to the end
                // of the check list if they were not already marked.
                while (f != e) {
                    if (!f.Rot().bOutside) {
                        f.Rot().bOutside = true;
                        lNewRegion.add(f);
                    }
                    f = f.RNext();
                }
                // propagate the outside mark across unconstrained edge
                if (!e.isConstrained()) {
                    if (!e.InvRot().bOutside) {
                        e.InvRot().bOutside = true;
                        //lNewRegion.add(e);
                    }
                }
            }
        }
    }
    
    /** An outside edge is one in which both adjacent faces are outside
     *	of the polygon. */
    public void removeOutsideEdges() {
        QuadEdge2D e;
        ListIterator<QuadEdge2D> iter = aMesh.listIterator();
        while (iter.hasNext()) {
            e = iter.next();
            if (e.Rot().bOutside && e.InvRot().bOutside) {
                e.detach();
                iter.remove();
            }
        }
    }

    /** Look for faces with more than three edges. Add edges until all faces
     *	have only three edges. */
    public void retriangulate() {
        QuadEdge2D e, f, eOrg, eDest;
        List<QuadEdge2D> aNew = new LinkedList<QuadEdge2D>();

        int n, nTotal;
        ListIterator<QuadEdge2D> iter;
        do {
            aNew.clear();
            iter = aMesh.listIterator();
            nTotal = aMesh.size();
            n = 0;
            while (iter.hasNext()) {
                IJ.showProgress(n, nTotal);
                n++;
                e = iter.next();
                eOrg = e;
                // is the left-hand face bigger than a triangle?
                eDest = e.LNext().LNext();
                if (eDest != e && eDest.LNext() != e) {

                    // insure that this is a non-crossing edge
                    KPoint2D pA = eOrg.getOrg();
                    KPoint2D pB = eDest.getOrg();

                    boolean bCrosses = false;
                    ListIterator<QuadEdge2D> jter = aMesh.listIterator();
                    while (jter.hasNext()) {
                        f = jter.next();
                        if (f.intersects(pA, pB)) {
                            bCrosses = true;
                            break;
                        }
                    }
                    if (!bCrosses) {
                        aNew.add(makeConnectingEdge(eOrg, eDest, false));
                    }
                }
            }
            if (!aNew.isEmpty()) {
                aMesh.addAll(aNew);
            }
        } while (!aNew.isEmpty());
    }

    /** detach a collection of edges from the meshwork edge. */
    public void detachAllEdges(List<QuadEdge2D> aEdges) {
        for (QuadEdge2D e : aEdges) {
            e.detach();
        }
    }

    /** replace a collection of edges with their symmetric edge. */
    public static void symAll(List<QuadEdge2D> aEdges) {
        QuadEdge2D e;
        ListIterator<QuadEdge2D> iter = aEdges.listIterator();
        while (iter.hasNext()) {
            e = iter.next();
            iter.set(e.Sym());
        }
    }

    public int countEdgesLeaving(KPoint2D pA) {
        int n = 0;
//        QuadEdge2D e = null;
//        Iterator<QuadEdge2D> iter = aMesh.iterator();
//        for (boolean sym = false; iter.hasNext() || sym; sym = !sym) {
//            e = sym ? e.Sym() : iter.next();
        for (QuadEdge2D e : new EdgeSymIterator<QuadEdge2D>(aMesh)) {
            if (e.getOrg().equals(pA)) {
                n++;
            }
        }
        return n;
    }

    public int countEdgesEntering(KPoint2D pA) {
        int n = 0;
//        QuadEdge2D e = null;
//        Iterator<QuadEdge2D> iter = aMesh.iterator();
//        for (boolean sym = false; iter.hasNext() || sym; sym = !sym) {
//            e = sym ? e.Sym() : iter.next();
        for (QuadEdge2D e : new EdgeSymIterator<QuadEdge2D>(aMesh)) {
            if (e.getDest().equals(pA)) {
                n++;
            }
        }
        return n;
    }
    //**********************************************************************
    //**  DEBUG DRAWING OF MESH
    //**********************************************************************
    protected ImagePlus impDebug = null;
    protected long delayTimeMS = 0;
    protected List<QuadEdge2D> debugMesh = null;
    KPoint2D ptScale = new KPoint2D(1, 1);
    final KPoint2D ptOff = new KPoint2D(0.5, 0.5);

    public void setDebugImage(ImagePlus imp, long delayTimeMS, double scale) {
        impDebug = imp;
        this.delayTimeMS = delayTimeMS;
        debugMesh = null;
        impDebug.setRoi(new DebugRoi(impDebug));
        ptScale.set(scale, scale);
        impDebug.updateAndRepaintWindow();
    }

    public void clearDebugImage() {
        debugMesh = null;
        if (impDebug != null) {
            impDebug.setRoi((Roi) null);
            impDebug.updateAndDraw();
        }
        impDebug = null;
    }

    protected class DebugRoi extends Roi {

        protected DebugRoi(ImagePlus imp) {
            super(0, 0, imp.getWidth(), imp.getHeight());
        }

        @Override
        public void draw(java.awt.Graphics graphics) {
            if (impDebug == null || debugMesh == null) {
                return;
            }
            Graphics2D g2 = (Graphics2D) graphics;
            
            AffineTransform at = new AffineTransform();
            double dMag = ic.getMagnification();
            Rectangle srcRect = ic.getSrcRect();
            g2.setFont(new Font("Arial",Font.PLAIN,5));
            g2.setColor(Color.WHITE);
            g2.fillRect(srcRect.x, srcRect.y, srcRect.width, srcRect.height);
            at.scale(dMag, dMag);
            at.translate(-srcRect.x, -srcRect.y);
            g2.setTransform(at);
            for (QuadEdge2D e : debugMesh) {
                e.draw(g2, ptScale, ptOff);
            }
        }
    }

    protected void updateDebugImage(List<QuadEdge2D> mesh) {
        if (impDebug != null) {
            debugMesh = mesh;
            impDebug.updateAndDraw();
            try {
                Thread.sleep(delayTimeMS);
            } catch (InterruptedException ex) {
            }
        }
    }
}
