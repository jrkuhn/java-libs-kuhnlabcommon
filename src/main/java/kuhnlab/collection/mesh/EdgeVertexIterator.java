/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package kuhnlab.collection.mesh;

import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;

/** Iterates counter-clockwise through all of the edges connected to the origin
 *  vertex of this edge.
 *
 * @author jrkuhn
 */
public class EdgeVertexIterator<T extends Edge> implements Iterable<T> {

    protected T start;
    protected T current;
    protected boolean hasNext;
    public EdgeVertexIterator(T start) {
        this.start = this.current = start;
        hasNext = true;
    }

    public Iterator<T> iterator() {
        return new Iterator() {

            public boolean hasNext() {
                return hasNext;
            }

            public Object next() {
                if (!hasNext) {
                    throw new NoSuchElementException();
                }
                T old = current;
                current = (T)current.ONext();
                if (current == start) {
                    hasNext = false;
                }
                return old;
            }

            public void remove() {
                throw new UnsupportedOperationException();
            }

        };
    }
    
}
