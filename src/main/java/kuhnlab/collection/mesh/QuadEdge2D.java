package kuhnlab.collection.mesh;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.geom.CubicCurve2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.QuadCurve2D;
import kuhnlab.coordinates.KPoint2D;
import kuhnlab.coordinates.KPolygon2D;

/**
 * An edge based on the QuadEdge data structure of Guibas and Stolfi (1985). 
 * Primitives for the Manipulation of General Subdivisions and the Computation 
 * of Voronoi Diagrams. ACM Transactions on Graphics, Vol. 4, No. 2, 
 * April 1985, Pages 74-123.
 * 
 * @author jrkuhn
 */
public class QuadEdge2D implements Edge {

    static public boolean bShowNext = true;
    static public boolean bShowOutside = true;
    static final double HALF_EDGE_LENGTH = 6;
    // faces are classified based on the number of unconstrained edges
    
    public EdgeType type = EdgeType.INTERNAL;
    
    public boolean bMark = false;
    public boolean bConstrained = false;
    public boolean bOutside = false;
    public KPoint2D vData = null;
    public QuadEdge2D eParent = null;
    public QuadEdge2D eNext = null;
    public QuadEdge2D eDual = null;
    public double value = 0;
    
    public enum EdgeID {
        E, ERot, ESym, EInvRot;
    };
    public EdgeID   debugEdgeID;
    public long     debugID;
    protected static long nextDebugID = 1;
    //=======================================================================
    //=================== Access to topological info ========================
    //=======================================================================
    /** Return the dual of the current edge, directed from its right to its left. */
    public QuadEdge2D Rot() {
        return eDual;
    }

    /** Return the edge from the destination to the origin of the current edge. */
    public QuadEdge2D Sym() {
        return eDual.eDual;
    }

    /** Return the dual of the current edge, directed from its left to its right. */
    public QuadEdge2D InvRot() {
        return Sym().Rot();
    }

    /** Return the next ccw edge around (from) the origin of the current edge */
    public QuadEdge2D ONext() {
        return eNext;
    }

    /** Return the next cw edge around (from) the origin of the current edge. */
    public QuadEdge2D OPrev() {
        return Rot().ONext().Rot();
    }

    /** Return the next ccw edge around (into) the destination of the current edge. */
    public QuadEdge2D DNext() {
        return Sym().ONext().Sym();
    }

    /** Return the next cw edge around (into) the destination of the current edge. */
    public QuadEdge2D DPrev() {
        return InvRot().ONext().InvRot();
    }

    /** Return the ccw edge around the left face following the current edge. */
    public QuadEdge2D LNext() {
        return InvRot().ONext().Rot();
    }

    /** Return the ccw edge around the left face before the current edge. */
    public QuadEdge2D LPrev() {
        return ONext().Sym();
    }

    /** Return the edge around the right face ccw following the current edge. */
    public QuadEdge2D RNext() {
        return Rot().ONext().InvRot();
    }

    /** Return the edge around the right face ccw before the current edge. */
    public QuadEdge2D RPrev() {
        return Sym().ONext();
    }
    //=======================================================================
    //==================== Access to non-topological info ===================
    //=======================================================================
    public KPoint2D getOrg() {
        return vData;
    }

    public KPoint2D getDest() {
        return Sym().vData;
    }

    public KPoint2D setOrg(KPoint2D a) {
        vData = a;
        return a;
    }

    public KPoint2D setDest(KPoint2D a) {
        Sym().vData = a;
        return a;
    }

    public void setEndPoints(KPoint2D origin, KPoint2D destination) {
        vData = origin;
        Sym().vData = destination;
    }

    public KPoint2D getMidpoint() {
        return (KPoint2D.newSumOf(getOrg(), getDest())).scale(0.5);
    }

    public double getLength() {
        return KPoint2D.distBetween(getOrg(), getDest());
    }

    public boolean isConstrained() {
        return bConstrained;
    }

    public void constrain() {
        bConstrained = true;
        Rot().bConstrained = true;
        Sym().bConstrained = true;
        InvRot().bConstrained = true;
    }

    public void unconstrain() {
        bConstrained = false;
        Rot().bConstrained = false;
        Sym().bConstrained = false;
        InvRot().bConstrained = false;
    }

    public boolean isMarked() {
        return bMark;
    }

    public void setMark() {
        bMark = true;
        Rot().bMark = true;
        Sym().bMark = true;
        InvRot().bMark = true;
    }

    public void clearMark() {
        bMark = false;
        Rot().bMark = false;
        Sym().bMark = false;
        InvRot().bMark = false;
    }
    
    public void clearAllOutside() {
        bOutside = false;
        Rot().bOutside = false;
        Sym().bOutside = false;
        InvRot().bOutside = false;
    }
    
    public void setAllOutside() {
        bOutside = true;
        Rot().bOutside = true;
        Sym().bOutside = true;
        InvRot().bOutside = true;
    }
    
    public void setType(EdgeType orgType, EdgeType destType) {
        type = orgType;
        Sym().type = destType;
    }

    public void setParents(QuadEdge2D eOrg, QuadEdge2D eDest) {
        eParent = eOrg;
        Sym().eParent = eDest;
    }

    public void clearParents() {
        eParent = null;
        Rot().eParent = null;
        Sym().eParent = null;
        InvRot().eParent = null;
    }

    //=======================================================================
    //=========================== edge operators ============================
    //=======================================================================
    protected static void connectQuadEdge(QuadEdge2D[] eArray) {
        eArray[0].eDual = eArray[1];
        eArray[1].eDual = eArray[2];
        eArray[2].eDual = eArray[3];
        eArray[3].eDual = eArray[0];

        eArray[0].eNext = eArray[0];
        eArray[1].eNext = eArray[3];
        eArray[2].eNext = eArray[2];
        eArray[3].eNext = eArray[1];
        
        eArray[0].debugEdgeID = EdgeID.E;
        eArray[1].debugEdgeID = EdgeID.ERot;
        eArray[2].debugEdgeID = EdgeID.ESym;
        eArray[3].debugEdgeID = EdgeID.EInvRot;

        eArray[0].debugID = nextDebugID;
        eArray[1].debugID = nextDebugID;
        eArray[2].debugID = nextDebugID;
        eArray[3].debugID = nextDebugID;
        nextDebugID++;
        
//        if (eArray[0].debugID == 600) {
//            return;
//        }
        
    }

    /** Create four edges and link them together into a single quadedge structure. */
    public static QuadEdge2D makeEdge(boolean constrained) {
        QuadEdge2D[] eArray = {new QuadEdge2D(), new QuadEdge2D(), new QuadEdge2D(), new QuadEdge2D()};
        
        QuadEdge2D e = eArray[0];
        connectQuadEdge(eArray);
        if (constrained) {
            e.constrain();
        }
        return e;
    }

    /** Create four linked edges and set the endpoints. */
    public static QuadEdge2D makeEdge(KPoint2D a, KPoint2D b, boolean constrained) {
//        if (a.epsilonEquals(45.1, 133.0, .2) || a.epsilonEquals(45.1, 133.0, .2)) {
//            IJ.log("found debug insertEdge");
//        }
        QuadEdge2D e = makeEdge(constrained);
        e.setOrg(a);
        e.setDest(b);
//        if (a.epsilonEquals(1.419, 14.444, 0.01) && b.epsilonEquals(1.419, 14.444, 0.01)) {
//            return eArray;
//        }
        return e;
    }

    /** This operator affects the two edge rings around the origins of a and b,
     *  and, independently, the two edge rings around the left faces of a and b.
     *  In each case, (i) if the two rings are distinct, Splice will combine
     *	them into one; (ii) if the two are the same ring, Splice will break it
     *  into two separate pieces.
     *  Thus, Splice can be used both o attach the two edges together, and
     *	to break them apart. See Guibas and Stolfi (1985) p. 96 for more details
     *  and illustrations.	 */
    static void splice(QuadEdge2D a, QuadEdge2D b) {
        QuadEdge2D alpha = a.ONext().Rot();
        QuadEdge2D beta = b.ONext().Rot();

        QuadEdge2D t1 = b.ONext();
        QuadEdge2D t2 = a.ONext();
        QuadEdge2D t3 = beta.ONext();
        QuadEdge2D t4 = alpha.ONext();

        a.eNext = t1;
        b.eNext = t2;
        alpha.eNext = t3;
        beta.eNext = t4;
    }

    /** Detach quad edge from the rest of the subdivision.  */
    public void detach() {
        splice(this, this.OPrev());
        splice(this.Sym(), (this.Sym()).OPrev());
    }

    /** Add a new quad-edge eArray connecting the destination of 'a' to the origin 
     *  of 'b', in such a way that aLeft = eLeft = bLeft after the connection
     *  is complete. */
    public static QuadEdge2D connect(QuadEdge2D a, QuadEdge2D b, boolean constrained) {
        QuadEdge2D e = makeEdge(a.getDest(), b.getOrg(), constrained);
        splice(e, a.LNext());
        splice(e.Sym(), b);
        return e;
    }

    /** Given an edge 'eArray' whose left and right faces are triangles, the problem
     *	is to delete 'eArray' and connect the other two vertices of the quadrilateral
     *	thus formed. The first pair of splices disconnects eArray from the edge
     *	structure, and leaves it as the single edge of a separate spherical
     *	component. The last two splices connect 'eArray' again at the required 
     *	position. */
    public void flip() {
        // NOTE 'this' edge is the edge 'eArray' in the above discussion.
        QuadEdge2D a = this.OPrev();
        QuadEdge2D b = (this.Sym()).OPrev();
        // disconnect this from the edge structure
        splice(this, a);
        splice(this.Sym(), b);
        // reconnect this in the new place.
        splice(this, a.LNext());
        splice(this.Sym(), b.LNext());

        setOrg(a.getDest());
        setDest(b.getDest());
    }

    /** returns true if the point x is strictly to the right of the edge eArray. */
    public static boolean isRightOf(KPoint2D x, QuadEdge2D e) {
        return KPoint2D.isCounterClockwise(x, e.getDest(), e.getOrg());
    }

    /** returns true if the point x is strictly to the left of the edge eArray. */
    public static boolean isLeftOf(KPoint2D x, QuadEdge2D e) {
        return KPoint2D.isCounterClockwise(x, e.getOrg(), e.getDest());
    }

    /** returns true if the edge is strictly to the right of the point p. */
    public boolean isRightOf(KPoint2D x) {
        return isLeftOf(x, this);
    }

    /** returns true if the edge is strictly to the left of the point p. */
    public boolean isLeftOf(KPoint2D x) {
        return isRightOf(x, this);
    }

    public boolean equals(KPoint2D org, KPoint2D dest) {
        return (getOrg().equals(org) && getDest().equals(dest));
    }

    public boolean equals(QuadEdge2D e) {
        return equals(e.getOrg(), e.getDest());
    }

    /** Test if this edge intersects the line between the points C, D */
    public boolean intersects(KPoint2D C, KPoint2D D) {
        return KPoint2D.intersects(this.getOrg(), this.getDest(), C, D);
    }

    /** Test if this edge intersects another edge */
    public boolean intersects(QuadEdge2D e) {
        return KPoint2D.intersects(this.getOrg(), this.getDest(), e.getOrg(), e.getDest());
    }

    /** find the intersection point between this edge and the line between the points C, D */
    public KPoint2D findIntersection(KPoint2D C, KPoint2D D) {
        return (new KPoint2D()).toIntersectionOf(getOrg(), getDest(), C, D);
    }

    /** find the intersection point between this edge and another */
    public KPoint2D findIntersection(QuadEdge2D e) {
        return findIntersection(e.getOrg(), e.getDest());
    }
    
    @Override
    public String toString() {
        String msg = debugEdgeID.name() + "_" + debugID;
        msg += vData == null ? "(null)" : vData.toString(2);
        msg += "->" + eNext.debugEdgeID.name() + "_" + eNext.debugID;
        return msg;
    }
    
    public boolean isInDebugList(int[] debugIDs) {
        for (int i : debugIDs) {
            if (i == debugID) {
                return true;
            }
        }
        return false;
    }

    //=======================================================================
    //=========================== face operators ============================
    //=======================================================================
    /** Count the number of edges connected to this edge by counting the
     *	number of ONext edges */
    public int countONext() {
        int n = 0;
        for (QuadEdge2D e : new EdgeVertexIterator<QuadEdge2D>(this)) {
            n++;
        }
//        QuadEdge2D e = this;
//        int n = 0;
//        do {
//            n++;
//            e = e.ONext();
//        } while (e != this);
        return n;
    }
    
    public int countConstrainedONext() {
        int n = 0;
        for (QuadEdge2D e : new EdgeVertexIterator<QuadEdge2D>(this)) {
            if (e.isConstrained()) {
                n++;
            }
        }
//        QuadEdge2D e = this;
//        int n = 0;
//        do {
//            if (e.isConstrained())
//                n++;
//            e = e.ONext();
//        } while (e != this);
        return n;
    }
    
    public void markAllONext() {
        for (QuadEdge2D e : new EdgeVertexIterator<QuadEdge2D>(this)) {
            e.bMark = true;
        }
//        QuadEdge2D e = this;
//        do {
//            e.bMark = true;
//            e = e.ONext();
//        } while (e != this);
    }

    /** Calculate the centroid of a face (modified from "Graphics Gems IV"). */
    public KPoint2D getFaceCentroid() {
        QuadEdge2D e = this;
        QuadEdge2D ePrev = OPrev();
        KPoint2D p, pPrev;
        double ai, atmp = 0, xtmp = 0, ytmp = 0;
        int n = 0;
        do {
            n++;
            p = e.Rot().getOrg();
            if (p == null)
                return KPoint2D.ZERO;
            pPrev = ePrev.Rot().getOrg();
            if (pPrev == null)
                return KPoint2D.ZERO;
            ai = pPrev.x * p.y - p.x * pPrev.y;
            atmp += ai;
            xtmp += (p.x + pPrev.x) * ai;
            ytmp += (p.y + pPrev.y) * ai;

            ePrev = e;
            e = e.ONext();
        } while (e != this);

        if (n < 3) {
            return null;
        }

        if (atmp != 0) {
            return new KPoint2D(xtmp / (3 * atmp), ytmp / (3 * atmp));
        }
        return null;
    }
    
    public KPolygon2D getFacePolygon() {
        KPolygon2D poly = new KPolygon2D(true);
        QuadEdge2D e = this;
        KPoint2D p;
        do {
            p = e.Rot().getOrg();
            poly.add(p);
            e = e.ONext();
        } while (e != this);

        return poly;
    }
    

    /** Calculate the area of a face. */
    public double getSignedFaceArea() {
        QuadEdge2D e = this;
        QuadEdge2D ePrev = OPrev();
        KPoint2D p, pPrev;
        double dSum = 0;
        double ai, atmp = 0, xtmp = 0, ytmp = 0;
        int n = 0;
        do {
            n++;
            p = e.Rot().getOrg();
            if (p == null)
                return Double.NaN;
            pPrev = ePrev.Rot().getOrg();
            if (pPrev == null)
                return Double.NaN;
            dSum += pPrev.x * p.y - p.x * pPrev.y;
            ePrev = e;
            e = e.ONext();
        } while (e != this);

        if (n < 3) {
            return 0;
        }

        return dSum / 2;
    }

    /** Count the number of edges in the face. */
    public int countFaceEdges() {
        QuadEdge2D e = this;
        int n = 0;
        do {
            n++;
            e = e.ONext();
        } while (e != this);

        return n;
    }


    //=======================================================================
    //================================ Drawing ==============================
    //=======================================================================
    public KPoint2D getQEPoint() {
        KPoint2D n;
        KPoint2D c;
        if (getOrg() == null) {
            c = Rot().getMidpoint();
            n = KPoint2D.newDiffOf(Rot().getOrg(), Rot().getDest());
            n.toPerpOf(n);
        } else {
            c = getMidpoint();
            n = KPoint2D.newDiffOf(getDest(), getOrg());
        }
        n.norm();
        n.scale(HALF_EDGE_LENGTH);
        c.sub(n);
        return c;
    }

    public void draw(Graphics2D g, KPoint2D pScale, KPoint2D pOff) {
        draw(g, pScale, pOff, Color.black);
    }

    public void draw(Graphics2D g, KPoint2D pScale, KPoint2D pOff, Color c) {

        KPoint2D pA = KPoint2D.newXformOf(getOrg(), pScale, pOff);
        KPoint2D pB = KPoint2D.newXformOf(getDest(), pScale, pOff);
        KPoint2D pC = null;
        if (pA == null || pB == null) {
            return;
        }
        KPoint2D circleR = new KPoint2D(2.5, 2.5);
        BasicStroke thinStroke = new BasicStroke(0.5f);
        BasicStroke thickStroke = new BasicStroke(3f);
        java.util.Random rand = new java.util.Random();


        // draw a mark if the face is outside
        if (bShowOutside && Rot().bOutside) {
            g.setPaint(Color.red);
            //pC = Rot().getQEPoint();
            //circle.setFrameFromCenter(pC, KPoint2D.add(pC, circleR));
            //g.fill(circle);
            pC = KPoint2D.newXformOf(Rot().getQEPoint(), pScale, pOff);
            Polygon poly = new Polygon();
            poly.addPoint((int) pA.x, (int) pA.y);
            poly.addPoint((int) pB.x, (int) pB.y);
            poly.addPoint((int) pC.x, (int) pC.y);
            g.fill(poly);
        }
        if (bShowOutside && InvRot().bOutside) {
            g.setPaint(Color.red);
            //pC = InvRot().getQEPoint();
            //circle.setFrameFromCenter(pC, KPoint2D.add(pC, circleR));
            //g.fill(circle);
            pC = KPoint2D.newXformOf(InvRot().getQEPoint(), pScale, pOff);
            Polygon poly = new Polygon();
            poly.addPoint((int) pA.x, (int) pA.y);
            poly.addPoint((int) pB.x, (int) pB.y);
            poly.addPoint((int) pC.x, (int) pC.y);
            g.fill(poly);
        }


        // draw the origin and destination points and a line between them
        if (isConstrained()) {
            if (bShowNext) {
                g.setStroke(thinStroke);
                g.setPaint(Color.black);
            } else {
                g.setStroke(thickStroke);
                g.setPaint(c);
            }
        } else {
            g.setStroke(thinStroke);
            if (bShowNext) {
                g.setPaint(Color.lightGray);
            } else {
                g.setPaint(Color.lightGray);
            //Color cBright = c;
            //for (int i=0; i<13; i++)
            //		cBright = cBright.brighter();
            //g.setPaint(cBright);
            }
        }
        g.draw(new Line2D.Double(pA.toPointShape(), pB.toPointShape()));
        Ellipse2D.Double circle = new Ellipse2D.Double();

        KPoint2D cr;

        // draw the endpoints		
        if (type == EdgeType.END) {
            cr = KPoint2D.newProdOf(1, circleR);
        } else if (type == EdgeType.JUNCTION) {
            cr = KPoint2D.newProdOf(2, circleR);
        } else {
            cr = null;
        }
        if (cr != null) {
            circle.setFrameFromCenter(pA.toPointShape(), KPoint2D.newSumOf(pA, cr).toPointShape());
            g.setPaint(c);
            g.fill(circle);
        }

        if (Sym().type == EdgeType.END) {
            cr = KPoint2D.newProdOf(1, circleR);
        } else if (Sym().type == EdgeType.JUNCTION) {
            cr = KPoint2D.newProdOf(2, circleR);
        } else {
            cr = null;
        }
        if (cr != null) {
            circle.setFrameFromCenter(pB.toPointShape(), KPoint2D.newSumOf(pB, cr).toPointShape());
            g.setPaint(c);
            g.fill(circle);
        }

        // show any marked edges
        QuadEdge2D m = this;
        for (int i = 0; i < 4; i++) {
            if (m.bMark) {
                pC = KPoint2D.newXformOf(m.getQEPoint(), pScale, pOff);
                circle.setFrameFromCenter(pC.toPointShape(), KPoint2D.newSumOf(pC, circleR).toPointShape());
                g.setPaint(Color.blue);
                g.fill(circle);
            }
            m = m.Rot();
        }

        KPoint2D pCent = KPoint2D.newXformOf(getMidpoint(), pScale, pOff);
        KPoint2D pAB = KPoint2D.newDiffOf(pB, pCent);
        pAB.norm();
        pAB.scale(HALF_EDGE_LENGTH);
        KPoint2D pABDual = KPoint2D.newPerpOf(pAB);
        KPoint2D pE, pNE, pArrow;
        CubicCurve2D.Double cc = new CubicCurve2D.Double();
        QuadCurve2D.Double qc = new QuadCurve2D.Double();
        g.drawString(""+debugID, (int)pCent.x+2+debugID%5, (int)pCent.y+2+debugID%5);

        if (bShowNext) {

            // draw the "next" curves
            g.setStroke(thinStroke);

            pE = KPoint2D.newXformOf(getQEPoint(), pScale, pOff);
            pNE = KPoint2D.newXformOf(ONext().getQEPoint(), pScale, pOff);
            qc.setCurve(pE.toPointShape(), pA.toPointShape(), pNE.toPointShape());
            if (c == Color.black) {
                g.setColor(Color.blue);
            } else {
                g.setColor(Color.lightGray);
            }
            g.draw(qc);
            pArrow = KPoint2D.newDiffOf(pA, pNE).norm().scale(HALF_EDGE_LENGTH);
            g.draw(new Line2D.Double(pNE.toPointShape(), KPoint2D.newSumOf(pNE, KPoint2D.newSumOf(pArrow, KPoint2D.newProdOf(1 / 3.0, KPoint2D.newPerpOf(pArrow)))).toPointShape()));

            pE = KPoint2D.newXformOf(Rot().getQEPoint(), pScale, pOff);
            pNE = KPoint2D.newXformOf((Rot().ONext()).getQEPoint(), pScale, pOff);
            cc.setCurve(pE.toPointShape(), pA.toPointShape(), pA.toPointShape(), pNE.toPointShape());
            if (c == Color.black) {
                g.setColor(Color.red);
            } else {
                g.setColor(Color.lightGray);
            }
            g.draw(cc);
            pArrow = KPoint2D.newDiffOf(pA, pNE).norm().scale(HALF_EDGE_LENGTH);
            g.draw(new Line2D.Float(pNE.toPointShape(), KPoint2D.newSumOf(pNE, KPoint2D.newSumOf(pArrow, KPoint2D.newProdOf(1 / 3.0, KPoint2D.newPerpOf(pArrow)))).toPointShape()));

            pE = KPoint2D.newXformOf(Sym().getQEPoint(), pScale, pOff);
            pNE = KPoint2D.newXformOf((Sym().ONext()).getQEPoint(), pScale, pOff);
            qc.setCurve(pE.toPointShape(), pB.toPointShape(), pNE.toPointShape());
            if (c == Color.black) {
                g.setColor(Color.blue);
            } else {
                g.setColor(Color.lightGray);
            }
            g.draw(qc);
            pArrow = KPoint2D.newDiffOf(pB, pNE).norm().scale(HALF_EDGE_LENGTH);
            g.draw(new Line2D.Float(pNE.toPointShape(), KPoint2D.newSumOf(pNE, KPoint2D.newSumOf(pArrow, KPoint2D.newProdOf(1 / 3.0, KPoint2D.newPerpOf(pArrow)))).toPointShape()));

            pE = KPoint2D.newXformOf((Sym().Rot()).getQEPoint(), pScale, pOff);
            pNE = KPoint2D.newXformOf((Sym().Rot().ONext()).getQEPoint(), pScale, pOff);
            cc.setCurve(pE.toPointShape(), pB.toPointShape(), pB.toPointShape(), pNE.toPointShape());
            if (c == Color.black) {
                g.setColor(Color.red);
            } else {
                g.setColor(Color.lightGray);
            }
            g.draw(cc);
            pArrow = KPoint2D.newDiffOf(pB, pNE).norm().scale(HALF_EDGE_LENGTH);
            g.draw(new Line2D.Float(pNE.toPointShape(), KPoint2D.newSumOf(pNE, KPoint2D.newSumOf(pArrow, KPoint2D.newProdOf(1 / 3.0, KPoint2D.newPerpOf(pArrow)))).toPointShape()));

            // draw the QuadEdge cross		

            g.setStroke(thickStroke);
            g.setPaint(c);
            g.draw(new Line2D.Float(pCent.toPointShape(), KPoint2D.newDiffOf(pCent, pABDual).toPointShape()));
            g.draw(new Line2D.Float(pCent.toPointShape(), KPoint2D.newSumOf(pCent, pABDual).toPointShape()));
            g.draw(new Line2D.Float(pCent.toPointShape(), KPoint2D.newSumOf(pCent, pAB).toPointShape()));
            g.draw(new Line2D.Float(pCent.toPointShape(), KPoint2D.newDiffOf(pCent, pAB).toPointShape()));

            KPoint2D pFirst = KPoint2D.newDiffOf(pCent, pAB);
            circle.setFrameFromCenter(pFirst.toPointShape(), KPoint2D.newSumOf(pFirst, circleR).toPointShape());
            g.setStroke(thinStroke);
            g.setPaint(Color.white);
            g.fill(circle);
            g.setPaint(c);
            g.draw(circle);
            
        }
    }
}

