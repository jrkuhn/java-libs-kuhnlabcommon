/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package kuhnlab.collection.mesh;

/**
 *
 * @author jrkuhn
 */
public enum EdgeType {
    UNKNOWN, ISOLATED, END, INTERNAL, JUNCTION
}
