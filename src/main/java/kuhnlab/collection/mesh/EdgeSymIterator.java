/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package kuhnlab.collection.mesh;

import java.util.Collection;
import java.util.Iterator;

/** Iterates through a collection of edges AND their Sym edges.
 *  Used to wrap a list of edges such that one can search both
 *  the list of edges and the sym of each edge.
 *
 * @author jrkuhn
 */
public class EdgeSymIterator<T extends Edge> implements Iterable<T> {

    protected Collection<T> edges;
    protected Iterator<T> iter;
    protected boolean sym;
    protected T current;
    public EdgeSymIterator(Collection<T> edges) {
        this.edges = edges;
        this.iter = edges.iterator();
        this.sym = false;
        this.current = null;
    }

    public Iterator<T> iterator() {
        return new Iterator() {

            public boolean hasNext() {
                return iter.hasNext() || sym;
            }

            public Object next() {
                if (sym) {
                    current = (T) current.Sym();
                } else {
                    current = iter.next();
                }
                sym = !sym;
                return current;
            }

            public void remove() {
                if (!edges.remove(current)) {
                    edges.remove(current.Sym());
                }
            }
        };
    }
}
