package kuhnlab.collection.mesh;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;
import java.util.*;
import kuhnlab.coordinates.KPoint2D;
import kuhnlab.coordinates.KPoint3D;

public class BranchEdge3D extends HalfEdge3D {

    public static int iNextID = 1;
    public static Random rand = new Random();
    int iID;
    int nCount = 0;
    public Color color;

    double dAngle = 0;
    List<BranchEdge3D> aeAfter = null;
    KPoint3D pBoundMin = new KPoint3D(Float.MAX_VALUE, Float.MAX_VALUE, Float.MAX_VALUE);
    KPoint3D pBoundMax = new KPoint3D(Float.MIN_VALUE, Float.MIN_VALUE, Float.MIN_VALUE);
    public HalfEdge3D sStartSegment;
    
    @Override
    public BranchEdge3D Sym() {
        return (BranchEdge3D)super.Sym();
    }
    
    @Override
    public BranchEdge3D ONext() {
        return (BranchEdge3D)super.ONext();
    }

    public void setID(int iID) {
        this.iID = iID;
        Sym().iID = iID;
    }

    public void setColor(Color c) {
        this.color = c;
        Sym().color = c;
    }

    public void clearCount() {
        this.nCount = 0;
        Sym().nCount = 0;
    }

    public void increaseCount() {
        this.nCount++;
        Sym().nCount++;
    }

    static public void clearAllCounts(List<BranchEdge3D> bc) {
        BranchEdge3D e;
        ListIterator<BranchEdge3D> iter = bc.listIterator();
        while (iter.hasNext()) {
            e = iter.next();
            e.clearCount();
        }
    }

    public void addAfter(BranchEdge3D e) {
        aeAfter.add(e);
    }

    public static BranchEdge3D makeBranch() {
        BranchEdge3D[] e = {new BranchEdge3D(), new BranchEdge3D()};
        connectDualEdge(e);
        e[0].setID(iNextID++);
        e[0].aeAfter = new ArrayList<BranchEdge3D>();
        e[1].aeAfter = e[0].aeAfter;
        e[0].setColor(new Color(Color.HSBtoRGB(rand.nextFloat(), rand.nextFloat(), rand.nextFloat() * 0.5f + 0.5f)));
        return e[0];
    }

    public static BranchEdge3D makeBranch(QuadEdge2D eStart, float z, double dFollowDist, boolean bMarkEdge) {

        BranchEdge3D branch = makeBranch();
        BranchEdge3D branchsym = branch.Sym();

        branch.dLength = 0;
        branch.dWidth = 0;
        branch.dAngle = 0;

        double dSegWidth;
        int numBranchWidth = 0;
        int numSegWidth;

        QuadEdge2D e = eStart, eSym = eStart.Sym(), ePrev;
        HalfEdge3D s, sPrev = null;
        boolean bFirst = true;

        KPoint3D ptDest, ptOrg = new KPoint3D(e.getOrg(), z);

        //IJ.write("----New Branch----");

        int i = 0;

        do {
            ptDest = new KPoint3D(e.getDest(), z);
            s = makeSegment(ptOrg, ptDest);
            branch.updateBounds(ptOrg);
            branch.updateBounds(ptDest);
            if (bFirst) {
                branch.sStartSegment = s;
                branch.setOrg(ptOrg);
                branch.type = (e.type == EdgeType.JUNCTION) ? EdgeType.JUNCTION : EdgeType.END;
                bFirst = false;
            } else {
                HalfEdge3D.splice(sPrev.Sym(), s);
            }

            // calculate segment and branch length
            s.setLength(KPoint3D.distBetween(ptOrg, ptDest));
            branch.dLength += s.dLength;

            // calculate segment and branch width
            dSegWidth = 0;
            numSegWidth = 0;
            if (e.type == EdgeType.INTERNAL && e.eParent != null) {
                dSegWidth += e.eParent.Rot().getLength();
                numSegWidth += 1;
            }
            if (eSym.type == EdgeType.INTERNAL && eSym.eParent != null) {
                dSegWidth += eSym.eParent.Rot().getLength();
                numSegWidth += 1;
            }
            if (numSegWidth > 1) {
                dSegWidth /= 2;
            }
            s.setWidth(dSegWidth);
            if (numSegWidth > 0) {
                branch.dWidth += s.dWidth;
                numBranchWidth++;
            }

            //IJ.write(""+i+"\t from \t"+s.getOrg()+"\t to \t"+s.getDest()+" width "+IJ.d2s(s.dWidth,2)+" length "+IJ.d2s(s.dLength,2));
            i++;

            if (bMarkEdge) {
                e.setMark();
            }
            sPrev = s;
            ePrev = e;
            ptOrg = ptDest;
            if (eSym.type != EdgeType.INTERNAL) {
                break;
            }
            e = eSym.ONext();
            eSym = e.Sym();

        } while (e.ONext() != e && e != eStart);

        if (numBranchWidth > 0) {
            branch.dWidth /= numBranchWidth;
        }

        // we have reached the end of the branch
        branchsym.sStartSegment = sPrev.Sym();
        branchsym.setOrg(branchsym.sStartSegment.getOrg());
        branchsym.dLength = branch.dLength;
        branchsym.dWidth = branch.dWidth;
        branchsym.type = (ePrev.Sym().type == EdgeType.JUNCTION) ? EdgeType.JUNCTION : EdgeType.END;
        branchsym.pBoundMin = branch.pBoundMin;
        branchsym.pBoundMax = branch.pBoundMax;

        // Now calculate angles
        branch.dAngle = branch.calcAngleZ(dFollowDist);
        branchsym.dAngle = branchsym.calcAngleZ(dFollowDist);

        return branch;
    }

    protected double calcAngleZ(double dFollowDist) {
        double dW = (dWidth > 0) ? dWidth : 1;
        KPoint3D pt = sStartSegment.getLocation(dFollowDist * dW);
        pt.sub(sStartSegment.getOrg());
        double dAngle = Math.atan2(pt.y, pt.x);
        // make sure the angle is positive
        while (dAngle < 0) {
            dAngle += 2 * Math.PI;
        }
        return dAngle;
    }

    protected void updateBounds(KPoint3D p) {
        if (p.x < pBoundMin.x) {
            pBoundMin.x = p.x;
        }
        if (p.y < pBoundMin.y) {
            pBoundMin.y = p.y;
        }
        if (p.z < pBoundMin.z) {
            pBoundMin.z = p.z;
        }
        if (p.x > pBoundMax.x) {
            pBoundMax.x = p.x;
        }
        if (p.y > pBoundMax.y) {
            pBoundMax.y = p.y;
        }
        if (p.z > pBoundMax.z) {
            pBoundMax.z = p.z;
        }
    }

    public boolean isInBounds(KPoint3D p) {
        if ((p.x < pBoundMin.x) || (p.x > pBoundMax.x)) {
            return false;
        }
        if ((p.y < pBoundMin.y) || (p.y > pBoundMax.y)) {
            return false;
        }
        if ((p.z < pBoundMin.z) || (p.z > pBoundMax.z)) {
            return false;
        }
        return true;
    }

    static public List<BranchEdge3D> convertSkeleton(SkeletonMesh2D skel, float z, double dFollowDist) {
        List<BranchEdge3D> branches = new ArrayList<BranchEdge3D>();
        BranchEdge3D b;
//        QuadEdge2D eEdge = ((QuadEdge2D) skel.aMesh.get(0));
//        boolean bSym = false;
//        Iterator iter = skel.aMesh.iterator();
//        do {
//            // search for end or junction points
//            if (bSym) {
//                eEdge = eEdge.Sym();
//            } else {
//                eEdge = (QuadEdge2D) iter.next();
//            }
//            bSym = !bSym;
        for (QuadEdge2D eEdge : new EdgeSymIterator<QuadEdge2D>(skel.aMesh)) {
            if (eEdge.isMarked() || (eEdge.type == EdgeType.INTERNAL)) {
                // previously marked or is not an endpoint, junction, or isolated
                continue;
            }

            b = makeBranch(eEdge, z, dFollowDist, true);
            branches.add(b);
        }
//        } while (iter.hasNext() || bSym);

        SkeletonMesh2D.clearEdgeMarks(skel.aMesh);

        // connect all of the junctions
        updateJunctions(branches);
        return branches;
    }

    public void drawSegments(Graphics2D g, KPoint2D pScale, KPoint2D pOff, Color c,
            float fRadius, float fEndOffset) {
        HalfEdge3D s = sStartSegment;
        boolean first = true;
        do {
            first = false;
            s.draw(g, pScale, pOff, c, fRadius, fEndOffset);
            s = s.getNext();
        } while (s != null);
        sStartSegment.draw(g, pScale, pOff, Color.BLACK, fRadius, fEndOffset);
    }
    
    public double getBranchLength() {
        HalfEdge3D s = sStartSegment;
        double len = 0;
        do {
            len += s.getLength();
            s = s.getNext();
        } while (s != null);
        return len;
    }

    public void drawAngles(Graphics2D g, KPoint2D pScale, KPoint2D pOff, Color c,
            float fRadius, float fOff, double dFollowDist) {

        double dW = (dWidth > 0) ? dWidth : 1;
        double dMag = (pScale.x + pScale.y) * dFollowDist * dW / 2;
        BranchEdge3D sym = Sym();
        KPoint2D pOrg = KPoint2D.newXformOf(getOffsetOrg(fOff).projectZ(), pScale, pOff);
        KPoint2D pDest = KPoint2D.newXformOf(sym.getOffsetOrg(fOff).projectZ(), pScale, pOff);

        KPoint2D pAngle;
        BasicStroke thinStroke = new BasicStroke(2f);

        g.setStroke(thinStroke);
        g.setPaint(c);

        pAngle = KPoint2D.newPolar(dMag, dAngle);
        g.draw(new Line2D.Float(pOrg.toPointShape(), KPoint2D.newSumOf(pOrg, pAngle).toPointShape()));

        pAngle = KPoint2D.newPolar(dMag, sym.dAngle);
        g.draw(new Line2D.Float(pDest.toPointShape(), KPoint2D.newSumOf(pDest, pAngle).toPointShape()));

    }

    static public void updateJunctions(List<BranchEdge3D> branches) {

//        BranchEdge3D e = null;
//        Iterator<BranchEdge3D> iter = branches.iterator();
//        boolean bSym = false;
//        do {
//            e = bSym ? e.Sym() : iter.next();
//            bSym = !bSym;
        for (BranchEdge3D e : new EdgeSymIterator<BranchEdge3D>(branches)) {
            BranchEdge3D eTest = null;
            boolean bTestSym = false;
            Iterator<BranchEdge3D> testIter = branches.iterator();

            KPoint3D ptOrg = e.getOrg(), ptTestOrg;

            do {
                eTest = bTestSym ? eTest.Sym() : testIter.next();
                bTestSym = !bTestSym;

                ptTestOrg = eTest.getOrg();

                if (ptOrg.equals(ptTestOrg) && !e.isConnectedTo(eTest)) {
                    connectBest(e, eTest);
                    e.type = EdgeType.JUNCTION;
                    eTest.type = EdgeType.JUNCTION;
                    break;
                }

            } while (testIter.hasNext() || bTestSym);
        }
//        } while (iter.hasNext() || bSym);
    }

    static public void connectBest(BranchEdge3D eA, BranchEdge3D eB) {
        KPoint2D pOrg = new KPoint2D(0, 0);
        KPoint2D pBDest = KPoint2D.newPolar(1, eB.dAngle);
        KPoint2D pEDest, pENDest;
        BranchEdge3D e = eA;
        while (e.ONext() != eA) {
            pEDest = KPoint2D.newPolar(1, e.dAngle);
            pENDest = KPoint2D.newPolar(1, e.ONext().dAngle);
            if (KPoint2D.isCounterClockwise(pOrg, pEDest, pBDest) && KPoint2D.isClockwise(pOrg, pENDest, pBDest)) {
                splice(e, eB);
                return;
            }
            e = e.ONext();
        }
        // there is no better way to connect these edges
        splice(eA, eB);
    }

    static public BranchEdge3D combine(BranchEdge3D eA, BranchEdge3D eB) {
        // NOTE: eA and eB both start on the same point
        BranchEdge3D eNew = makeBranch();
        BranchEdge3D eNewSym = eNew.Sym();
        BranchEdge3D eASym = eA.Sym();
        BranchEdge3D eBSym = eB.Sym();

        // connect the segment chains
        splice(eA.sStartSegment, eB.sStartSegment);

        eNew.setOrg(eASym.getOrg());
        eNewSym.setOrg(eBSym.getOrg());
        eNew.sStartSegment = eASym.sStartSegment;
        eNewSym.sStartSegment = eBSym.sStartSegment;
        eNew.setLength(eA.dLength + eB.dLength);
        eNew.setWidth((eA.dWidth + eB.dWidth) / 2);
        eNew.dAngle = eASym.dAngle;
        eNewSym.dAngle = eBSym.dAngle;

        eNew.pBoundMin = new KPoint3D().toMinOf(eA.pBoundMin, eB.pBoundMin);
        eNew.pBoundMax = new KPoint3D().toMaxOf(eA.pBoundMax, eB.pBoundMax);
        eNewSym.pBoundMin = eNew.pBoundMin;
        eNewSym.pBoundMax = eNew.pBoundMax;

        return eNew;
    }

    static public void symAll(List<BranchEdge3D> bc) {
        BranchEdge3D e;
        ListIterator<BranchEdge3D> iter = bc.listIterator();
        while (iter.hasNext()) {
            e = iter.next();
            iter.set(e.Sym());
        }
    }

    static public void clearAllMarks(List<BranchEdge3D> bc) {
        BranchEdge3D e;
        ListIterator<BranchEdge3D> iter = bc.listIterator();
        while (iter.hasNext()) {
            e = iter.next();
            e.clearMark();
        }
    }

    static public void detachAll(List<BranchEdge3D> bc) {
        BranchEdge3D e;
        ListIterator<BranchEdge3D> iter = bc.listIterator();
        while (iter.hasNext()) {
            e = iter.next();
            splice(e.OPrev(), e);
            splice(e.Sym().OPrev(), e.Sym());
        }
    }

    static public boolean uncross(List<BranchEdge3D> branches, double dMaxCurve) {
        List<BranchEdge3D> aToAdd = new ArrayList<BranchEdge3D>();
        List<BranchEdge3D> aToRemove = new ArrayList<BranchEdge3D>();

        boolean bChanged = false;
        double dMinAngleDiff;
        double dAngleDiff;
        final double PI = Math.PI;
        final double TWOPI = 2 * PI;
        BranchEdge3D eTest;
        BranchEdge3D eMin;
//        BranchEdge3D e = null;
//        Iterator<BranchEdge3D> iter = branches.iterator();
//        boolean bSym = false;
//        do {
//            e = bSym ? e.Sym() : iter.next();
//            bSym = !bSym;
        for (BranchEdge3D e : new EdgeSymIterator<BranchEdge3D>(branches)) {
            if (e.isMarked()) {
                continue;
            }
            dMinAngleDiff = Double.MAX_VALUE;
            eMin = null;
            eTest = e.ONext();
            while (eTest != e) {
                // calculate the change in angle. PI is added to rotate 
                // one segment by 180 degrees
                dAngleDiff = eTest.dAngle + PI - e.dAngle;
                // make sure the angle difference is between -PI and PI
                while (dAngleDiff >= PI) {
                    dAngleDiff -= TWOPI;
                }
                while (dAngleDiff < -PI) {
                    dAngleDiff += TWOPI;
                //IJ.write("Angle difference = "+IJ.d2s(Math.toDegrees(dAngleDiff), 2));
                }
                dAngleDiff = Math.abs(dAngleDiff);

                if (dAngleDiff < dMinAngleDiff) {
                    // we have a new candidate
                    dMinAngleDiff = dAngleDiff;
                    eMin = eTest;
                }
                eTest = eTest.ONext();
            }
            // The minimum angle difference must still be under the maximum curvature
            if (eMin != e && dMinAngleDiff < dMaxCurve) {
                //IJ.write("Combining edges = "+IJ.d2s(Math.toDegrees(dMinAngleDiff), 2));
                // we found a cross-point. Remove it from the junction
                bChanged = true;
                e.setMark();
                eMin.setMark();
                aToAdd.add(combine(e, eMin));
                aToRemove.add(e);
                aToRemove.add(eMin);
                //IJ.write("Done combining");

                detachAll(aToRemove);
                branches.removeAll(aToRemove);
                symAll(aToRemove);
                branches.removeAll(aToRemove);

                branches.addAll(aToAdd);
                updateJunctions(branches);

                return true;

            // mark all of the edges in the junction (only do one cross at a time)
            //eTest = (BranchEdge3D)e.ONext();
            //while (eTest != e) {
            //	eTest.setMark();
            //	eTest = (BranchEdge3D)eTest.ONext();
            //}
            }
        }
//        } while (iter.hasNext() || bSym);

        //clearAllMarks(branches);

        return false;
    }
}