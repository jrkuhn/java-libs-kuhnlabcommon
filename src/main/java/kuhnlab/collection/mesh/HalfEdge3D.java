package kuhnlab.collection.mesh;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.QuadCurve2D;
import kuhnlab.coordinates.KPoint2D;
import kuhnlab.coordinates.KPoint3D;

//===========================================================================
//  class HalfEdge3D - an line segment based on a DualEdge data structure
//===========================================================================
public class HalfEdge3D implements Edge {

    static public boolean bShowNext = true;
    static public Color lightColor = new Color(192, 192, 192);
    
    public EdgeType type = EdgeType.UNKNOWN;
    public HalfEdge3D eNext = null;
    public HalfEdge3D eSym = null;
    public KPoint3D vData = null;
    public double dLength = 0;		// length of the segment
    public double dWidth = 0;		// width of the segment
    public boolean bMark = false;

    //=======================================================================
    //=================== Access to topological info ========================
    //=======================================================================
    /** Return the edge from the destination to the origin of the current edge. */
    public HalfEdge3D Sym() {
        return eSym;
    }

    /** Return the next ccw edge around (from) the origin of the current edge */
    public HalfEdge3D ONext() {
        return eNext;
    }

    /** Return the next ccw edge around (out of) the destination of the current edge.
     *  Used to iterate ccw around the leftward face.     */
    public HalfEdge3D LNext() {
        return Sym().ONext();
    }

    /** Return the next cw edge around (from) the origin of the current edge. */
    public HalfEdge3D OPrev() {
        HalfEdge3D e = this;
        while (e != null && e.eNext != this) {
            e = e.eNext;
        }
        return e;
    }

    /** Return the next ccw edge around (into) the destination of the current edge. */
    public HalfEdge3D DNext() {
        return Sym().ONext().Sym();
    }

    /** Return the next cw edge around (into) the destination of the current edge. */
    public HalfEdge3D DPrev() {
        return Sym().OPrev().Sym();
    }
    
    //=======================================================================
    //==================== Access to non-topological info ===================
    //=======================================================================
    public KPoint3D getOrg() {
        return vData;
    }

    public KPoint3D getDest() {
        return Sym().vData;
    }

    public KPoint3D setOrg(KPoint3D a) {
        vData = a;
        return a;
    }

    public KPoint3D setDest(KPoint3D a) {
        Sym().vData = a;
        return a;
    }

    public void setEndPoints(KPoint3D org, KPoint3D dest) {
        vData = org;
        Sym().vData = dest;
    }

    public void setLength(double d) {
        dLength = d;
        Sym().dLength = d;
    }

    public void setWidth(double d) {
        dWidth = d;
        Sym().dWidth = d;
    }

    public KPoint3D getMidpoint() {
        return KPoint3D.newSumOf(getOrg(), getDest()).scale(0.5);
    }

    public double getLength() {
        return KPoint3D.distBetween(getOrg(), getDest());
    }

    public boolean isMarked() {
        return bMark;
    }

    public void setMark() {
        bMark = true;
        Sym().bMark = true;
    }

    public void clearMark() {
        bMark = false;
        Sym().bMark = false;
    }

    //=======================================================================
    //=========================== edge operators ============================
    //=======================================================================
    protected static void connectDualEdge(HalfEdge3D[] e) {
        e[0].eSym = e[1];
        e[1].eSym = e[0];
        e[0].eNext = e[0];
        e[1].eNext = e[1];
    }

    /** Create two line segments and link them together into a single dualedge structure. */
    public static HalfEdge3D makeSegment() {
        HalfEdge3D[] e = {new HalfEdge3D(), new HalfEdge3D()};
        connectDualEdge(e);
        return e[0];
    }

    /** Create two linked edges and set the endpoints. */
    public static HalfEdge3D makeSegment(KPoint3D a, KPoint3D b) {
        HalfEdge3D e = makeSegment();
        e.setEndPoints(a, b);
        return e;
    }

    /** Splice together two edges that are not connected or detach two edges
     *	that are connected.
     */
    public static void splice(HalfEdge3D a, HalfEdge3D b) {
        HalfEdge3D t1 = a.ONext();
        HalfEdge3D t2 = b.ONext();

        b.eNext = t1;
        a.eNext = t2;
    }

    /** Project everything down on the X-Y plane and insert b into a's 
     *	ONext chain such that ONext chain goes in a counter-clockwise
     *	direction. */
    public static void connectCounterClockwise(HalfEdge3D a, HalfEdge3D b) {
        HalfEdge3D e = a;
        KPoint2D pBDest = b.getDest().projectZ();
        KPoint2D pEOrg, pEDest, pENDest;
        while (e.ONext() != a) {
            pEOrg = e.getOrg().projectZ();
            pEDest = e.getDest().projectZ();
            pENDest = e.ONext().getDest().projectZ();
            if (KPoint2D.isCounterClockwise(pEOrg, pEDest, pBDest) && KPoint2D.isClockwise(pEOrg, pENDest, pBDest)) {
                splice(e, b);
                return;
            }
            e = e.ONext();
        }
        // there is no better way to connect these edges
        splice(a, b);
    }

    /** Follow a branch for a certain distance and return the location of a 
     *	point on the branch line at that distance. */
    public KPoint3D getLocation(double dDistance) {
        KPoint3D pO, pD, pA, vOA, vN = new KPoint3D(0, 0, 0);
        double dTotalLength = 0;
        int n = 0;
        HalfEdge3D s = this;
        HalfEdge3D sSym = s.Sym();
        pO = s.getOrg();
        pD = s.getDest();

        // average the normalized direction vectors from the origin to each
        // endpoint to get the average line direction.
        while (true) {
            dTotalLength += s.dLength;
            if (dTotalLength > dDistance) {
                // this endpoint would put us above our distance
                break;
            }

            pA = s.getDest();
            vOA = KPoint3D.newDiffOf(pA, pO);
            vN.add(vOA);
            n++;

            if (sSym.ONext() == sSym || sSym.ONext() == s) {
                // we have reached the end of the line 
                break;
            }
            s = sSym.ONext();
            sSym = s.Sym();
        }

        if (n == 0) {
            //IJ.write("Follow distance = 0");
            return new KPoint3D(pD);
        }

        vN.norm();
        vN.scale(dDistance);
        vN.add(pO);
        return vN;
    }
    
    public double getAngle(double dAngleDist) {
        KPoint3D pt = getLocation(dAngleDist);
        pt.sub(getOrg());
        double dAngle = Math.atan2(pt.y, pt.x);
        // make sure the angle is positive
        while (dAngle < 0) {
            dAngle += 2 * Math.PI;
        }
        return dAngle;
    }

    public boolean hasNext() {
        return eSym.eNext != eSym;
    }

    public HalfEdge3D getNext() {
        return hasNext() ? eSym.eNext : null;
    }
    
    /** Count the number of edges connected to this edge by counting the
     *	number of ONext edges */
    public int countONext() {
        int n = 0;
        for (HalfEdge3D e : new EdgeVertexIterator<HalfEdge3D>(this)) {
            n++;
        }
//        HalfEdge3D e = this;
//        int n = 0;
//        do {
//            n++;
//            e = e.ONext();
//        } while (e != this);
        return n;
    }
    
    public boolean isConnectedTo(HalfEdge3D eTest) {
        HalfEdge3D e = this;
        do {
            if (e == eTest) {
                return true;
            }
            e = e.ONext();
        } while (e != this);
        return false;
    }
    
    protected void copyValuesFrom(HalfEdge3D e) {
        this.type = e.type;
        this.dLength = e.dLength;
        this.dWidth = e.dWidth;
        this.bMark = e.bMark;
    }
    

    //=======================================================================
    //================================ Drawing ==============================
    //=======================================================================
    public KPoint3D getOffsetOrg(float fOff) {
        KPoint3D v = KPoint3D.newDiffOf(getDest(), getOrg());
        v.norm();
        v.scale(fOff);
        v.add(getOrg());
        return v;
    }

    public void draw(Graphics2D g, KPoint2D pScale, KPoint2D pOff, float fRadius, float fEndOffset) {
        draw(g, pScale, pOff, Color.black, fRadius, fEndOffset);
    }

    public void draw(Graphics2D g, KPoint2D pScale, KPoint2D pOff, Color c, float fRadius, float fOff) {

        KPoint2D pOrg = KPoint2D.newXformOf(getOrg().projectZ(), pScale, pOff);
        KPoint2D pDest = KPoint2D.newXformOf(Sym().getOrg().projectZ(), pScale, pOff);
        KPoint2D pOffOrg = KPoint2D.newXformOf(getOffsetOrg(fOff).projectZ(), pScale, pOff);
        KPoint2D pOffDest = KPoint2D.newXformOf(Sym().getOffsetOrg(fOff).projectZ(), pScale, pOff);
        KPoint2D circleR = new KPoint2D(2.5, 2.5);
        KPoint2D endR = KPoint2D.newProdOf(fRadius, pScale);
        BasicStroke thinStroke = new BasicStroke(0.5f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
        BasicStroke thickStroke = new BasicStroke((float) (pScale.x + pScale.y) * fRadius, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);

        Ellipse2D.Float circle = new Ellipse2D.Float();

        KPoint2D pA, pB, pC, pMid, pArrow;

        // draw a line between the origin and destination points 
        if (bShowNext) {
            g.setPaint(Color.lightGray);
            g.setStroke(thinStroke);
        } else {
            g.setPaint(c);
            g.setStroke(thickStroke);
        }
        g.draw(new Line2D.Float(pOrg.toPointShape(), pDest.toPointShape()));

        // draw the endpoints
        g.setStroke(thinStroke);
        circle.setFrameFromCenter(pOrg.toPointShape(), KPoint2D.newSumOf(pOrg, circleR).toPointShape());
        g.setPaint(c);
        g.fill(circle);
        circle.setFrameFromCenter(pDest.toPointShape(), KPoint2D.newSumOf(pDest, circleR).toPointShape());
        g.setPaint(c);
        g.fill(circle);

        if (bShowNext) {
            // draw the DualEdge segment
            g.setStroke(thickStroke);
            g.setPaint(c);
            pA = KPoint2D.newXformOf(getOffsetOrg(fOff + fRadius).projectZ(), pScale, pOff);
            pB = KPoint2D.newXformOf(Sym().getOffsetOrg(fOff + fRadius).projectZ(), pScale, pOff);
            g.draw(new Line2D.Float(pA.toPointShape(), pB.toPointShape()));

            // draw the DualEdge endpoints			
            g.setStroke(thinStroke);
            circle.setFrameFromCenter(pOffOrg.toPointShape(), KPoint2D.newSumOf(pOffOrg, endR).toPointShape());
            g.setPaint(lightColor);
            g.fill(circle);
            g.setPaint(c);
            g.draw(circle);
            circle.setFrameFromCenter(pOffDest.toPointShape(), KPoint2D.newSumOf(pOffDest, endR).toPointShape());
            g.setPaint(Color.white);
            g.fill(circle);
            g.setPaint(c);
            g.draw(circle);

            // draw the "next" curves
            QuadCurve2D.Float cc = new QuadCurve2D.Float();

            // curve for forward segment			
            g.setStroke(thinStroke);
            pA = pOffOrg;
            pB = pOrg;
            pC = KPoint2D.newXformOf(ONext().getOffsetOrg(fOff - fRadius).projectZ(), pScale, pOff);
            pMid = KPoint2D.newSumOf(pA, pC).scale(0.5);
            cc.setCurve(pA.toPointShape(), pB.toPointShape(), pC.toPointShape());
            if (c == Color.black) {
                g.setColor(Color.blue);
            } else {
                g.setColor(Color.lightGray);
            }
            g.draw(cc);
            pArrow = KPoint2D.newDiffOf(pB, pC).norm().scale(6);
            g.draw(new Line2D.Float(pC.toPointShape(), KPoint2D.newSumOf(pC, KPoint2D.newSumOf(pArrow, KPoint2D.newProdOf(1 / 3.0, KPoint2D.newPerpOf(pArrow)))).toPointShape()));

            // curve for reverse (sym) segment
            pA = pOffDest;
            pB = pDest;
            pC = KPoint2D.newXformOf(Sym().ONext().getOffsetOrg(fOff - fRadius).projectZ(), pScale, pOff);
            pMid = KPoint2D.newSumOf(pA, pC).scale(0.5);
            cc.setCurve(pA.toPointShape(), pB.toPointShape(), pC.toPointShape());
            if (c == Color.black) {
                g.setColor(Color.blue);
            } else {
                g.setColor(Color.lightGray);
            }
            g.draw(cc);
            pArrow = KPoint2D.newDiffOf(pB, pC).norm().scale(6);
            g.draw(new Line2D.Float(pC.toPointShape(), KPoint2D.newSumOf(pC, KPoint2D.newSumOf(pArrow, KPoint2D.newProdOf(1 / 3.0, KPoint2D.newPerpOf(pArrow)))).toPointShape()));
        }
    }
}
