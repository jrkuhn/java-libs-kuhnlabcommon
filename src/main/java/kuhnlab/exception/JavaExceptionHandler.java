/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package kuhnlab.exception;

import ij.IJ;

/**
 *
 * @author jrkuhn
 */
public class JavaExceptionHandler implements IJ.ExceptionHandler {
    
//    protected static JavaExceptionHandler instance = null;
//    
//    protected JavaExceptionHandler() {
//    }
//    
//    public static JavaExceptionHandler getInstance() {
//        if (instance == null) {
//            instance = new JavaExceptionHandler();
//        }
//        return instance;
//    }

    public void handle(Throwable e) {
        Thread thread = Thread.currentThread();
        ThreadGroup group = thread.getThreadGroup();
        group.uncaughtException(thread, e);
    }

}
