/*
 * MathTools.java
 *
 * Created on January 13, 2004, 2:14 PM
 */

package kuhnlab.math;

import kuhnlab.coordinates.KPoint2D;
import kuhnlab.estimate.EstimateFunction;
import kuhnlab.estimate.LevenbergMarquardtEstimator;

/**
 * Performs a 1 dimensional Gaussian fit to data.
 * 
 * This gaussian fit function is:
 * 
 *      g(x) = a0*exp(-(x - a1)^2/(2*a2^2)) + a3
 * 
 * where
 *      a0 is the y multiplier
 *      a1 is the x offset
 *      a2 is the width of the gaussian (=sigma)
 *      a3 is the y offset
 * 
 * @author  jkuhn
 */

public class GaussianFit2D implements EstimateFunction {

    public static final int AMPLITUDE = 0;
    public static final int SIGMA = 1;
    public static final int CENTER_X = 2;
    public static final int CENTER_Y = 3;
    public static final int OFFSET = 4;
    
    static final int NUM_COEF = 5;
    

    protected final static String[] COEF_NAMES = {
        "amplitude", "sigma", "center x", "center y", "offset"
    };
    
    public int getPointDimension() {
        return 2;
    }

    public int getNumCoef() {
        return NUM_COEF;
    }

    public String getCoefName(int index) {
        return COEF_NAMES[index];
    }

    public Estimate getEstimate(double[] point, double[] coef) {
        //
        // This 2D gaussian fit function is:
        //
        //           A          / -((x - x0)^2 + (y - y0)^2) \
        //  g(x) = -------- exp|  --------------------------  | + B
        //         2*Pi*D^2     \            2*D^2           /
        //
        //  by defining the coefficents:
        //      a is the amplitude = A / (2*Pi*D^2)
        //      b is the width of the Gaussian = D*sqrt(2)
        //      c is the x offset = x0
        //      d is the y offset = y0
        //      e is the intensity offset = B
        //  and using the transformation functions:
        //      P = (x - c) / b
        //      Q = (y - d) / b
        //  the fit function becomes
        //
        //  g(x,y) = a * exp(-P^2 - Q^2) + e
        //
        // The derivative of g(x) with respect to the various parameters is:
        //
        //  dg/da = exp(-P^2 - Q^2)
        //
        //  dg/db = (2*a/b) * exp(-P^2 - Q^2) * (P^2 + Q^2)
        //
        //  dg/dc = (2*a/b) * exp(-P^2 - Q^2) * P
        //
        //  dg/dd = (2*a/b) * exp(-P^2 - Q^2) * Q
        //
        //  dg/de = 1
        //

        double x, y, a, b, c, d, e, P, Q;

        x = point[0];
        y = point[1];
        a = coef[AMPLITUDE];
        b = coef[SIGMA];
        c = coef[CENTER_X];
        d = coef[CENTER_Y];
        e = coef[OFFSET];
        P = (x - c) / b;
        Q = (y - d) / b;

        double expPQ = Math.exp(-P*P - Q*Q);

        Estimate est = new Estimate(NUM_COEF);

        //  g(x,y) = a * exp(-P^2 - Q^2) + e
        est.estimate = a * expPQ + e;

        //  dg/da = exp(-P^2 - Q^2)
        est.derivatives[AMPLITUDE] = expPQ;

        //  dg/db = (2*a/b) * exp(-P^2 - Q^2) * (P^2 + Q^2)
        est.derivatives[SIGMA] = (2*a/b) * expPQ * (P*P + Q*Q);

        //  dg/dc = (2*a/b) * exp(-P^2 - Q^2) * P
        est.derivatives[CENTER_X] = (2*a/b) * expPQ * P;

        //  dg/dd = (2*a/b) * exp(-P^2 - Q^2) * Q
        est.derivatives[CENTER_Y] = (2*a/b) * expPQ * Q;

        //  dg/de = 1
        est.derivatives[OFFSET] = 1.0;

        return est;
    }
    
    public double[] findFit(KPoint2D[] points, double[] vals, double[] sds, double[] coef, double[] fitErr) {

        LevenbergMarquardtEstimator fitter = new LevenbergMarquardtEstimator(this);
        int nPoints = points.length;
        for (int i=0; i<nPoints; i++) {
            double[] ptx = {points[i].x, points[i].y};
            fitter.addDataPoint(ptx, vals[i], sds[i]);
        }

        double[] fit = fitter.estimate(coef);

        if (fitErr != null) {
            double[][] covar = fitter.estimatedCovariance();
            for (int i=0; i<covar[0].length; i++) {
                fitErr[i] = Math.sqrt(covar[i][i]);
            }
        }

        return fit;
    }
}
