/*
 * GeomTools.java
 *
 * Created on January 13, 2004, 5:04 PM
 */

package kuhnlab.math;


/**
 *
 * @author  jkuhn
 */
public class Peaks {
    
    static public void parabolicPeakRefine(double[] f, int iOld, 
            double[] iMax, double[] fMax, boolean bTrough) {
        if (iOld<=0 || iOld>=f.length-1) {
            // we are at either end, and cannot refine the peak
            return;
        }
        double fL=f[iOld-1], fC=f[iOld], fR=f[iOld+1];
        parabolicPeakRefine(fL, fC, fR, iMax, fMax, bTrough);
        iMax[0] += iOld;
    }
    
    static public void parabolicPeakRefine(double fL, double fC, double fR, 
            double[] xMaxOff, double[] fMax, boolean bTrough) {
        xMaxOff[0] = 0;
        fMax[0] = fC;
        
        if (bTrough) {
            fL = -fL; fC = -fC; fR = -fR;
        }
        double dDF = (fR - fL)/2;
        double dDDF = (-fL + 2*fC - fR);
        if (Math.abs(dDDF) > 1.0e-8) {
            xMaxOff[0] = dDF/dDDF;
            if (bTrough)
                fMax[0] -= (dDF*dDF/dDDF)/2;
            else
                fMax[0] += (dDF*dDF/dDDF)/2;
        }
    }
    
    public static boolean isLocalMax(double[] ssrc, int i, int iRadius) {
        int w = ssrc.length;
        int j, jL, jR;
        double center = ssrc[i];
        for (j=1; j<=iRadius; j++) {
            jL = (i-j >= 0) ? i-j : 0;
            jR = (i+j <  w) ? i+j : w-1;
            if (ssrc[jL] >= center || ssrc[jR] >= center) {
                return false;
            }
        }
        return true;
    }
    
    public static boolean isLocalMax(float[] ssrc, int i, int iRadius) {
        int w = ssrc.length;
        if (i<=0 || i>=w-1)
            return false;
        int j, jL, jR;
        float center = ssrc[i];
        for (j=1; j<=iRadius; j++) {
            jL = (i-j >= 0) ? i-j : 0;
            jR = (i+j <  w) ? i+j : w-1;
            if (ssrc[jL] >= center || ssrc[jR] >= center) {
                return false;
            }
        }
        return true;
    }
    
    public static boolean isLocalMin(double[] ssrc, int i, int iRadius) {
        int w = ssrc.length;
        int j, jL, jR;
        double center = ssrc[i];
        for (j=1; j<=iRadius; j++) {
            jL = (i-j >= 0) ? i-j : 0;
            jR = (i+j <  w) ? i+j : w-1;
            if (ssrc[jL] <= center || ssrc[jR] <= center) {
                return false;
            }
        }
        return true;
    }
    
    public static boolean isLocalMin(float[] ssrc, int i, int iRadius) {
        int w = ssrc.length;
        int j, jL, jR;
        float center = ssrc[i];
        for (j=1; j<=iRadius; j++) {
            jL = (i-j >= 0) ? i-j : 0;
            jR = (i+j <  w) ? i+j : w-1;
            if (ssrc[jL] <= center || ssrc[jR] <= center) {
                return false;
            }
        }
        return true;
    }
}
