/*
 * MathTools.java
 *
 * Created on January 13, 2004, 2:14 PM
 */

package kuhnlab.math;

import kuhnlab.coordinates.KPoint2D;
import kuhnlab.estimate.EstimateFunction;
import kuhnlab.estimate.LevenbergMarquardtEstimator;
import org.apache.commons.math3.exception.MaxCountExceededException;
import org.apache.commons.math3.special.Erf;

/**
 * Performs a 1 dimensional Gaussian fit to data.
 * 
 * This gaussian fit function is:
 * 
 *      g(x) = a0*exp(-(x - a1)^2/(2*a2^2)) + a3
 * 
 * where
 *      a0 is the y multiplier
 *      a1 is the x offset
 *      a2 is the width of the gaussian (=sigma)
 *      a3 is the y offset
 * 
 * @author  jkuhn
 */

public class EndpointErfFit2D implements EstimateFunction {

    public static final int AMPLITUDE = 0;
    public static final int SIGMA = 1;
    public static final int CENTER_X = 2;
    public static final int CENTER_Y = 3;
    public static final int OFFSET = 4;

    final double SQRTPI = Math.sqrt(Math.PI);


    protected final static String[] COEF_NAMES = {
        "amplitude", "sigma", "center x", "center y", "offset"
    };
    
    public int getPointDimension() {
        return 2;
    }

    public int getNumCoef() {
        return 5;
    }

    public String getCoefName(int index) {
        return COEF_NAMES[index];
    }

    public Estimate getEstimate(double[] point, double[] coef) {
        //
        // This endpoint gaussian fit function is:
        //
        //           A        / -(y - y0)^2 \    [         /  x - x0 \  ]
        //  g(x) = ------ exp|  -----------  | * [ 1 + erf|  -------- | ] + B
        //         4*Pi*D     \    2*D^2    /    [         \ Sqrt2*D /  ]
        //
        //  by defining the coefficents:
        //      a is the amplitude = A / (4*Pi*D)
        //      b is the width of the Gaussian = D*sqrt(2)
        //      c is the x offset = x0
        //      d is the y offset = y0
        //      e is the intensity offset = B
        //  and using the transformation functions:
        //      P = (x - c) / b
        //      Q = (y - d) / b
        //  the fit function becomes
        //
        //  g(x,y) = a * exp(-Q^2) * (1 + erf(P)) + e
        //
        // The derivative of g(x) with respect to the various parameters is:
        //
        //  dg/da = exp(-Q^2) * (1 + erf(P))
        //
        //  dg/db = (2*a/b) * exp(-Q^2) * (Q^2*(1 + erf(P)) - (P/sqrt(Pi)*exp(-P^2))
        //
        //  dg/dc = -(2*a/(sqrt(Pi)*b)) * exp(-P^2) * exp(-Q^2)
        //
        //  dg/dd = (2*a*Q/b) * exp(-Q^2) * (1 + erf(P))
        //
        //  dg/de = 1
        //

        double x, y, a, b, c, d, e, P, Q;

        // The x value passed to this routine is actually an index into the
        // table of x and y values.
        x = point[0];
        y = point[1];

        a = coef[AMPLITUDE];
        b = coef[SIGMA];
        c = coef[CENTER_X];
        d = coef[CENTER_Y];
        e = coef[OFFSET];
        P = (x - c) / b;
        Q = (y - d) / b;

        double expQQ = Math.exp(-Q*Q);
        double expPP = Math.exp(-P*P);
        double erfP;
        try {
            erfP = Erf.erf(P);
        } catch (MaxCountExceededException ex) {
            erfP = 0.0;
        }

        Estimate est = new Estimate(5);

        //  g(x,y) = a * exp(-Q^2) * (1 + erf(P)) + e
        est.estimate = a * expQQ * (1 + erfP) + e;

        //  dg/da = exp(-Q^2) * (1 + erf(P))
        est.derivatives[AMPLITUDE] = expQQ * (1 + erfP);

        //  dg/db = (2*a/b) * exp(-Q^2) * (Q^2*(1 + erf(P)) - (P/sqrt(Pi)*exp(-P^2))
        est.derivatives[SIGMA] = (2*a/b) * expQQ * (Q*Q*(1 + erfP) - (P*expPP/SQRTPI));

        //  dg/dc = -(2*a/(sqrt(Pi)*b)) * exp(-P^2) * exp(-Q^2)
        est.derivatives[CENTER_X] = -2*a * expPP * expQQ / (SQRTPI*b);

        //  dg/dd = (2*a*Q/b) * exp(-Q^2) * (1 + erf(P))
        est.derivatives[CENTER_Y] = 2*a*Q * expQQ * (1 + erfP) / b;

        //  dg/de = 1
        est.derivatives[OFFSET] = 1.0;

        return est;
    }

    
    public double[] findFit(KPoint2D[] points, double[] vals, double[] sds, double[] coef, double[] fitErr) {

        LevenbergMarquardtEstimator fitter = new LevenbergMarquardtEstimator(this);
        int nPoints = points.length;
        for (int i=0; i<nPoints; i++) {
            double[] ptx = {points[i].x, points[i].y};
            fitter.addDataPoint(ptx, vals[i], sds[i]);
        }

        double[] fit = fitter.estimate(coef);

        if (fitErr != null) {
            double[][] covar = fitter.estimatedCovariance();
            for (int i=0; i<covar[0].length; i++) {
                fitErr[i] = Math.sqrt(covar[i][i]);
            }
        }
        
        return fit;
    }
}
