/*
 * MathTools.java
 *
 * Created on January 13, 2004, 2:14 PM
 */

package kuhnlab.math;

import kuhnlab.estimate.EstimateFunction;
import kuhnlab.estimate.LevenbergMarquardtEstimator;
import org.apache.commons.math3.exception.MaxCountExceededException;
import org.apache.commons.math3.special.Erf;

/**
 * Performs a 1 dimensional Erf (integral of Gaussian) fit to data.
 * 
 * This erf fit function is:
 * 
 *      g(x) = a0*(1 + erf((x - a1)/(Sqrt(2)*a2)))/2 + a3
 * 
 * where
 *      a0 is the y multiplier
 *      a1 is the x offset
 *      a2 is the width of the gaussian (=sigma)
 *      a3 is the y offset
 * 
 * @author  jkuhn
 */

public class ErfFit1D implements EstimateFunction {

    public static final int AMPLITUDE = 0;
    public static final int CENTER = 1;
    public static final int SIGMA = 2;
    public static final int OFFSET = 3;
    static final int NUM_COEF = 4;
    static final double SQRT2 = Math.sqrt(2);
    static final double SQRT2PI = Math.sqrt(2*Math.PI);

    protected final static String[] COEF_NAMES = {
        "amplitude", "center", "sigma", "offset"
    };
    
    public int getPointDimension() {
        return 1;
    }

    public int getNumCoef() {
        return NUM_COEF;
    }

    public String getCoefName(int index) {
        return COEF_NAMES[index];
    }

    public Estimate getEstimate(double[] point, double[] a) {
        //
        // This gaussian fit function is:
        //
        //  g(x) = a0*(1 + erf((x - a1)/(Sqrt(2)*a2)))/2 + a3
        //
        //  where:
        //      a0 is the y multiplier
        //      a1 is the x offset
        //      a2 is the width of the gaussian (=sigma)
        //      a3 is the y offset
        //
        // The derivative of g(x) with respect to the various parameters is:
        //
        //  g(x) = a0*(1 + erf(A))/2 + a3
        //
        //      where A = (x - a1)/(Sqrt(2)*a2)
        //
        //  dg/da0 = (1 + erf(A))/2
        //
        //  dg/da1 = -a0*exp(-A^2)/(a2*sqrt(2*Pi))
        //
        //  dg/da2 = -a0*(x - a1)*Exp[-A^2]/(a2^2*sqrt(2*Pi))
        //
        //  dg/da3 = 1

        Estimate est = new Estimate(NUM_COEF);
        
        double x = point[0];
        double a0 = a[AMPLITUDE];
        double a1 = a[CENTER];
        double a2 = a[SIGMA];
        double a3 = a[OFFSET];
        double xoff = (x - a1);
        double A = xoff/(SQRT2*a2);
        double erfAp1over2;
        try {
            erfAp1over2 = (Erf.erf(A) + 1)/2;
        } catch (MaxCountExceededException ex) {
            erfAp1over2 = 1/2;
        }
        double expAsq = Math.exp(-A*A);
        
        est.estimate = a0*erfAp1over2 + a3;
        est.derivatives[AMPLITUDE] = erfAp1over2;
        est.derivatives[CENTER] = -a0*expAsq/(a2*SQRT2PI);
        est.derivatives[SIGMA] = -a0*xoff*expAsq/(a2*a2*SQRT2PI);
        est.derivatives[OFFSET] = 1.0;
        
        return est;
    }
    
    public double[] findFit(double[] sx, double[] sy, double[] ssd, double[] coef, double[] fitErr) {

        LevenbergMarquardtEstimator fitter = new LevenbergMarquardtEstimator(this);
        int nPoints = sy.length;
        for (int i=0; i<nPoints; i++) {
            double[] ptx = {sx[i]};
            fitter.addDataPoint(ptx, sy[i], ssd[i]);
        }
        
        double[] fit = fitter.estimate(coef);

        if (fitErr != null) {
            double[][] covar = fitter.estimatedCovariance();
            for (int i=0; i<covar[0].length; i++) {
                fitErr[i] = Math.sqrt(covar[i][i]);
            }
        }

        return fit;
    }
}
