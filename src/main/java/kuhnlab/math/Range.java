/*
 * GeomTools.java
 *
 * Created on January 13, 2004, 5:04 PM
 */

package kuhnlab.math;
import java.util.*;
import java.awt.geom.*;


/**
 *
 * @author  jkuhn
 */
public class Range {
    static public int range(int min, int val, int max) {
        if (val < min) return min;
        if (val > max) return max;
        return val;
    }
    static public float range(float min, float val, float max) {
        if (val < min) return min;
        if (val > max) return max;
        return val;
    }
    static public double range(double min, double val, double max) {
        if (val < min) return min;
        if (val > max) return max;
        return val;
    }
}
