/*
 * MathTools.java
 *
 * Created on January 13, 2004, 2:14 PM
 */

package kuhnlab.math;

import kuhnlab.estimate.EstimateFunction;
import kuhnlab.estimate.LevenbergMarquardtEstimator;

/**
 * Performs a 1 dimensional Gaussian fit to data.
 * 
 * This gaussian fit function is:
 * 
 *      g(x) = a0*exp(-(x - a1)^2/(2*a2^2)) + a3
 * 
 * where
 *      a0 is the y multiplier
 *      a1 is the x offset
 *      a2 is the width of the gaussian (=sigma)
 *      a3 is the y offset
 * 
 * @author  jkuhn
 */

public class GaussianFit1DConstSigma implements EstimateFunction {

    public static final int AMPLITUDE = 0;
    public static final int CENTER = 1;
    public static final int OFFSET = 2;
    static final int NUM_COEF = 3;
    
    protected double sigma;

    protected final static String[] COEF_NAMES = {
        "amplitude", "center", "offset"
    };
    
    public int getPointDimension() {
        return 1;
    }

    public int getNumCoef() {
        return NUM_COEF;
    }

    public String getCoefName(int index) {
        return COEF_NAMES[index];
    }

    public void setSigma(double sigma) {
        this.sigma = sigma;
    }
    
    public double getSigma() {
        return this.sigma;
    }
    
    public Estimate getEstimate(double[] point, double[] a) {
        //
        // This gaussian fit function is:
        //
        //  g(x) = a0*exp(-(x - a1)^2/(2*a2^2)) + a3
        //
        //  where:
        //      a0 is the y multiplier
        //      a1 is the x offset
        //      a2 is the width of the gaussian (=sigma)
        //      a3 is the y offset
        //
        // The derivative of g(x) with respect to the various parameters is:
        //
        //  dg/da0 = exp(-(x - a1)^2/(2*a2^2))
        //
        //  dg/da1 = a0*exp(...)*(x - a1)/(a2^2)
        //
        //  dg/da2 = a0*exp(...)*(x - a1)^2/(a2^3)
        //
        //  dg/da3 = 1

        Estimate est = new Estimate(NUM_COEF);
        
        double x = point[0];
        double a0 = a[AMPLITUDE];
        double a1 = a[CENTER];
        double a2 = sigma;
        double a3 = a[OFFSET];
        double xoff = (x - a1);
        double exp = Math.exp(-xoff*xoff/(2*a2*a2));
        est.estimate = a0*exp + a3;
        est.derivatives[AMPLITUDE] = exp;
        est.derivatives[CENTER] = a0*exp*xoff/(a2*a2);
        est.derivatives[OFFSET] = 1.0;
        
        return est;
    }
    
    public double[] findFit(double[] sx, double[] sy, double[] ssd, double[] coef, double[] fitErr) {

        LevenbergMarquardtEstimator fitter = new LevenbergMarquardtEstimator(this);
        int nPoints = sy.length;
        for (int i=0; i<nPoints; i++) {
            double[] ptx = {sx[i]};
            fitter.addDataPoint(ptx, sy[i], ssd[i]);
        }
        
        double[] fit = fitter.estimate(coef);

        if (fitErr != null) {
            double[][] covar = fitter.estimatedCovariance();
            for (int i=0; i<covar[0].length; i++) {
                fitErr[i] = Math.sqrt(covar[i][i]);
            }
        }

        return fit;
    }
}
