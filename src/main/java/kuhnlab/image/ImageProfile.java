/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package kuhnlab.image;

import ij.process.FloatProcessor;
import java.util.List;
import kuhnlab.coordinates.KPoint2D;

/**
 *
 * @author jrkuhn
 */
public class ImageProfile {
    public static float[] getFloatLineProfile(FloatProcessor fpSrc, List<KPoint2D> lpLine) {
        int nPoints = lpLine.size();
        float[] sfProfile = new float[nPoints];
        KPoint2D p;
        for (int i=0; i<nPoints; i++) {
            p = lpLine.get(i);
            sfProfile[i] = (float)fpSrc.getInterpolatedValue(p.x, p.y);
        }
        return sfProfile;
    }

    public static double[] getDoubleLineProfile(FloatProcessor fpSrc, List<KPoint2D> lpLine) {
        int nPoints = lpLine.size();
        double[] sdProfile = new double[nPoints];
        KPoint2D p;
        for (int i=0; i<nPoints; i++) {
            p = lpLine.get(i);
            sdProfile[i] = fpSrc.getInterpolatedValue(p.x, p.y);
        }
        return sdProfile;
    }
    
    public static FloatProcessor get2DProfile(FloatProcessor fpSrc, KPoint2D ptCenter, 
            KPoint2D vXDir, KPoint2D vYDir,
            int width, int height) {
        KPoint2D ptYOff = new KPoint2D();
        KPoint2D vXOff = new KPoint2D();
        KPoint2D ptSrc = new KPoint2D();
        FloatProcessor fpRes = new FloatProcessor(width, height);
        float[] resData = (float[])fpRes.getPixels();
        
        int iDest, jDest, iCenter = width/2, jCenter = height/2;
        int iSrc, jSrc;
        int ijDest = 0;
        for (jDest=0, jSrc=-jCenter; jDest<height; jDest++, jSrc++) {
            ptYOff.toProdOf(jSrc, vYDir);
            ptYOff.add(ptCenter);
            for (iDest=0, iSrc=-iCenter; iDest<width; iDest++, iSrc++) {
                vXOff.toProdOf(iSrc, vXDir);
                ptSrc.toSumOf(ptYOff, vXOff);
                resData[ijDest++] = (float)fpSrc.getInterpolatedPixel(ptSrc.x, ptSrc.y);
            }
        }
        
        return fpRes;
        
    }

}
