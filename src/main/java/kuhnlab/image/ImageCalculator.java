/*
 * ImageConvolver.java
 *
 * Created on January 12, 2004, 10:19 AM
 */
package kuhnlab.image;
import ij.process.FloatProcessor;
import java.util.Arrays;

/**
 *
 * @author  jkuhn
 */
public class ImageCalculator {
    // Compute the derivative using optimized 5x1(*)1x5 masks
    //
    //	                    [   7 ]
    //                      [  63 ]
    //	[3 10 0 -10 -3] (*) [ 116 ]  /256
    //                      [  63 ]
    //	                    [   7 ]
    //
    // here, (*) means convolution
    //
    public static void derivativeX(int w, int h, float[] ssrc, float[] stemp, float[] sdest) {
        int ss = w * h;
        int w2 = w * 2;
        int x, y, yw, xend = w - 1, ywend = ss - w;		// NOTE: ss-w == (h-1)*w
        int yw1, yw2, yw3, yw4, yw5;
        int x1, x2, x3, x4, x5;
        double a1, a2, a3, a4, a5;

        double dSum;

        final double dks1 = 7.0 / 256;
        final double dks2 = 63.0 / 256;
        final double dks3 = 116.0 / 256;

        // smooth in y direction
        for (y = 0, yw = 0; y < h; y++, yw += w) {
            yw1 = (y > 1) ? yw - w2 : 0;
            yw2 = (y > 0) ? yw - w : 0;
            yw3 = yw;
            yw4 = (y < h - 1) ? yw + w : ywend;
            yw5 = (y < h - 2) ? yw + w2 : ywend;

            for (x = 0; x < w; x++) {
                a1 = ssrc[x + yw1];
                a2 = ssrc[x + yw2];
                a3 = ssrc[x + yw3];
                a4 = ssrc[x + yw4];
                a5 = ssrc[x + yw5];

                dSum = dks1 * (a1 + a5) + dks2 * (a2 + a4) + dks3 * a3;

                stemp[x + yw] = (float) dSum;
            }
        }

        // take derivative in x direction
        for (x = 0; x < w; x++) {
            x1 = (x > 1) ? x - 2 : 0;
            x2 = (x > 0) ? x - 1 : 0;
            x4 = (x < w - 1) ? x + 1 : xend;
            x5 = (x < w - 2) ? x + 2 : xend;

            for (y = 0, yw = 0; y < h; y++, yw += w) {
                a1 = stemp[x1 + yw];
                a2 = stemp[x2 + yw];
                a4 = stemp[x4 + yw];
                a5 = stemp[x5 + yw];

                // remember: convolution reverses the kernel before multiplying.
                dSum = 3 * (a5 - a1) + 10 * (a4 - a2);
                sdest[x + yw] = (float) dSum;
            }
        }
    }

    // Compute the derivative using optimized 1x5 (*) 5x1 masks
    //
    //	[   3 ]
    //  [  10 ]
    //	[   0 ] (*) [7 63 116 63 7] /256
    //  [ -10 ]
    //	[  -3 ]
    //
    // here, (*) means convolution
    //
    public static void derivativeY(int w, int h, float[] ssrc, float[] stemp, float[] sdest) {
        int ss = w * h;
        int w2 = w * 2;
        int x, y, yw, xend = w - 1, ywend = ss - w;		// NOTE: ss-w == (h-1)*w
        int yw1, yw2, yw3, yw4, yw5;
        int x1, x2, x3, x4, x5;
        double a1, a2, a3, a4, a5;

        double dSum;

        final double dks1 = 7.0 / 256;
        final double dks2 = 63.0 / 256;
        final double dks3 = 116.0 / 256;

        // smooth in x direction
        for (x = 0; x < w; x++) {
            x1 = (x > 1) ? x - 2 : 0;
            x2 = (x > 0) ? x - 1 : 0;
            x3 = x;
            x4 = (x < w - 1) ? x + 1 : xend;
            x5 = (x < w - 2) ? x + 2 : xend;

            for (y = 0, yw = 0; y < h; y++, yw += w) {
                a1 = ssrc[x1 + yw];
                a2 = ssrc[x2 + yw];
                a3 = ssrc[x3 + yw];
                a4 = ssrc[x4 + yw];
                a5 = ssrc[x5 + yw];

                dSum = dks1 * (a1 + a5) + dks2 * (a2 + a4) + dks3 * a3;
                stemp[x + yw] = (float) dSum;
            }
        }

        // take derivative in y direction
        for (y = 0, yw = 0; y < h; y++, yw += w) {
            yw1 = (y > 1) ? yw - w2 : 0;
            yw2 = (y > 0) ? yw - w : 0;
            yw4 = (y < h - 1) ? yw + w : ywend;
            yw5 = (y < h - 2) ? yw + w2 : ywend;

            for (x = 0; x < w; x++) {
                a1 = stemp[x + yw1];
                a2 = stemp[x + yw2];
                a4 = stemp[x + yw4];
                a5 = stemp[x + yw5];

                // remember: convolution reverses the kernel before multiplying.
                dSum = 3 * (a5 - a1) + 10 * (a4 - a2);
                sdest[x + yw] = (float) dSum;
            }
        }

    }

    // Compute the derivative in the x direction using the central difference
    public static void centralDifferenceX(int w, int h, float[] ssrc, float[] sdest) {
        int x, y, yw, xend = w - 1;
        int x1, x3;
        double a1, a3;

        // take derivative in x direction
        for (x = 0; x < w; x++) {
            x1 = (x > 0) ? x - 1 : 0;
            x3 = (x < xend) ? x + 1 : xend;

            for (y = 0, yw = 0; y < h; y++, yw += w) {
                a1 = ssrc[x1 + yw];
                a3 = ssrc[x3 + yw];

                sdest[x + yw] = (float) (0.5*(a3 - a1));
            }
        }
    }

    // Compute the derivative in the y direction using the central difference
    public static void centralDifferenceY(int w, int h, float[] ssrc, float[] sdest) {
        int ss = w * h;
        int w2 = w * 2;
        int x, y, yw, yend = h - 1, ywend = ss - w;   // NOTE: ss-w == (h-1)*w
        int yw1, yw3;
        double a1, a3;

        double dSum;

        // take derivative in y direction
        for (y = 0, yw = 0; y < h; y++, yw += w) {
            yw1 = (y > 0) ? yw - w : 0;
            yw3 = (y < yend) ? yw + w : ywend;

            for (x = 0; x < w; x++) {
                a1 = ssrc[x + yw1];
                a3 = ssrc[x + yw3];

                sdest[x + yw] = (float) (0.5*(a3 - a1));
            }
        }

    }

    
    public static void rankFilter(int w, int h, float[] ssrc, float[] sdest, int width, int rank, boolean fromTop, boolean excludeCenter) {
        int kw2 = width / 2;

        int ywmax = (h - 1) * w;
        int wmax = w - 1;
        int x, y, xs, xo, ys, yw, yws, ywo, i;
        int xmin, xmax, ymin, ymax, ywmin;

        int numNeighbors = width*width;
        if (excludeCenter) {
            numNeighbors--;
        }
        float[] neighbors = new float[numNeighbors];
        if (fromTop) {
            rank = numNeighbors - 1 - rank;
        }

        for (y = 0, yw = 0; y < h; y++, yw += w) {
            ymin = y - kw2;
            ymax = ymin + width;
            ywmin = ymin * w;
            for (x = 0; x < w; x++) {
                xmin = x - kw2;
                xmax = xmin + width;

                i=0;
                for (xs = xmin; xs < xmax; xs++) {
                    xo = xs;
                    if (xo < 0) {
                        xo = 0;
                    }
                    if (xo > wmax) {
                        xo = wmax;
                    }
                    for (ys = ymin, yws = ywmin; ys < ymax; ys++, yws += w) {
                        ywo = yws;
                        if (ywo < 0) {
                            ywo = 0;
                        }
                        if (ywo > ywmax) {
                            ywo = ywmax;
                        }
                        if (!(excludeCenter && xs==x && ys==y)) {
                            neighbors[i++] = ssrc[ywo + xo];
                        }
                    }
                }
                Arrays.sort(neighbors,0,numNeighbors);
                sdest[yw + x] = neighbors[rank];
            }
        }
    }
    
    public static void copy(int w, int h, float[] ssrc, float[] sdest) {
        System.arraycopy(ssrc, 0, sdest, 0, w*h);
    }

    public static void add(int w, int h, float[] ssrcA, float[] ssrcB, float[] sdest) {
        int ss = w * h;
        for (int xy = 0; xy < ss; xy++) {
            sdest[xy] = (float) (ssrcA[xy] + ssrcB[xy]);
        }
    }

    public static void subtract(int w, int h, float[] ssrcA, float[] ssrcB, float[] sdest) {
        int ss = w * h;
        for (int xy = 0; xy < ss; xy++) {
            sdest[xy] = (float) (ssrcA[xy] - ssrcB[xy]);
        }
    }

    public static void multiply(int w, int h, float[] ssrcA, float[] ssrcB, float[] sdest) {
        int ss = w * h;
        for (int xy = 0; xy < ss; xy++) {
            sdest[xy] = (float) (ssrcA[xy] * ssrcB[xy]);
        }
    }

    public static void abs(int w, int h, float[] ssrc, float[] sdest) {
        int ss = w * h;
        float s;
        for (int xy = 0; xy < ss; xy++) {
            s = ssrc[xy];
            sdest[xy] = (s < 0) ? -s : s;
        }
    }

    public static void pow(int w, int h, float[] ssrc, double dPow, float[] sdest) {
        int ss = w * h;
        float s;
        for (int xy = 0; xy < ss; xy++) {
            s = ssrc[xy];
            sdest[xy] = (float)Math.pow(s, dPow);
        }
    }

    public static void normalize(int w, int h, float[] ssrc) {
        int ss = w * h;
        float fAbs, fMax = Float.NEGATIVE_INFINITY, fMin = Float.POSITIVE_INFINITY;
        int xy, len = ssrc.length;
        for (xy = 0; xy < len; xy++) {
            fAbs = ssrc[xy];
            if (fAbs > fMax) {
                fMax = fAbs;
            }
            if (fAbs < fMin) {
                fMin = fAbs;
            }
        }
        double dScale = 1.0 / (fMax - fMin);
        for (xy = 0; xy < len; xy++) {
            ssrc[xy] = (float) ((ssrc[xy] - fMin) * dScale);
        }
    }

    public static void scale(double dScale, int w, int h, float[] ssrc, float[] sdest) {
        int ss = w * h;
        for (int xy = 0; xy < ss; xy++) {
            sdest[xy] = (float) (dScale*ssrc[xy]);
        }
    }

    /** Calculates the gradient vector flow used for snakes. */
    static public void gradientVectorFlow(FloatProcessor fpSrc, FloatProcessor fpDestU, FloatProcessor fpDestV, double mu, int numIterations) {
        int width = fpSrc.getWidth();
        int height = fpSrc.getHeight();
        int size = width * height;

        // laplacian kernel
        double[] kernLaplacian = {0.00, 0.25, 0.00,
            0.25, -1.00, 0.25,
            0.00, 0.25, 0.00};

        // Create intermediate images
        FloatProcessor fpFX = new FloatProcessor(width, height);
        FloatProcessor fpFY = new FloatProcessor(width, height);
        FloatProcessor fpMagSq = new FloatProcessor(width, height);
        FloatProcessor fpLapU = new FloatProcessor(width, height);
        FloatProcessor fpLapV = new FloatProcessor(width, height);

        // get image data arrays for point-by-point operations
        float[] pFX = (float[]) fpFX.getPixels();
        float[] pFY = (float[]) fpFY.getPixels();
        float[] pMagSq = (float[]) fpMagSq.getPixels();
        float[] pU = (float[]) fpDestU.getPixels();
        float[] pV = (float[]) fpDestV.getPixels();
        float[] pLapU = (float[]) fpLapU.getPixels();
        float[] pLapV = (float[]) fpLapV.getPixels();

        // Create a normalized copy of the source image
        // and store it temporarily in fpMagSq
        ImageCalculator.copy(fpSrc, fpMagSq);
        ImageCalculator.normalize(fpMagSq);

        // calculate the gradients in X and Y of the normalized image
        ImageCalculator.centralDifferenceX(fpMagSq, fpFX);
        ImageCalculator.centralDifferenceY(fpMagSq, fpFY);

        // Initialize the temporary image buffers
        double fx, fy, magsq;
        for (int i = 0; i < size; i++) {
            // calculate the mag^2 of the gradient
            fx = pFX[i];
            fy = pFY[i];
            magsq = fx * fx + fy * fy;
            pMagSq[i] = (float) magsq;
            // multiply gradients in x and y by the mag^2
            pFX[i] *= magsq;
            pFY[i] *= magsq;
            // Initialize the U and V destination vectors
            // with the original gradients
            pU[i] = (float) fx;
            pV[i] = (float) fy;
        }

        // Itteratively solve for the GVF
        double fourmu = 4 * mu;
        for (int iter = 0; iter < numIterations; iter++) {
            // Calculate the laplacian of the current gradient
            ImageConvolver.convolve2D(fpDestU, fpLapU, kernLaplacian, 3, 3);
            ImageConvolver.convolve2D(fpDestV, fpLapV, kernLaplacian, 3, 3);
            double u, v, lapu, lapv, magsqfx, magsqfy;
            for (int i = 0; i < size; i++) {
                u = pU[i];
                v = pV[i];
                magsq = pMagSq[i];
                lapu = pLapU[i];
                lapv = pLapV[i];
                magsqfx = pFX[i];
                magsqfy = pFY[i];
                pU[i] = (float) ((1 - magsq) * u + fourmu * lapu + magsqfx);
                pV[i] = (float) ((1 - magsq) * v + fourmu * lapv + magsqfy);
            }
        }
    }

    /** Sets the length of all vectors (U(x,y), V(x,y)) to one. (U, V) will only
     * retain their direction.
      */
    static public void normalizeGradientVectorFlow(FloatProcessor fpU, FloatProcessor fpV, double minMagnitude) {
        final double LAMBDA = 1e-9;
        int size = fpU.getWidth() * fpU.getHeight();
        float[] pU = (float[]) fpU.getPixels();
        float[] pV = (float[]) fpV.getPixels();

        // Divide U and V by the magnitude of (u,v)
        double u, v, mag;
        for (int i = 0; i < size; i++) {
            u = pU[i];
            v = pV[i];
            mag = Math.sqrt(u * u + v * v);
            if (mag > minMagnitude) {
                u = u / (mag + LAMBDA);
                v = v / (mag + LAMBDA);
                pU[i] = (float) (u);
                pV[i] = (float) (v);
            } else {
                pU[i] = 0f;
                pV[i] = 0f;
            }
        }

    }


    public static void derivativeX(FloatProcessor fpSrc, FloatProcessor fpTemp, FloatProcessor fpDest) {
        derivativeX(fpSrc.getWidth(), fpSrc.getHeight(), (float[]) fpSrc.getPixels(), (float[]) fpTemp.getPixels(), (float[]) fpDest.getPixels());
    }

    public static void derivativeY(FloatProcessor fpSrc, FloatProcessor fpTemp, FloatProcessor fpDest) {
        derivativeY(fpSrc.getWidth(), fpSrc.getHeight(), (float[]) fpSrc.getPixels(), (float[]) fpTemp.getPixels(), (float[]) fpDest.getPixels());
    }

    public static void centralDifferenceX(FloatProcessor fpSrc, FloatProcessor fpDest) {
        centralDifferenceX(fpSrc.getWidth(), fpSrc.getHeight(), (float[]) fpSrc.getPixels(), (float[]) fpDest.getPixels());
    }

    public static void centralDifferenceY(FloatProcessor fpSrc, FloatProcessor fpDest) {
        centralDifferenceY(fpSrc.getWidth(), fpSrc.getHeight(), (float[]) fpSrc.getPixels(), (float[]) fpDest.getPixels());
    }
    
    public static void rankFilter(FloatProcessor fpSrc, FloatProcessor fpDest, int width, int rank, boolean fromTop, boolean excludeCenter) {
        rankFilter(fpSrc.getWidth(), fpSrc.getHeight(), (float[])fpSrc.getPixels(), (float[])fpDest.getPixels(), width, rank, fromTop, excludeCenter);
    }
    

    public static void copy(FloatProcessor fpSrc, FloatProcessor fpDest) {
        copy(fpSrc.getWidth(), fpSrc.getHeight(), (float[]) fpSrc.getPixels(), (float[]) fpDest.getPixels());
    }

    public static void add(FloatProcessor fpSrcA, FloatProcessor fpSrcB, FloatProcessor fpDest) {
        add(fpSrcA.getWidth(), fpSrcA.getHeight(), (float[]) fpSrcA.getPixels(), (float[]) fpSrcB.getPixels(), (float[]) fpDest.getPixels());
    }

    public static void subtract(FloatProcessor fpSrcA, FloatProcessor fpSrcB, FloatProcessor fpDest) {
        subtract(fpSrcA.getWidth(), fpSrcA.getHeight(), (float[]) fpSrcA.getPixels(), (float[]) fpSrcB.getPixels(), (float[]) fpDest.getPixels());
    }

    public static void multiply(FloatProcessor fpSrcA, FloatProcessor fpSrcB, FloatProcessor fpDest) {
        multiply(fpSrcA.getWidth(), fpSrcA.getHeight(), (float[]) fpSrcA.getPixels(), (float[]) fpSrcB.getPixels(), (float[]) fpDest.getPixels());
    }

    public static void abs(FloatProcessor fpSrc, FloatProcessor fpDest) {
        abs(fpSrc.getWidth(), fpSrc.getHeight(), (float[]) fpSrc.getPixels(), (float[]) fpDest.getPixels());
    }

    public static void pow(FloatProcessor fpSrc, double dPow, FloatProcessor fpDest) {
        pow(fpSrc.getWidth(), fpSrc.getHeight(), (float[]) fpSrc.getPixels(), dPow, (float[]) fpDest.getPixels());
    }

    public static void normalize(FloatProcessor fpSrc) {
        normalize(fpSrc.getWidth(), fpSrc.getHeight(), (float[]) fpSrc.getPixels());
    }

    public static void scale(double dScale, FloatProcessor fpSrc, FloatProcessor fpDest) {
        scale(dScale, fpSrc.getWidth(), fpSrc.getHeight(), (float[]) fpSrc.getPixels(), (float[]) fpDest.getPixels());
    }

}
