/*
 * ImageConvolver.java
 *
 * Created on January 12, 2004, 10:19 AM
 */
package kuhnlab.image;
import ij.process.FloatProcessor;

/**
 *
 * @author  jkuhn
 */
public class ImageConvolver {
    // Length of the Gaussian kernel in widths (sigma) 
    public static final double GAUSSIAN_RADIUS_MULT = 3.5;

    /**
     * Creates a symmetrical 1 dimensional Gaussian kernel or derivate
     * of a Gaussian kernal.
     * <p>
     * G(x) = exp(-x^2/(2*sigma^2))/sqrt(2*PI*sigma^2)
     * <p>
     * All derivative of Gaussian functions are symmetric about x=0, so
     * we only need to store the 0 \<= x \< iR half of the kernel and a flag to
     * indicate whether the symmetry is even or odd. The last entry in the
     * kernel will be the symmetry flag (1 for even, -1 for odd), while
     * the first entry corresponds to g(x=0), the second to g(x=1), etc.
     * <p>
     * Even derivatives have even symmetry (i.e. the second derivative is
     * even, while the third is odd).
     * @param iR radius of the kernel, should be at least 3*dSigma.
     * @param dSigma half width of gaussian
     * @param iOrder which derivative order to create. For example:
     * 		0 = no derivative, 1 = 1st derivative,
     * 		2 = 2nd derivative, etc. up to a 5th derivative.
     * @return an array of iR+1 [0..iR] double values. The first
     * 		iR [0..iR-1] values correspond to the kernel
     * 		coefficients. The last number in the array [iR] denotes
     * 		the symmetry: 1 for even, -1 for odd.
     */
    public static double[] createSymGaussian1DKernel(double dSigma, int iOrder) {
        //
        //                                 /       2   2      \
        //                1                |   - (x + y +...) |
        //  G(x) = ------------------ * Exp| ---------------- |
        //                    2 Dim/2      |             2    |
        //         (2*pi*sigma )           \      2*sigma     /
        //
        int iR = (int) (dSigma * GAUSSIAN_RADIUS_MULT);
        int iDim = 1;	// this is a one-dimensional kernel, no y, z, etc.
        int kw = iR + 1;
        if (kw < 3) {
            kw = 3;
        }
        double[] sdSTG = new double[kw + 1];
        double sigma2 = dSigma * dSigma;
        double sigma4 = sigma2 * sigma2;
        double sigma6 = sigma2 * sigma4;
        double sigma8 = sigma4 * sigma4;
        double sigma10 = sigma4 * sigma6;
        double twosigma2 = 2 * sigma2;
        double denom = Math.pow(2 * Math.PI * sigma2, iDim / 2.0);
        double dE;
        int x, x2, i;

        sdSTG[kw] = (float) Math.pow(-1, iOrder);
        for (i = 0; i < kw; i++) {
            x = i;
            x2 = x * x;
            dE = Math.exp(-x2 / twosigma2) / denom;
            switch (iOrder) {
                case 0:	// g(x)
                    sdSTG[i] = dE;
                    break;
                case 1:	// g'(x)
                    sdSTG[i] = -dE * x / sigma2;
                    break;
                case 2:	// g''(x)
                    sdSTG[i] = dE * (x2 - sigma2) / sigma4;
                    break;
                case 3:	// g'''(x)
                    sdSTG[i] = -dE * x * (x2 - 3 * sigma2) / sigma6;
                    break;
                case 4:	// g''''(x)
                    sdSTG[i] = dE * (x2 * x2 - 6 * x2 * sigma2 + 3 * sigma4) / sigma8;
                    break;
                case 5:	// g'''''(x)
                    sdSTG[i] = -dE * x * (x2 * x2 - 10 * x2 * sigma2 + 15 * sigma4) / sigma10;
                    break;
            }
        }

        return sdSTG;
    }

    /**
     * Performs a convolution in the X direction using a symmetrical kernel.
     *
     * @param	ssrc	array of source pixels
     * @param	w	width of source image
     * @param	h	height of source image
     * @param	sdest	array of destination pixels
     * @param	skern	symmetrical kernal of radius R. The first R-1 [0..R-1]
     *			values correspond to the kernel multipliers for
     *			pixels x, x+1, x+2,..x+R-1. The final value [R] denotes
     *			the symmetry: 1 for even (skern[x-1] = skern[x+1],
     *			skern[x-2] = skern[x+2], etc) and -1 for odd
     *			(skern[x-1] = -skern[x+1], skern[x-2] = -skern[x+2],
     *			etc).
     */
    public static void convolveSymX(int w, int h, float[] ssrc, float[] sdest, double[] skern) {
        int kw = skern.length - 1;
        boolean bEvenSym = (skern[kw] >= 0);

        int x, y, xpos, xneg, yw, k;
        int xmax = w - 1;

        double dSum;

        for (x = 0; x < w; x++) {
            for (y = 0, yw = 0; y < h; y++, yw += w) {
                // start with x=0
                dSum = ssrc[yw + x] * skern[0];
                for (k = 1; k < kw; k++) {
                    xpos = x + k;
                    xneg = x - k;
                    if (xneg < 0) {
                        xneg = 0;
                    }
                    if (xpos > xmax) {
                        xpos = xmax;
                    }
                    if (bEvenSym) {
                        dSum += (ssrc[yw + xneg] + ssrc[yw + xpos]) * skern[k];
                    } else {
                        // remember: convolution reverses the kernel before multiplying.
                        dSum += (ssrc[yw + xneg] - ssrc[yw + xpos]) * skern[k];
                    }
                }
                sdest[yw + x] = (float) dSum;
            }
        }
    }

    /**
     * Performs a convolution in the Y direction using a symmetrical kernel.
     *
     * @param	ssrc	array of source pixels
     * @param	w		width of source image
     * @param	h		height of source image
     * @param	sdest	array of destination pixels
     * @param	skern	symmetrical kernal of radius R. The first R-1 [0..R-1]
     *					values correspond to the kernel multipliers for
     *					pixels y, y+1, y+2,..y+R-1. The final value [R] denotes
     *					the symmetry: 1 for even (skern[y-1] = skern[y+1],
     *					skern[y-2] = skern[y+2], etc) and -1 for odd
     *					(skern[y-1] = -skern[y+1], skern[y-2] = -skern[y+2],
     *					etc).
     */
    public static void convolveSymY(int w, int h, float[] ssrc, float[] sdest, double[] skern) {
        int kw = skern.length - 1;
        boolean bEvenSym = (skern[kw] >= 0);

        int x, y, ywpos, ywneg, yw, ywo, k;
        int ywmax = (h - 1) * w;

        double dSum;

        for (y = 0, yw = 0; y < h; y++, yw += w) {
            for (x = 0; x < w; x++) {
                dSum = ssrc[yw + x] * skern[0];
                for (k = 1, ywo = w; k < kw; k++, ywo += w) {
                    ywpos = yw + ywo;
                    ywneg = yw - ywo;
                    if (ywneg < 0) {
                        ywneg = 0;
                    }
                    if (ywpos > ywmax) {
                        ywpos = ywmax;
                    }
                    if (bEvenSym) {
                        dSum += (ssrc[ywneg + x] + ssrc[ywpos + x]) * skern[k];
                    } else {
                        // remember: convolution reverses the kernel before multiplying.
                        dSum += (ssrc[ywneg + x] - ssrc[ywpos + x]) * skern[k];
                    }
                }
                sdest[yw + x] = (float) dSum;
            }
        }
    }

    public static void convolveSymBoth(int w, int h, float[] ssrc,
            float[] stemp, float[] sdest,
            double[] skernX, double[] skernY) {
        convolveSymX(w, h, ssrc, stemp, skernX);
        convolveSymY(w, h, stemp, sdest, skernY);
    }

    public static void convolveX(int w, int h, float[] ssrc, float[] sdest, double[] skern) {
        int kw = skern.length;
        int kw1 = kw - 1;
        int kw2 = kw / 2;

        int x, y, xo, xs, yw, k;
        int xmin, xmax;

        double dSum;

        for (x = 0; x < w; x++) {
            xmin = x - kw2;
            xmax = xmin + kw;
            for (y = 0, yw = 0; y < h; y++, yw += w) {
                dSum = 0;
                for (xs = xmin, k = kw1; xs < xmax; xs++, k--) {
                    xo = xs;
                    if (xo < 0) {
                        xo = 0;
                    }
                    if (xo >= w) {
                        xo = w - 1;
                    }
                    dSum += ssrc[yw + xo] * skern[k];
                }
                sdest[yw + x] = (float) dSum;
            }
        }
    }

    public static void convolveY(int w, int h, float[] ssrc, float[] sdest, double[] skern) {
        int kw = skern.length;
        int kw1 = kw - 1;
        int kw2 = kw / 2;

        int ywmax = (h - 1) * w;
        int x, y, yo, ys, yw, yws, ywo, k;
        int ymin, ymax, ywmin;

        double dSum;

        for (y = 0, yw = 0; y < h; y++, yw += w) {
            ymin = y - kw2;
            ymax = ymin + kw;
            ywmin = ymin * w;

            for (x = 0; x < w; x++) {
                dSum = 0;
                for (ys = ymin, yws = ywmin, k = kw1; ys < ymax; ys++, yws += w, k--) {
                    ywo = yws;
                    if (ywo < 0) {
                        ywo = 0;
                    }
                    if (ywo > ywmax) {
                        ywo = ywmax;
                    }
                    dSum += ssrc[ywo + x] * skern[k];
                }
                sdest[yw + x] = (float) dSum;
            }
        }
    }

    public static void convolveBoth(int w, int h, float[] ssrc,
            float[] stemp, float[] sdest,
            double[] skernX, double[] skernY) {
        convolveX(w, h, ssrc, stemp, skernX);
        convolveY(w, h, stemp, sdest, skernY);
    }


    public static void convolve2D(int w, int h, float[] ssrc, float[] sdest, double[] skern, int kw, int kh) {
        int kw1 = kw - 1;
        int kw2 = kw / 2;
        int kh1 = kh - 1;
        int kh2 = kh / 2;

        int kwymax = kw*kh - kw;
        int ywmax = (h - 1) * w;
        int wmax = w - 1;
        int x, y, xs, xo, yo, ys, yw, yws, ywo, kx, ky, kwy;
        int xmin, xmax, ymin, ymax, ywmin;

        double dSum;

        for (y = 0, yw = 0; y < h; y++, yw += w) {
            ymin = y - kw2;
            ymax = ymin + kh;
            ywmin = ymin * w;
            for (x = 0; x < w; x++) {
                xmin = x - kw2;
                xmax = xmin + kw;

                dSum = 0;
                for (xs = xmin, kx = kw1; xs < xmax; xs++, kx--) {
                    xo = xs;
                    if (xo < 0) {
                        xo = 0;
                    }
                    if (xo > wmax) {
                        xo = wmax;
                    }
                    for (ys = ymin, yws = ywmin, kwy = kwymax; ys < ymax; ys++, yws += w, kwy -= kw) {
                        ywo = yws;
                        if (ywo < 0) {
                            ywo = 0;
                        }
                        if (ywo > ywmax) {
                            ywo = ywmax;
                        }
                        dSum += ssrc[ywo + xo] * skern[kx + kwy];
                    }
                }
                sdest[yw + x] = (float) dSum;
            }
        }
    }


    public static void convolveSymX(FloatProcessor fpSrc, FloatProcessor fpDest, double[] skern) {
        convolveSymX(fpSrc.getWidth(), fpSrc.getHeight(), (float[]) fpSrc.getPixels(), (float[]) fpDest.getPixels(), skern);
    }

    public static void convolveSymY(FloatProcessor fpSrc, FloatProcessor fpDest, double[] skern) {
        convolveSymY(fpSrc.getWidth(), fpSrc.getHeight(), (float[]) fpSrc.getPixels(), (float[]) fpDest.getPixels(), skern);
    }

    public static void convolveSymBoth(FloatProcessor fpSrc, FloatProcessor fpTemp, FloatProcessor fpDest, double[] skernX, double[] skernY) {
        convolveSymBoth(fpSrc.getWidth(), fpSrc.getHeight(), (float[]) fpSrc.getPixels(), (float[]) fpTemp.getPixels(), (float[]) fpDest.getPixels(), skernX, skernY);
    }

    public static void convolveX(FloatProcessor fpSrc, FloatProcessor fpDest, double[] skern) {
        convolveX(fpSrc.getWidth(), fpSrc.getHeight(), (float[]) fpSrc.getPixels(), (float[]) fpDest.getPixels(), skern);
    }

    public static void convolveY(FloatProcessor fpSrc, FloatProcessor fpDest, double[] skern) {
        convolveY(fpSrc.getWidth(), fpSrc.getHeight(), (float[]) fpSrc.getPixels(), (float[]) fpDest.getPixels(), skern);
    }

    public static void convolveBoth(FloatProcessor fpSrc, FloatProcessor fpTemp, FloatProcessor fpDest, double[] skernX, double[] skernY) {
        convolveBoth(fpSrc.getWidth(), fpSrc.getHeight(), (float[]) fpSrc.getPixels(), (float[]) fpTemp.getPixels(), (float[]) fpDest.getPixels(), skernX, skernY);
    }

    public static void convolve2D(FloatProcessor fpSrc, FloatProcessor fpDest, double[] skern, int kwidth, int kheight) {
        convolve2D(fpSrc.getWidth(), fpSrc.getHeight(), (float[]) fpSrc.getPixels(), (float[]) fpDest.getPixels(), skern, kwidth, kheight);
    }
}
