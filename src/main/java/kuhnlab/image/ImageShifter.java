/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package kuhnlab.image;

import ij.process.ImageProcessor;

/**
 *
 * @author jrkuhn
 */
public class ImageShifter {
    public static void shiftImage(ImageProcessor ip, double xShift, double yShift) {
        int w = ip.getWidth();
        int h = ip.getHeight();

        int ix, iy;
        double y;
        double val;

        // get a copy of the image
        ImageProcessor ipOld = ip.duplicate();

        for (iy=0; iy<h; iy++) {
            y = iy - yShift;
            for (ix=0; ix<w; ix++) {
                val = ipOld.getInterpolatedValue(ix - xShift, y);
                ip.putPixelValue(ix, iy, val);
            }
        }
    }

}
