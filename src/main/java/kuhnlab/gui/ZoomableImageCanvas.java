/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package kuhnlab.gui;

import ij.ImagePlus;
import ij.gui.ImageCanvas;
import java.awt.Dimension;
import java.awt.Rectangle;

/**
 *
 * @author jrkuhn
 */
public class ZoomableImageCanvas extends ImageCanvas {

    public ZoomableImageCanvas(ImagePlus imp) {
        super(imp);
    }
    private static final double[] zoomLevels2 = {
        1 / 72.0, 1 / 48.0, 1 / 32.0, 1 / 24.0, 1 / 16.0, 1 / 12.0,
        1 / 8.0, 1 / 6.0, 1 / 4.0, 1 / 3.0, 1 / 2.0, 0.75, 1.0, 1.5,
        2.0, 3.0, 4.0, 6.0, 8.0, 12.0, 16.0, 24.0, 32.0, 64.0, 128.0, 256.0
    };
    private static final double minZoom2 = zoomLevels2[0];
    private static final double maxZoom2 = zoomLevels2[zoomLevels2.length - 1];

    public static double getLowerZoomLevel(double currentMag) {
        double newMag = zoomLevels2[0];
        for (int i = 0; i < zoomLevels2.length; i++) {
            if (zoomLevels2[i] < currentMag) {
                newMag = zoomLevels2[i];
            } else {
                break;
            }
        }
        return newMag;
    }

    public static double getHigherZoomLevel(double currentMag) {
        double newMag = maxZoom2;
        for (int i = zoomLevels2.length - 1; i >= 0; i--) {
            if (zoomLevels2[i] > currentMag) {
                newMag = zoomLevels2[i];
            } else {
                break;
            }
        }
        return newMag;
    }

    @Override
    public void zoomIn(int sx, int sy) {
        if (magnification >= maxZoom2) {
            return;
        }
        double newMag = getHigherZoomLevel(magnification);
        int newWidth = (int) (imageWidth * newMag);
        int newHeight = (int) (imageHeight * newMag);
        Dimension newSize = canEnlarge(newWidth, newHeight);
        if (newSize != null) {
            setDrawingSize(newSize.width, newSize.height);
            if (newSize.width != newWidth || newSize.height != newHeight) {
                adjustSourceRect2(newMag, sx, sy);
            } else {
                setMagnification(newMag);
            }
            imp.getWindow().pack();
        } else {
            adjustSourceRect2(newMag, sx, sy);
        }
        repaint();
//		if (srcRect.width<imageWidth || srcRect.height<imageHeight)
//			resetMaxBounds();
    }

    void adjustSourceRect2(double newMag, int x, int y) {
        //IJ.log("adjustSourceRect1: "+newMag+" "+dstWidth+"  "+dstHeight);
        int w = (int) Math.round(dstWidth / newMag);
        if (w * newMag < dstWidth) {
            w++;
        }
        int h = (int) Math.round(dstHeight / newMag);
        if (h * newMag < dstHeight) {
            h++;
        }
        x = offScreenX(x);
        y = offScreenY(y);
        Rectangle r = new Rectangle(x - w / 2, y - h / 2, w, h);
        if (r.x < 0) {
            r.x = 0;
        }
        if (r.y < 0) {
            r.y = 0;
        }
        if (r.x + w > imageWidth) {
            r.x = imageWidth - w;
        }
        if (r.y + h > imageHeight) {
            r.y = imageHeight - h;
        }
        srcRect = r;
        setMagnification(newMag);
        //IJ.log("adjustSourceRect2: "+srcRect+" "+dstWidth+"  "+dstHeight);
    }

    @Override
    public void setMagnification(double magnification) {
        setMagnification3(magnification);
    }

    void setMagnification3(double magnification) {
        if (magnification > maxZoom2) {
            magnification = maxZoom2;
        }
        if (magnification < minZoom2) {
            magnification = minZoom2;
        }
        this.magnification = magnification;
        imp.setTitle(imp.getTitle());
    }
}
