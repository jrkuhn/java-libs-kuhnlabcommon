/*
 * ShapeRoi.java
 *
 * Created on February 25, 2004, 3:42 PM
 */

package kuhnlab.gui.overlay;

import ij.IJ;
import ij.ImagePlus;
import ij.ImageStack;
import ij.Prefs;
import ij.gui.Roi;
import ij.process.ColorProcessor;
import ij.util.Java2;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author  jkuhn
 */
public class OverlayRoi extends ij.gui.Roi {
    
    ImagePlus impMaster = null;
    int numSlices = 0;
    boolean isAntialiased;
    protected List<ShapeList> frameShapes = null;
    protected List<Rectangle> frameBounds = null;
    int activeFrame;
    int activeShape;
    public boolean clickRemoves = false;
    
    /** Creates a new instance of ShapeRoi */
    public OverlayRoi(ImagePlus imp, boolean antialiased) {
        super(0, 0, 1, 1);
        impMaster = imp;
        this.isAntialiased = antialiased;
        numSlices = impMaster.getStackSize();
        frameShapes = new ArrayList<ShapeList>(numSlices);
        frameBounds = new ArrayList<Rectangle>(numSlices);
        for (int i=0; i<=numSlices; i++) {
            frameShapes.add(new ShapeList());
            frameBounds.add(null);
        }
        activeFrame = -1;
        activeShape = -1;
    }
    
    protected boolean isBadFrame(int iFrame) {
        // NOTE: frame number starts at 1
        return (iFrame < 1 || iFrame > numSlices);
    }
    
    public void clearShapes (int iFrame) {
        if (isBadFrame(iFrame))
            return;
        frameShapes.get(iFrame - 1).clear();
        frameBounds.set(iFrame - 1, null);
    }
    
    public void setShapeList(int iFrame, ShapeList list) {
        if (isBadFrame(iFrame))
            return;
        frameShapes.get(iFrame - 1).clear();
        frameShapes.set(iFrame - 1, list);
        Rectangle bounds = frameBounds.get(iFrame - 1);
        for (ShapeEntry se : list) {
            Rectangle r = se.shape.getBounds();
            if (bounds == null) {
                bounds = r;
            } else {
                bounds = bounds.union(r);
            }
        }
        frameBounds.set(iFrame, bounds);
        
    }
    
    public void addShape(int iFrame, Shape shape, BasicStroke stroke, Paint foreground, Paint background) {
        if (shape == null || isBadFrame(iFrame))
            return;
        ShapeEntry se = frameShapes.get(iFrame - 1).addShape(shape, stroke, foreground, background);
        updateBounds(iFrame, se);
    }

    public void addShapes(int iFrame, List<Shape> shapes, BasicStroke stroke, Paint foreground, Paint background) {
        if (shapes == null || isBadFrame(iFrame))
            return;
        for (Shape shape : shapes) {
            ShapeEntry se = frameShapes.get(iFrame - 1).addShape(shape, stroke, foreground, background);
            updateBounds(iFrame, se);
        }
    }
    
    public void addString(int iFrame, double x, double y, String str, BasicStroke stroke, Paint foreground, Paint background) {
        if (isBadFrame(iFrame))
            return;
        ShapeEntry se = frameShapes.get(iFrame - 1).addString(x, y, str, stroke, foreground, background);
        updateBounds(iFrame, se);
    }
    
    public void addString(int iFrame, double x, double y, String str, Font font, BasicStroke stroke, Paint foreground, Paint background) {
        if (isBadFrame(iFrame))
            return;
        ShapeEntry se = frameShapes.get(iFrame - 1).addString(x, y, str, font, stroke, foreground, background);
        updateBounds(iFrame, se);
    }
    
    public void setLastMessage(int iFrame, String message) {
        if (isBadFrame(iFrame))
            return;
        ShapeList sl = frameShapes.get(iFrame - 1);
        int size = sl.size();
        if (size < 1)
            return;
        sl.get(size-1).setMessage(message);
    }
    
    protected void updateBounds(int iFrame, ShapeEntry se) {
        Rectangle bounds = frameBounds.get(iFrame - 1);
        Rectangle r = se.shape.getBounds();
        if (bounds == null) {
            bounds = r;
        } else {
            bounds = bounds.union(r);
        }
        frameBounds.set(iFrame - 1, bounds);
    }
    
    protected AffineTransform getCurrentTransform() {
        AffineTransform at = new AffineTransform();
        if (ic != null) {
            double dMag = ic.getMagnification();
            Rectangle srcRect = ic.getSrcRect();
            at.scale(dMag, dMag);
            at.translate(-srcRect.x, -srcRect.y);
        }
        return at;
    }
    
    @Override
    public void draw(java.awt.Graphics graphics) {
        int iFrame = impMaster.getCurrentSlice();
        ShapeList shapes = frameShapes.get(iFrame - 1);

        if (shapes.size() > 0) {
            Graphics2D g2 = (Graphics2D)graphics;
            if (isAntialiased) {
                g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            }
            AffineTransform atOld = g2.getTransform();
            Stroke strokeOld = g2.getStroke();
            Paint paintOld = g2.getPaint();

            g2.setTransform(getCurrentTransform());

            for (ShapeEntry se : shapes) {
                if (se.doFill) {
                    if (se.background != null) g2.setPaint(se.background);
                    g2.fill(se.shape);
                }
                if (se.doDraw) {
                    if (se.stroke != null) g2.setStroke(se.stroke);
                    if (se.foreground != null) g2.setPaint(se.foreground);
                    g2.draw(se.shape);
                }
            }
            if (iFrame == activeFrame && activeShape >= 0) {
                ShapeEntry se = shapes.get(activeShape);
                // Draw a white highlight around the shape
                float width = 1f;
                if (se.stroke == null) {
                    Rectangle2D bounds = se.shape.getBounds2D();
                    double size = (bounds.getWidth() + bounds.getHeight())/2.0;
                    width = Math.min(1f,(float)size);
                } else {
                    width = se.stroke.getLineWidth()*2;
                }
                Shape shapeToHighlight;
                if (se.doDraw) {
                    Stroke strokePath = (se.stroke != null) ? se.stroke : new BasicStroke(1f);
                    shapeToHighlight = strokePath.createStrokedShape(se.shape);
                } else {
                    shapeToHighlight = se.shape;
                }
                BasicStroke strokeHighlightB = new BasicStroke(1.5f*width, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
                BasicStroke strokeHighlightW = new BasicStroke(width, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
                g2.setStroke(strokeHighlightB);
                g2.setPaint(Color.BLACK);
                g2.draw(shapeToHighlight);
                g2.setStroke(strokeHighlightW);
                g2.setPaint(Color.WHITE);
                g2.draw(shapeToHighlight); 
                // draw the original shape inside of the highlight
                if (se.doFill) {
                    if (se.background != null) g2.setPaint(se.background);
                    g2.fill(se.shape);
                }
                if (se.doDraw) {
                    if (se.stroke != null) g2.setStroke(se.stroke);
                    if (se.foreground != null) g2.setPaint(se.foreground);
                    g2.draw(se.shape);
                }
            }
            
            g2.setTransform(atOld);
            g2.setStroke(strokeOld);
            g2.setPaint(paintOld);
        }
    }
    
    @Override
    public int isHandle(int sx, int sy) {
        if (ic == null) {
            activeFrame = -1;
            return clickRemoves ? -1 : 0;
        }
        int iFrame = impMaster.getCurrentSlice();
        ShapeList shapes = frameShapes.get(iFrame - 1);
        if (shapes.size() == 0) {
            activeFrame = -1;
            return clickRemoves ? -1 : 0;
        }
        
        double x = ic.offScreenXD(sx), y = ic.offScreenYD(sy);
        // Go through shapes in reverse order (front shapes first)
        // and look for intersections
        boolean match;
        for (int n = shapes.size()-1; n >= 0; n--) {
            match = false;
            ShapeEntry se = shapes.get(n);
            if (se.doFill && se.shape.contains(x, y)) {
                match = true;
            } else if (se.doDraw && se.stroke != null) {
                if (se.stroke.createStrokedShape(se.shape).contains(x, y)) {
                    match = true;
                }
            }
            if (match) {
                activeFrame = iFrame;
                return n;
            }
        }
        activeFrame = -1;
        return clickRemoves ? -1 : 0;
    }
    
    @Override
    protected void mouseDownInHandle(int handle, int sx, int sy) {
        if (handle >= 0 && activeFrame > 0) {
            activeShape = handle;
            
            ShapeList shapes = frameShapes.get(activeFrame - 1);
            if (shapes.size() == 0)
                return;
            if (activeShape < 0 || activeShape >= shapes.size())
                return;
            ShapeEntry se = shapes.get(activeShape);
            if (se.message != null) {
                IJ.showStatus(se.message);
            }
            imp.draw();
        } else {
            activeFrame = -1;
            activeShape = 0;
        }
    }
    
    @Override
    protected void handleMouseUp(int screenX, int screenY) {
        activeFrame = -1;
        activeShape = 0;
        IJ.showStatus("");
        imp.draw();
    }
    
    @Override
    public boolean contains(int x, int y) {
        return isHandle(x, y) >= 0;
    }
    
    @Override
    public java.awt.Rectangle getBounds() {
        Rectangle bounds = frameBounds.get(impMaster.getCurrentSlice() - 1);
        if (bounds == null) {
            return new Rectangle(0,0,0,0);
        }
        return bounds;
    }

    public ImagePlus convertToImagePlus(String strTitle, int iStartFrame, int iEndFrame, boolean fullFrame) {
        if (ic == null || impMaster == null) {
            return null;
        }
        int masterWidth = impMaster.getWidth();
        int masterHeight = impMaster.getHeight();
        int iOldFrame = impMaster.getCurrentSlice();
        double dMag = ic.getMagnification();
        Rectangle srcRect = ic.getSrcRect();
        int iFinalWidth, iFinalHeight;
        if (fullFrame) {
            iFinalWidth = (int)(masterWidth * dMag);
            iFinalHeight = (int)(masterHeight * dMag);
        } else {
            iFinalWidth = (int)(srcRect.width * dMag);
            iFinalHeight = (int)(srcRect.height * dMag);
        }
        ImageStack stack = new ImageStack(iFinalWidth, iFinalHeight);
        if (iEndFrame > numSlices) 
            iEndFrame = numSlices;
        
        for (int iFrame=iStartFrame; iFrame<=iEndFrame; iFrame++) {
            //Image img = frame.createImage(iFinalWidth, iFinalHeight);
            BufferedImage img = new BufferedImage(iFinalWidth, iFinalHeight, BufferedImage.TYPE_INT_RGB);
            Graphics2D g = img.createGraphics();
            if (isAntialiased) {
                g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            }
            
            impMaster.setSlice(iFrame);
            if (fullFrame) {
                Rectangle fullRect = new Rectangle(0, 0, masterWidth, masterHeight);
                ic.setSourceRect(fullRect);
                Java2.setBilinearInterpolation(g, Prefs.interpolateScaledImages);
                Image srcImg = impMaster.getImage();
                if (img!=null)
                        g.drawImage(srcImg, 0, 0, iFinalWidth, iFinalHeight,
                        0, 0, impMaster.getWidth(), impMaster.getHeight(), null);
                draw(g);
                ic.setSourceRect(srcRect);
            } else {
                ic.paint(g);
            }
            g.dispose();
            
            ColorProcessor cp = new ColorProcessor(img);
            stack.addSlice("Frame "+(iFrame), cp);
        }
        impMaster.setSlice(iOldFrame);
        return new ImagePlus(strTitle, stack);
    }

    public static boolean hasOverlayRoi(ImagePlus impSrc) {
        Roi roi = impSrc.getRoi();
        return OverlayRoi.class.isInstance(roi);
    }
    
    public static void createOverlayedImage(ImagePlus impSrc) {
        if (!hasOverlayRoi(impSrc)) {
            return;
        }
        OverlayRoi overlay = (OverlayRoi)impSrc.getRoi();
	
	int iStartFrame = 1;
	int iEndFrame = impSrc.getStackSize();
	ImagePlus impNew = overlay.convertToImagePlus(impSrc.getTitle()+" Overlay", iStartFrame, iEndFrame, false);
	if (impNew != null) {
	    impNew.show();
	}
    }
}
