/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package kuhnlab.gui.overlay;

import java.awt.BasicStroke;
import java.awt.Paint;
import java.awt.Shape;

/**
 *
 * @author jrkuhn
 */
public class ShapeEntry {
        Shape shape;
        BasicStroke stroke;
        Paint foreground, background;
        boolean doDraw, doFill;
        String message;
        public ShapeEntry(Shape shape, BasicStroke stroke, Paint foreground,
                Paint background, boolean doDraw, boolean doFill) {
            this.shape = shape;
            this.stroke = stroke;
            this.foreground = foreground;
            this.background = background;
            this.doDraw = doDraw;
            this.doFill = doFill;
            this.message = null;
        }
        public void setMessage(String message) {
            this.message = message;
        }
}
