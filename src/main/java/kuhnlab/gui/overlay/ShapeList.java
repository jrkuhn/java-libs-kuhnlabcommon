/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package kuhnlab.gui.overlay;

import java.awt.BasicStroke;
import java.awt.Font;
import java.awt.Paint;
import java.awt.Shape;
import java.awt.font.FontRenderContext;
import java.awt.font.TextLayout;
import java.awt.geom.AffineTransform;

/**
 *
 * @author jrkuhn
 */
public class ShapeList extends java.util.ArrayList<ShapeEntry> {
    static final Font defaultFont = new Font("SansSerif", Font.PLAIN, 8);

    public ShapeEntry addShape(Shape shape, BasicStroke stroke, Paint foreground, Paint background) {
        ShapeEntry se = new ShapeEntry(shape, stroke, foreground, background, foreground != null, background != null);
        this.add(se);
        return se;
    }
    public ShapeEntry addString(double x, double y, String str, Font font, BasicStroke stroke, Paint foreground, Paint background) {
        AffineTransform at = new AffineTransform();
        at.translate(x, y);
        TextLayout tl = new TextLayout(str, font, new FontRenderContext(null, true, false));
        return addShape(tl.getOutline(at), stroke, foreground, background);
    }
    public ShapeEntry addString(double x, double y, String str, BasicStroke stroke, Paint foreground, Paint background) {
        return addString (x, y, str, defaultFont, stroke, foreground, background);
    }
    public void setLastMessage(String message) {
        int size = size();
        if (size < 1)
            return;
        get(size-1).setMessage(message);
    }
}
