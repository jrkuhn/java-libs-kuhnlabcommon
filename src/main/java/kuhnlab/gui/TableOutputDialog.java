/*
 * TableOutputDialog.java
 *
 * Created on March 14, 2006, 8:39 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package kuhnlab.gui;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

/**
 *
 * @author drjrkuhn
 */
public class TableOutputDialog extends JDialog {
    
    public JPanel buttonPanel;
    public JButton okButton;
    public JTable table;
    public JScrollPane tableScrollPane;
    
    public TableOutputDialog(JFrame frame, Object[][] data, String title, boolean isModal) {
        super(frame, isModal);
        setTitle(title);
        //Rectangle bounds = frame.getBounds();
        setLocation(new Point(frame.getWidth(), 0));
        //setLocationRelativeTo(parent);
        int nCols = data[0].length;
        String[] header = new String[nCols];
        Arrays.fill(header, " ");
        TableModel model = new DefaultTableModel(data, header);
        initComponents(model, true);
    }
    
    public TableOutputDialog(JFrame frame, Object[][] data, Object[] header, String title, boolean isModal) {
        super(frame, isModal);
        setTitle(title);
        setLocation(new Point(frame.getWidth(), 0));
        TableModel model = new DefaultTableModel(data, header);
        initComponents(model, true);
    }
    
    public TableOutputDialog(Frame parent, String tabDelimTable, String title, boolean isModal) {
        super(parent, isModal);
        setTitle(title);
        setLocationRelativeTo(parent);
        try {
            BufferedReader reader = new BufferedReader(new StringReader(tabDelimTable));
            String line = reader.readLine().trim();
            List<String[]> rowList = new ArrayList<String[]>();
            line = reader.readLine().trim();
            while (line != null && !line.equals("")) {
                rowList.add(line.split("\t"));
                line = reader.readLine();
            }
            reader.close();
            int nCols = rowList.get(0).length;
            String[] header = new String[nCols];
            Arrays.fill(header, " ");
            TableModel model = new DefaultTableModel((String[][])rowList.toArray(), header);
            initComponents(model, true);
        } catch (IOException ex) {
            initComponents(new DefaultTableModel(), true);
        }
    }
    
    protected void initComponents(TableModel tableModel, boolean boldFirstRow) {
        tableScrollPane = new JScrollPane();
        table = new JTable();
        buttonPanel = new JPanel();
        okButton = new JButton();
        
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        table.setModel(tableModel);
        tableScrollPane.setPreferredSize(new Dimension(100*tableModel.getColumnCount(), 200));
        table.setColumnSelectionAllowed(true);
        if (boldFirstRow) {
            table.setDefaultRenderer(Object.class, new DefaultTableCellRenderer() {
                Color normBack, selBack, normFore, selFore;
                public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                    Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                    if (normBack==null && !isSelected) {
                        normBack = c.getBackground();
                        normFore = c.getForeground();
                    } else if (selBack==null && isSelected) {
                        selBack = c.getBackground();
                        selFore = c.getForeground();
                    }
                    if (row == 0) {
                        c.setBackground(normFore);
                        c.setForeground(normBack);
                    } else {
                        c.setBackground(isSelected ? selBack : normBack);
                        c.setForeground(isSelected ? selFore : normFore);
                    }
                    return c;
                }
            });
        }
        tableScrollPane.setViewportView(table);
        tableScrollPane.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createEmptyBorder(5,5,5,5),
                BorderFactory.createLineBorder(SystemColor.controlDkShadow)));
        
        getContentPane().add(tableScrollPane, java.awt.BorderLayout.CENTER);
        
        okButton.setText("OK");
        okButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dispose();
            }
        });
        
        buttonPanel.add(okButton);
        getContentPane().add(buttonPanel, java.awt.BorderLayout.SOUTH);
        getRootPane().setDefaultButton(okButton);
        pack();
    }
    
    // Allow the ESCAPE key to close
    protected JRootPane createRootPane() {
        ActionListener actionListener = new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                dispose();
            }
        };
        JRootPane rootPane = new JRootPane();
        KeyStroke stroke = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
        rootPane.registerKeyboardAction(actionListener, stroke, JComponent.WHEN_IN_FOCUSED_WINDOW);
        return rootPane;
    }
    
}
