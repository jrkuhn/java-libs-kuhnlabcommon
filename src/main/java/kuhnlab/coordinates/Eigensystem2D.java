/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package kuhnlab.coordinates;

/**
 *
 * @author jrkuhn
 */
public class Eigensystem2D {
    static final double ALMOST_ZERO = 1.0e-99;
    

    public double lambda1, lambda2;    // scalar eignevalues
    public KPoint2D e1 = new KPoint2D();
    public KPoint2D e2 = new KPoint2D();

    /**
     * Calculate the eigenvalues and eigenvectors for a 2x2 symmetric matrix
     *
     *       | a  b |
     *   A = |      |
     *       | b  c |
     */
    public void calcFrom(double a, double b, double c) {
        double temp, bsq, sqrtbsqacsq, Len;
        temp = a - c;
        bsq = b * b;
        sqrtbsqacsq = Math.sqrt(4 * bsq + temp * temp);  // sqrt(4b^2+(a-c)^2)

        // Eigenvalues found by solving the equation 
        //      det(A - lambda I) = 0
        //
        // For a symmetric matrix A, the solution is:
        //      lambda1 = (a + c + sqrt(4b^2 + (a-c)^2))/2
        //      lambda2 = (a + c - sqrt(4b^2 + (a-c)^2))/2
        // Eigenvalue 1 should always be greater than eigenvalue 2 since
        // the sqrt term is always positive
        lambda1 = ((a + c) + sqrtbsqacsq) / 2;
        lambda2 = ((a + c) - sqrtbsqacsq) / 2;

        // Calculate the corresponding eigenvectors using the equation
        //      (A - lambda I) e = 0
        //
        // which gives two equations:
        //      a*ex + b*ey - lambda*ex = 0
        //      b*ex + c*ey - lambda*ey = 0
        //
        // In the solutions, ex and ey are not independent. However
        // we can still use their relative dependence to calculate
        // normal vectors in the direction of each eigenvector
        //
        // There are two forms of solutions for the normal vectors ex, ey
        // depending on which row of the above equation we choose to solve
        //
        // The first form uses the solution ex/ey = b / (lambda - a)
        // The length of the eigenvector is L = sqrt(b^2 + (lambda - a)^2)
        // with corresponding normals:
        //  ex = b / L                 and     ey = (lambda - a) / L
        //
        // The second form uses the solution ex/ey = (lambda - c) / b
        // The length of the eigenvector is L = sqrt(b^2 + (lambda - c)^2)
        // with corresponding normals:
        //  ex = (lambda - c) / L      and     ey = b / L
        //
        // The correct form to use depends on which length L is non-zero


        // Test for lambda1 = lambda1. This is a special case where
        // all vectors are eigenvectors. So, we choose two orthagonal 
        // vectors at random. Since lambda1 and lambda2 differ by the
        // value of sqrtbsqacsq, we can just test sqrtbsqacsq
        if (sqrtbsqacsq < ALMOST_ZERO) {
            // lambda1 == lambda2
            // so all vectors are eigenvectors. Choose two
            e1.x = 1;
            e1.y = 0;
            e2.x = 0;
            e2.y = 1;
            return;
        }


        // Calculate e1 by determining which form is best

        // Try the first form
        temp = lambda1 - a;
        Len = Math.sqrt(bsq + temp * temp);
        if (Len > ALMOST_ZERO) {
            // use the first form
            e1.x = b / Len;
            e1.y = temp / Len;
        } else {
            // use the second form
            temp = lambda1 - c;
            Len = Math.sqrt(bsq + temp * temp);
            e1.x = temp / Len;
            e1.y = b / Len;
        }

        // Calculate e2 by determining which form is best

        // Try the first form
        temp = lambda2 - a;
        Len = Math.sqrt(bsq + temp * temp);
        if (Len > ALMOST_ZERO) {
            // use the first form
            e2.x = b / Len;
            e2.y = temp / Len;
        } else {
            // use the second form
            temp = lambda2 - c;
            Len = Math.sqrt(bsq + temp * temp);
            e2.x = temp / Len;
            e2.y = b / Len;
        }
    }
}
