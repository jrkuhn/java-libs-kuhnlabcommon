/*
 * point.java
 *
 * Created on December 20, 2004, 3:01 PM
 */

package kuhnlab.coordinates;
import java.io.Serializable;
import java.math.BigDecimal;
import kuhnlab.xml.XDomItem;
import kuhnlab.xml.XDomTranslate;
	
/** A class to represent 2-dimensional points and points.
 * @author Jeffrey R. Kuhn (jeffrey.kuhn@yale.edu)
*/
public class KPoint3D implements Serializable, XDomTranslate {
    public static final String tagTPoint = "point3d";
    public static final String tagX = "x";
    public static final String tagY = "y";
    public static final String tagZ = "z";
    
    public double x;
    public double y;
    public double z;
    
    /** Default constructor. Set's coordinates to zero. */
    public KPoint3D() { 
    }
    
    /** Construct a point from specified coordinates.
     * @param x new x coordinate.
     * @param y new y coordinate.
     * @param z new z coordinate. */
    public KPoint3D(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
    
    /** Construct a point from an array of 2 values.
     * @param array array containing {x,y,z} */
    public KPoint3D(double[] array) {
        this(array[0], array[1], array[2]);
    }
    
    /** Construct a point from another point.
     * @param t point containing new x, y and z coordinates. */
    public KPoint3D(KPoint3D t) {
        this(t.x, t.y, t.z);
    }
    
    /** Construct a point from specified coordinates.
     * @param p2d 2-D point containing x and y coordinates.
     * @param z new z coordinate. */
    public KPoint3D(KPoint2D p2d, double z) {
        this.x = p2d.x;
        this.y = p2d.y;
        this.z = z;
    }
    
    /** Construct a point from this point. */
    @Override
    public Object clone() {
        return new KPoint3D(this.x, this.y, this.z);
    }
    
    /** Returns the hash code for the point. */
    @Override
    public int hashCode() {
        long hash = java.lang.Double.doubleToLongBits(x);
        hash = (hash<<5) - hash + java.lang.Double.doubleToLongBits(y);
        hash = (hash<<5) - hash + java.lang.Double.doubleToLongBits(z);
        return (int) (hash ^ (hash >>> 32));
    }
    
    /** Convert point to string. */
    @Override
    public String toString() {
        return "(" + x + "," + y + "," + z + ")";
    }
    
    /** Convert point to string with given precision. */
    public String toString(int precision) {
        String fmt = "%."+precision+"f";
        return "(" + String.format(fmt,x) + ", " + String.format(fmt,y) + ", " + String.format(fmt,z) + ")";
    }
    
    
    /************************************************************************
     *** XML input/output                                                 ***
     ************************************************************************/

    /** Create a new Point from an XML element */
    public static KPoint3D TPoint3D_fromXDomItem(XDomItem item) {
        KPoint3D t = new KPoint3D();
        return (t.fromXDomItem(item)) ? t : null;
    }

    /** Fill in this object from an XML element. */
    public boolean fromXDomItem(XDomItem item) {
        if (item == null) return false;
        if (!item.isElementNamed(tagTPoint)) return false;
        x = item.getDouble(tagX, 0);
        y = item.getDouble(tagY, 0);
        z = item.getDouble(tagZ, 0);
        return true;
    }
    
    /** Create an XML Element from this object. */
    public XDomItem toXDomItem(XDomItem.Factory factory) {
        if (factory == null) return null;
        XDomItem item = factory.createElement(tagTPoint);
        if (item == null) return null;
        item.setDouble(tagX, x);
        item.setDouble(tagY, y);
        item.setDouble(tagZ, z);
        return item;
    }

    /************************************************************************
     *** OPERATIONS ON COORDINATES                                        ***
     ************************************************************************/
    
    /** Set all coordinates to zero. */
    public void clear() {
        this.x = 0;  this.y = 0;  this.z = 0;
    }
    
    /** Sets all coordinates to NaN. */
    public void toNaN() {
        this.x = java.lang.Double.NaN;
        this.y = java.lang.Double.NaN;
        this.z = java.lang.Double.NaN;
    }
    
    /**
     * Check if this point is NaN.
     * @return true if one of the coordinates is NaN
     *  and false if all coordinates are valid. */
    public boolean isNaN() {
        if (java.lang.Double.isNaN(this.x)) return true;
        if (java.lang.Double.isNaN(this.y)) return true;
        if (java.lang.Double.isNaN(this.z)) return true;
        return false;
    }
    
    /** Sets all coordinates to Infinity. */
    public void toInfinite() {
        this.x = java.lang.Double.POSITIVE_INFINITY;
        this.y = java.lang.Double.POSITIVE_INFINITY;
        this.z = java.lang.Double.POSITIVE_INFINITY;
    }
    
    /** Sets all coordinates to Negative Infinity. */
    public void toNegativeInfinite() {
        this.x = java.lang.Double.NEGATIVE_INFINITY;
        this.y = java.lang.Double.NEGATIVE_INFINITY;
        this.z = java.lang.Double.NEGATIVE_INFINITY;
    }

    /** Check if this point is infinite.
     * @return true if one of the coordinates is infinite
     *  and false if all coordinates are valid. */
    public boolean isInfinite() {
        if (java.lang.Double.isInfinite(this.x)) return true;
        if (java.lang.Double.isInfinite(this.y)) return true;
        if (java.lang.Double.isInfinite(this.z)) return true;
        return false;
    }
    
    /** Get the x coordinate.
     * @return Value of x coordinate. */
    public double getX() { return this.x; }
    /** Get the y coordinate.
     * @return Value of y coordinate. */
    public double getY() { return this.y; }
    /** Get the z coordinate.
     * @return Value of z coordinate. */
    public double getZ() { return this.z; }
    
    
    /** Set the x coordinate.
     * @param x New value of x coordinate. */
    public void setX(double x) { this.x = x; }
    
    /** Set the y coordinate.
     * @param y New value of y coordinate. */
    public void setY(double y) { this.y = y; }
    
    /** Set the z coordinate.
     * @param z New value of z coordinate. */
    public void setZ(double z) { this.z = z; }
    
    /** Specified the coordinates.
     * @param x new x coordinate.
     * @param y new y coordinate.
     * @param z new z coordinate. */
    public void set(double x, double y, double z) {
        this.x = x; this.y = y; this.z = z;
    }
    
    /** Specify the coordinates from an array of 2 values.
     * @param array array containing {x,y,z} */
    public void set(double[] array) {
        this.x = array[0]; this.y = array[1]; this.z = array[2];
    }
    
    /** Specify the coordinates from another point.
     * @param t point containing new x, y, and z coordinates. */
    public void set(KPoint3D t) {
        this.x = t.x; this.y = t.y; this.z = t.z;
    }
    
    /************************************************************************
     *** UNITARY OPERATIONS RETURNING point VALUES                       ***
     ************************************************************************/
    
//    /*** Polar coordinates ***/
//    
//    /** Sets the coordinates of this point from polar coordinates.
//     *  @param length length of the point.
//     *  @param angle angle (in radians) from the x-axis.
//     */
//    public KPoint3D toPolar(double length, double angle) {
//        this.x = length*Math.cos(angle);
//        this.y = length*Math.sin(angle);
//        return this;
//    }
//    
//    /** Returns a new point from polar coordinates.
//     *  @param length length of the point.
//     *  @param angle angle (in radians) from the x-axis.
//     */
//    static public KPoint3D newPolar(double length, double angle) {
//        return new KPoint3D().toPolar(length, angle);
//    }
    
    /*** Rounding ***/
    
    /** Sets the coordinates of this point to the rounded
     *  values of coordinates in p1. */
    public KPoint3D toRoundOf(KPoint3D p1, double place) {
        BigDecimal bplace, b, br, bs;
        bplace = new BigDecimal(place);
        b = new BigDecimal(p1.x);
        br = b.divide(bplace, 0, BigDecimal.ROUND_HALF_UP);
        bs = br.multiply(bplace);
        this.x = bs.doubleValue();
        b = new BigDecimal(p1.y);
        br = b.divide(bplace, 0, BigDecimal.ROUND_HALF_UP);
        bs = br.multiply(bplace);
        this.y = bs.doubleValue();
        b = new BigDecimal(p1.z);
        br = b.divide(bplace, 0, BigDecimal.ROUND_HALF_UP);
        bs = br.multiply(bplace);
        this.z = bs.doubleValue();
        return this;
    }
    
    /** Returns a new point containing the rounded value of the coordinates
     *  of p1. */
    static public KPoint3D newRoundOf(KPoint3D p1, double place) {
        return new KPoint3D().toRoundOf(p1, place);
    }
    
    /** Set coordinates to absolute values. */
    public KPoint3D round(double place) {
        return toRoundOf(this, place);
    }
    
    /*** Absolute values ***/
    
    /** Sets the coordinates of this point to the absolute
     *  value of coordinates in p1. */
    public KPoint3D toAbsOf(KPoint3D p1) {
        this.x = Math.abs(p1.x);  this.y = Math.abs(p1.y);  this.z = Math.abs(p1.z);
        return this;
    }
    
    /** Returns a new point containing the absolute value of the coordinates
     *  of p1. */
    static public KPoint3D newAbsOf(KPoint3D p1) {
        return new KPoint3D().toAbsOf(p1);
    }
    
    /** Set coordinates to absolute values. */
    public KPoint3D abs() {
        return toAbsOf(this);
    }
    
    /*** Negative values ***/
    
    /** Sets the coordinates of this point to the negative
     *  value of coordinates in p1. */
    public KPoint3D toNegOf(KPoint3D p1) {
        this.x = -p1.x;
        this.y = -p1.y;
        this.z = -p1.z;
        return this;
    }
    
    /** Returns a new point containing the negative value of the coordinates
     *  of p1. */
    static public KPoint3D newNegOf(KPoint3D p1) {
        return new KPoint3D().toNegOf(p1);
    }
    
    /** Set coordinates to negative values. */
    public KPoint3D neg() {
        return toNegOf(this);
    }
    
    /*** Normalization ***/
    
    /** Sets this point to the normal of p1. Sets the
     * coordinates to Double.POSITIVE_INFINITY if the
     * length of p1 was zero.
     * @param p1
     */
    public KPoint3D toNormOf(KPoint3D p1) {
        double len = Math.sqrt(p1.x*p1.x + p1.y*p1.y + p1.z*p1.z);
        if (len == 0.0) {
            this.x = this.y = this.z = 0.0;
        } else {
            this.x = p1.x / len;
            this.y = p1.y / len;
            this.z = p1.z / len;
        }
        return this;
    }
    
    /** Returns a new point containing normalized coordinates of p1.
     *  Sets the coordinates to Double.POSITIVE_INFINITY
     *  if the length was zero. */
    static public KPoint3D newNormOf(KPoint3D p1) {
        return new KPoint3D().toNormOf(p1);
    }
    
    /** Sets the length of this point to 1.
     *  Sets the coordinates to Double.POSITIVE_INFINITY
     *  if the length was zero. */
    public KPoint3D norm() {
        return toNormOf(this);
    }
    
    /************************************************************************
     *** BINARY OPERATIONS RETURNING point VALUES                        ***
     ************************************************************************/
    
    /*** Addition ***/
    
    /** Sets the coordinates of this point to the
     * sum of the coordiantes of p1 and p2. */
    public KPoint3D toSumOf(KPoint3D p1, KPoint3D p2) {
        this.x = p1.x + p2.x;
        this.y = p1.y + p2.y;
        this.z = p1.z + p2.z;
        return this;
    }
    
    /** Returns a new point containing the sum of the coordinates
     *  of p1 and p2. */
    static public KPoint3D newSumOf(KPoint3D p1, KPoint3D p2) {
        return new KPoint3D().toSumOf(p1, p2);
    }
    
    /** Adds the coordinates of p2 to this point. */
    public KPoint3D add(KPoint3D p2) {
        return toSumOf(this, p2);
    }
    
    /*** Subtraction ***/
    
    /** Sets the coordinates of this point to p1 - p2. */
    public KPoint3D toDiffOf(KPoint3D p1, KPoint3D p2) {
        this.x = p1.x - p2.x;
        this.y = p1.y - p2.y;
        this.z = p1.z - p2.z;
        return this;
    }
    
    /** Returns a new point containing the difference between the coordinates
     *  of p1 and p2. */
    static public KPoint3D newDiffOf(KPoint3D p1, KPoint3D p2) {
        return new KPoint3D().toDiffOf(p1, p2);
    }
    
    /** Subtract the coordinates of p2 from this point. */
    public KPoint3D sub(KPoint3D p2) {
        return toDiffOf(this, p2);
    }
    
    /*** Scaling ***/
    
    /** Sets these coordinates to coordinates of p1 multiplied by a constant
     *  scale factor. */
    public KPoint3D toProdOf(double scale, KPoint3D p1) {
        this.x = scale * p1.x;  this.y = scale * p1.y;  this.z = scale * p1.z;
        return this;
    }
    
    /** Return a new point having coordinates of p1 multiplied by a constant
     *  scale factor. */
    static public KPoint3D newProdOf(double scale, KPoint3D p1) {
        return new KPoint3D().toProdOf(scale, p1);
    }
    
    /** Multiply coordinates by a constant scale factor. */
    public KPoint3D scale(double scale) {
        return toProdOf(scale, this);
    }
    
    /** Sets these coordinates to coordinates of p1 multiplied by constant
     *  scale factors. */
    public KPoint3D toProdOf(double xscale, double yscale, double zscale, KPoint3D p1) {
        this.x = xscale * p1.x;  this.y = yscale * p1.y;  this.z = zscale * p1.z;
        return this;
    }
    
    /** Return a new point having coordinates of p1 multiplied by constant
     *  scale factors. */
    static public KPoint3D newProdOf(double xscale, double yscale, double zscale, KPoint3D p1) {
        return new KPoint3D().toProdOf(xscale, yscale, zscale, p1);
    }
    
    /** Multiply coordinates by a constant scale factor. */
    public KPoint3D scale(double xscale, double yscale, double zscale) {
        return toProdOf(xscale, yscale, zscale, this);
    }
    
    /************************************************************************
     *** MISCELANEOUS point OPERATIONS                                   ***
     ************************************************************************/

    /** returns a KPoint2D representation of this point without Z information */
    public KPoint2D projectZ() {
            return new KPoint2D(x, y);
    }
	
    
    /** Sets the coordinates of this point to the linear interpolation
     *  between p1 and p2. Alpha should be in the range [0,1]. An alpha
     *  of 0 would give p1 and an alpha of 1 would give p2. */
    public KPoint3D toInterpolateBetween(double alpha, KPoint3D p1, KPoint3D p2) {
        this.x = (1 - alpha) * p1.x + alpha * p2.x;
        this.y = (1 - alpha) * p1.y + alpha * p2.y;
        this.z = (1 - alpha) * p1.z + alpha * p2.z;
        return this;
    }
    
    /** determine the minimum x, y, and z coordinates  */
    public KPoint3D toMinOf(KPoint3D p1, KPoint3D p2) {
        this.x = Math.min(p1.x, p2.x);
        this.y = Math.min(p1.y, p2.y);
        this.z = Math.min(p1.z, p2.z);
        return this;
    }

    /** determine the maximum x, y, and z coordinates  */
    public KPoint3D toMaxOf(KPoint3D p1, KPoint3D p2) {
        this.x = Math.max(p1.x, p2.x);
        this.y = Math.max(p1.y, p2.y);
        this.z = Math.max(p1.z, p2.z);
        return this;
    }

    
//    /** Sets the coordinates of this point to the intersection
//     *  between line segment AB and line segment CD
//     *	or NaN if these two lines do not intersect */
//    public KPoint3D toIntersectionOf(KPoint3D A, KPoint3D B, KPoint3D C, KPoint3D D) {
//        double p1, p2, denom;
//        // The equations for any point along either line are give by
//        //	P1 = A + p1 (B - A)
//        //	P2 = C + p2 (D - C)
//        // Look for the intersection by setting P1==P2 and solving for p1 and p2
//        //
//        //      (Dx - Cx)(Ay - Cy) - (Dy - Cy)(Ax - Cx)
//        // p1 = ---------------------------------------
//        //      (Dy - Cy)(Bx - Ax) - (Dx - Cx)(By - Ay)
//        //
//        //      (Bx - Ax)(Ay - Cy) - (By - Ay)(Ax - Cx)
//        // p2 = ---------------------------------------
//        //      (Dy - Cy)(Bx - Ax) - (Dx - Cx)(By - Ay)
//        //
//        // The lines intersect if 0<=p1<=1 and 0<=p2<=1
//        //
//        denom = (D.y - C.y)*(B.x - A.x) - (D.x - C.x)*(B.y - A.y);
//        if (denom == 0) {
//            // the lines are parallel
//            this.toNaN();
//            return this;
//        }
//        p1 = ((D.x - C.x)*(A.y - C.y) - (D.y - C.y)*(A.x - C.x)) / denom;
//        p2 = ((B.x - A.x)*(A.y - C.y) - (B.y - A.y)*(A.x - C.x)) / denom;
//        
//        if ((p1 < 0 || p1 > 1.0) || (p2 < 0 || p2 > 1.0)) {
//            this.toNaN();
//            return this;
//        }
//        
//        // the lines intersect. set this to the intersection point
//        this.x = A.x + p1*(B.x - A.x);
//        this.y = A.y + p1*(B.y - A.y);
//        return this;
//    }
//    
//    /** find the shortest distance between the line segment AB and the
//     *  point C by dropping a perpendicular from C to AB. The coordinates of
//     *  this point are set to a point along AB and CP which perpendicular
//     *  to AB. If this point is outside of AB, then it is set to NaN.
//     */
//    public KPoint3D toPerpProjOf(KPoint3D A, KPoint3D B, KPoint3D C) {
//        double lensq, r, dx, dy;
//        // The value r represents the distance from A along AB at
//        // which P is located. It is given by the equation
//        //       AC dot AB
//        //   r = ---------
//        //        |AB|^2
//        //
//        //  If 0 <= r <= 1 Then P lies on the segement AB and
//        //  P is given by Px = Ax + r(Bx-Ax); Py = Ay + r(By-Ay)
//        dx = B.x - A.x;
//        dy = B.y - A.y;
//        lensq = dx*dx + dy*dy;
//        if (lensq == 0.0) {
//            this.toNaN();
//            return this;
//        }
//        r = ((C.x - A.x)*dx + (C.y - A.y)*dy)/lensq;
//        
//        if (r < 0 || r > 1) {
//            this.toNaN();
//        } else {
//            this.x = A.x + r*dx;
//            this.y = A.y + r*dy;
//        }
//        return this;
//    }
//
//    /** Sets this point perpendicular to p1 by rotating p1
//     *  90 degrees counter-clockwise. */
//    public KPoint3D toPerpOf(KPoint3D p1) {
//        this.x = -p1.y;
//        this.y = p1.x;
//        return this;
//    }
//    
//    /** Rotates this point by angle (specified in radians). */
//    public KPoint3D rotateBy(double dAngle) {
//        double xold = this.x;
//        double yold = this.y;
//        //  /  \   /                          \   /  \
//        //  |Bx|   |  cos(theta)  -sin(theta) |   |Ax|
//        //  |  | = |                          | x |  |
//        //  |By|   |  sin(theta)   cos(theta) |   |Ay|
//        //  \  /   \                          /   \  /
//        double dCos=Math.cos(dAngle), dSin=Math.sin(dAngle);
//        this.x = xold*dCos - yold*dSin;
//        this.y = xold*dSin + yold*dCos;
//        return this;
//    }
    
    /************************************************************************
     *** OPERATIONS RETURNING SCALAR VALUES                               ***
     ************************************************************************/
    
    /** Returns the distance between p1 and p2. */
    static public double distBetween(KPoint3D p1, KPoint3D p2) {
        double dx = p2.x - p1.x;
        double dy = p2.y - p1.y;
        double dz = p2.z - p1.z;
        return Math.sqrt(dx*dx + dy*dy + dz*dz);
    }
    
    /** Returns the square of the distance between p1 and p2. */
    static public double distSqBetween(KPoint3D p1, KPoint3D p2) {
        double dx = p2.x - p1.x;
        double dy = p2.y - p1.y;
        double dz = p2.z - p1.z;
        return dx*dx + dy*dy + dz*dz;
    }
    
    /** Computes the L1 distance, |Dx|+|Dy|, between p1 and p2. */
    static public double distL1Between(KPoint3D p1, KPoint3D p2) {
        double dx = p2.x - p1.x;
        double dy = p2.y - p1.y;
        double dz = p2.z - p1.z;
        if (dx < 0) dx = -dx;
        if (dy < 0) dy = -dy;
        if (dz < 0) dz = -dz;
        return dx + dy + dz;
    }
    
    /** Computes the L-infinite distance, max(|Dx|,|Dy|),
     * between p1 and p2. */
    static public double distLinfBetween(KPoint3D p1, KPoint3D p2) {
        double dx = p2.x - p1.x;
        double dy = p2.y - p1.y;
        double dz = p2.z - p1.z;
        if (dx < 0) dx = -dx;
        if (dy < 0) dy = -dy;
        if (dz < 0) dz = -dz;
        return Math.max(dx, Math.max(dy, dz));
    }
    
    /** Computes the distance between this and p2. */
    public double distTo(KPoint3D p2) {
        return distBetween(this, p2);
    }
    
    /** Computes the square of the distance between this and p2. */
    public double distSqTo(KPoint3D p2) {
        return distSqBetween(this, p2);
    }
    
    /** Computes the L1 distance, |Dx|+|Dy|, between this and p2. */
    public double distL1To(KPoint3D p2) {
        return distL1Between(this, p2);
    }
    
    /** Computes the L-infinite distance, max(|Dx|,|Dy|),
     * between this and p2. */
    public double distLinfTo(KPoint3D p2) {
        return distLinfBetween(this, p2);
    }
    
    /** Returns the length of p1.*/
    static public double lengthOf(KPoint3D p1) {
        return Math.sqrt(p1.x*p1.x + p1.y*p1.y + p1.z*p1.z);
    }
    
    /** Returns the square of the length of p1. */
    static public double lengthSqOf(KPoint3D p1) {
        return p1.x*p1.x + p1.y*p1.y + p1.z*p1.z;
    }
    
    /** Returns the length of this point.*/
    public double length() {
        return lengthOf(this);
    }
    
    /** Returns the square of the length of this point. */
    public double lengthSq() {
        return lengthSqOf(this);
    }
    
    /** Returns true if all of the coordinates
     *  of p1 and p2 are equal. */
    static public boolean equals(KPoint3D p1, KPoint3D p2) {
        return p1.x == p2.x && p1.y == p2.y && p1.z == p2.z;
    }
    
    /** Returns true if all of the coordinates
     *  of p1 are within 'epsilon' of the coordinates
     *  of p2. */
    static public boolean epsilonEquals(KPoint3D p1, KPoint3D p2, double epsilon) {
        double d;
        d = p2.x - p1.x;
        if (d < 0)
            d = -d;
        if (d > epsilon)
            return false;
        d = p2.y - p1.y;
        if (d < 0)
            d = -d;
        if (d > epsilon)
            return false;
        d = p2.z - p1.z;
        if (d < 0)
            d = -d;
        if (d > epsilon)
            return false;
        return true;
    }
    
    /** Returns true if all of the coordinates
     *  of this and p2 are equal. */
    public boolean equals(KPoint3D p2) {
        return equals(this, p2);
    }
    
    /** Returns true if all of the coordinates
     *  of this are within 'epsilon' of the coordinates
     *  of p2. */
    public boolean epsilonEquals(KPoint3D p2, double epsilon) {
        return epsilonEquals(this, p2, epsilon);
    }
    
//    /** Find the angle (in radians) between the points A and B. */
//    static public double angleBetween(KPoint3D A, KPoint3D B) {
//        double dot = A.x*B.x + A.y*B.y;     // = |A||B|cos(theta)
//        double cross = A.x*B.y - A.y*B.x;   // = |A||B|sin(theta)
//        return Math.atan2(cross, dot);
//    }
//    
//    /** Find the angle (in radians) between this and B. */
//    public double angleTo(KPoint3D B) {
//        return angleBetween(this, B);
//    }
    
    /** Returns the dot product between p1 and p2. */
    static public double dotOf(KPoint3D p1, KPoint3D p2) {
        return p1.x*p2.x + p1.y*p2.y + p1.z*p2.z;
    }
    
    /** Returns the dot product between this and p2. */
    public double dot(KPoint3D p2) {
        return dotOf(this, p2);
    }
}
