/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package kuhnlab.coordinates;

import ij.gui.Roi;
import ij.process.ByteProcessor;
import ij.process.FloatProcessor;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.RenderingHints;
import java.awt.geom.Area;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.ejml.alg.dense.linsol.LinearSolver;
import org.ejml.alg.dense.linsol.LinearSolverFactory;
import org.ejml.data.DenseMatrix64F;

/**
 *
 * @author jrkuhn
 */
public class Snake extends KPolygon2D {

    public Snake() {
        super();
    }
    
    /** Create a points without copying the points (shallow copy. */
    public Snake(Collection<KPoint2D> points) {
        super(points, true);
    }

    /** Create a points from an ImageJ ROI */
    public Snake(Roi roi) {
        Polygon poly = roi.getPolygon();
        points = new ArrayList<KPoint2D>(poly.npoints);
        int x, y;
        for (int i = 0; i < poly.npoints; i++) {
            x = poly.xpoints[i];
            y = poly.ypoints[i];
            points.add(new KPoint2D(x, y));
        }
        isClosed = true;
    }

    /** Deep Copy constructor */
    public Snake(Snake src) {
        super(src);
    }

    public static class EvolveResults {
        public double maxDist;
        public double avgDist;
        public double minDist;
    }

    public EvolveResults evolve(double alpha, double beta, double gamma, double kappa,
            FloatProcessor fpU, FloatProcessor fpV, int numIterations) {
        int nPoints = points.size();
        //IJ.write("evolving " + nPoints + " point points");

        double a = beta;
        double b = -alpha - 4 * beta;
        double c = 6 * beta + 2 * alpha;

        c = c + gamma;  // to make matrix (A + gamma*I)

        // create a symmetric pentadiagonal matrix to hold a, b, and c
        //
        // | c b a 0 0 0 0 ... a b |
        // | b c b a 0 0 0 ... 0 a |
        // | a b c b a 0 0 ... 0 0 |
        // | 0 a b c b a 0 ... 0 0 |
        // | 0 0 a b c b a ... 0 0 |
        // |           :           |
        // | 0 0 ... a b c b a 0 0 |
        // | 0 0 ... 0 a b c b a 0 |
        // | 0 0 ... 0 0 a b c b a |
        // | a 0 ... 0 0 0 a b c b |
        // | b a ... 0 0 0 0 a b c |

        DenseMatrix64F matA = new DenseMatrix64F(nPoints, nPoints);
        //DoubleMatrix2D matA = new DenseDoubleMatrix2D(nPoints, nPoints);
        //Matrix matA = new Matrix(nPoints, nPoints);
        int im2, im1, ip1, ip2;
        for (int i = 0; i < nPoints; i++) {
            im2 = i - 2;
            im1 = i - 1;
            ip1 = i + 1;
            ip2 = i + 2;
            // wrap the values
            if (im2 < 0) {
                im2 += nPoints;
            }
            if (im1 < 0) {
                im1 += nPoints;
            }
            if (ip1 >= nPoints) {
                ip1 -= nPoints;
            }
            if (ip2 >= nPoints) {
                ip2 -= nPoints;
            }
            matA.set(i, im2, a);
            matA.set(i, im1, b);
            matA.set(i, i, c);
            matA.set(i, ip1, b);
            matA.set(i, ip2, a);
        }

        // convert points points to column matrices
        DenseMatrix64F matXCurrent = new DenseMatrix64F(nPoints, 1);
        DenseMatrix64F matYCurrent = new DenseMatrix64F(nPoints, 1);

        KPoint2D pt;
        for (int i = 0; i < nPoints; i++) {
            pt = points.get(i);
            matXCurrent.set(i, 0, pt.x);
            matYCurrent.set(i, 0, pt.y);
        }

        // Iteratively solve the equations
        //
        //  (A + gamma I) x_next = x_current + kappa u(x,y)
        //
        //  (A + gamma I) y_next = y_current + kappa v(x,y)
        //
        // for x_next and y_next


        // Precompute an LU decomposition (inverse) of A to solve
        //    A*x = B
        // for 'x' where x = x_next OR y_next
        // and B = x_current + kappa*u  OR  y_current + kappa*v

        LinearSolver solverA = LinearSolverFactory.linear(nPoints);
        solverA.setA(matA);

        double x, y, u, v;
        DenseMatrix64F matXNext, matYNext;
        for (int iter = 0; iter < numIterations; iter++) {
            // add the weighted external gradient to each point

            DenseMatrix64F matXPlusU = new DenseMatrix64F(nPoints, 1);
            DenseMatrix64F matYPlusV = new DenseMatrix64F(nPoints, 1);
            for (int i = 0; i < nPoints; i++) {
                x = matXCurrent.get(i, 0);
                y = matYCurrent.get(i, 0);
                u = fpU.getInterpolatedPixel(x, y);
                v = fpV.getInterpolatedPixel(x, y);

                matXPlusU.set(i, 0, x + kappa * u);
                matYPlusV.set(i, 0, y + kappa * v);
            }

            // Solve A*x = B
            matXNext = new DenseMatrix64F(nPoints, 1);
            solverA.solve(matXPlusU, matXNext);
            matYNext = new DenseMatrix64F(nPoints, 1);
            solverA.solve(matYPlusV, matYNext);

            matXCurrent = matXNext;
            matYCurrent = matYNext;

        }// END for iter

        // calculate how much the points moved
        KPoint2D ptStart;
        double dist;
        EvolveResults res = new EvolveResults();
        res.maxDist = Double.NEGATIVE_INFINITY;
        res.minDist = Double.POSITIVE_INFINITY;
        res.avgDist = 0;
        for (int i = 0; i < nPoints; i++) {
            ptStart = points.get(i);
            x = matXCurrent.get(i,0);
            y = matYCurrent.get(i,0);
            x = x - ptStart.x;
            y = y - ptStart.y;
            dist = Math.sqrt(x*x + y*y);
            res.avgDist += dist;
            if (dist > res.maxDist) {
                res.maxDist = dist;
            }
            if (dist < res.minDist) {
                res.minDist = dist;
            }
        }
        res.avgDist = res.avgDist / nPoints;

        for (int i = 0; i < nPoints; i++) {
            x = matXCurrent.get(i, 0);
            y = matYCurrent.get(i, 0);
            points.get(i).set(x, y);
        }
        return res;
    }

    /** Treat the points as a closed polygon and resampledCopy it. */
    public void resample(double sampleDist) {
        // replicate the first point at the end and resampledCopy to polygon
        points.add(points.get(0));
        KPolygon2D resampled = resampledCopy(sampleDist);
        points = resampled.points;

        // Now remove the last point if it is too doublePoints to the first point
        int nPoints = points.size();
        KPoint2D ptLast = points.get(nPoints - 1);
        KPoint2D ptFirst = points.get(0);
        if (KPoint2D.distBetween(ptLast, ptFirst) < sampleDist) {
            points.remove(nPoints-1);
        }
    }

    /** Clip the points to the outskirts of the image */
    public void clipToRect(int xmin, int ymin, int width, int height) {
        int xmax = xmin+width;
        int ymax = ymin+height;
        List<KPoint2D> oldSnake = points;
        points = new ArrayList<KPoint2D>(oldSnake.size());
        for (KPoint2D pt : oldSnake) {
            if (pt.x >= xmin && pt.x < xmax && pt.y >= ymin && pt.y < ymax) {
                points.add(pt);
            }
        }
    }

    public void removeDuplicates(double minDistance) {
        // replicate the first point at the end
        points.add(points.get(0));
        KPolygon2D poly = noDuplicatesCopy(minDistance);
        points = poly.points;
        // Now remove the last point if it is too doublePoints to the first point
        int nPoints = points.size();
        KPoint2D ptLast = points.get(nPoints - 1);
        KPoint2D ptFirst = points.get(0);
        if (KPoint2D.distBetween(ptLast, ptFirst) < minDistance) {
            points.remove(nPoints-1);
        }
    }

    public ByteProcessor toMask(int width, int height, boolean antialiased) {
        // Draw the filled version
        BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY);
        Graphics2D g = bi.createGraphics();

        if (antialiased) {
            g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        }
        g.setBackground(Color.BLACK);
        g.clearRect(0, 0, width, height);
        g.setPaint(Color.WHITE);
        g.fill(toGeneralPath());
        g.dispose();

        return new ByteProcessor(bi);
    }

    public Snake offset(KPoint2D offset) {
        KPolygon2D poly = offsetCopy(offset);
        return new Snake(poly.points);
    }

    /**
     *  Finds the CAG representation of snake1 - snake2.
     * @param snake1
     * @param snake2
     * @return
     */
    public List<Snake> subtract(Snake snake2) {
        Area diff = toClosedArea();
        diff.subtract(snake2.toClosedArea());

        List<KPolygon2D> polys = KPolygon2D.toKPolygons(diff.getPathIterator(null, 0.1), 0.1);
        List<Snake> snakes = new ArrayList<Snake>(polys.size());
        for (KPolygon2D p : polys) {
            snakes.add(new Snake(p.points));
        }

        return snakes;
    }
}
