/*
 * KPolygon2D.java
 *
 * Created on May 4, 2005, 5:13 PM
 */
package kuhnlab.coordinates;

import java.awt.Shape;
import java.awt.geom.Area;
import java.awt.geom.FlatteningPathIterator;
import java.awt.geom.GeneralPath;
import java.awt.geom.PathIterator;
import java.util.*;
import kuhnlab.collection.Pair;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 *
 * @author drjrkuhn
 */
@JsonIgnoreProperties({"length", "counterClockwise", "clockwise"})
public class KPolygon2D {
    public List<KPoint2D> points;
    public boolean isClosed;

    protected KPolygon2D() {
        this.points = null;
        this.isClosed = false;
    }

    public KPolygon2D(int initialSize, boolean isClosed) {
        this.points = new ArrayList<KPoint2D>(initialSize);
        this.isClosed = isClosed;
    }

    public KPolygon2D(boolean isClosed) {
        this.points = new ArrayList<KPoint2D>();
        this.isClosed = isClosed;
    }

    /** Deep Copy constructor */
    public KPolygon2D(KPolygon2D src) {
        this(src.points.size(), src.isClosed);
        for (KPoint2D p : src.points) {
            points.add(new KPoint2D(p));
        }
    }

    /** Shallow Copy constructor */
    public KPolygon2D(Collection<KPoint2D> poly, boolean isClosed) {
        this.points = new ArrayList<KPoint2D>(poly.size());
        this.points.addAll(poly);
        this.isClosed = isClosed;
    }

    public static KPolygon2D wrap(List<KPoint2D> poly, boolean isClosed) {
        KPolygon2D newPoly = new KPolygon2D();
        newPoly.points = poly;
        newPoly.isClosed = isClosed;
        return newPoly;
    }

    public void add(KPoint2D p) {
        points.add(p);
    }
    
    public void reverse() {
        Collections.reverse(points);
    }
    
    public int numPoints() {
        return points.size();
    }

    /************************************************************************
     *** POLYGON UTILITIES                                                ***
     ************************************************************************/

    public KPolygon2D roundedCopy(double place) {
        KPolygon2D out = new KPolygon2D(points.size(), isClosed);
        for (KPoint2D p : points) {
            KPoint2D p2 = (KPoint2D)p.clone();
            p2.round(place);
            out.points.add(p2);
        }
        return out;
    }

    @Deprecated
    public static List<KPoint2D> roundPoints(List<KPoint2D> lIn, double place) {
        return wrap(lIn, false).roundedCopy(place).points;
    }

    public double getLength() {
        final int iLast = points.size() - 1;
        double length = getLength(0, iLast);
        if (isClosed) {
            length += points.get(iLast).distTo(points.get(0));
        }
        return length;
    }

    public double getLength(int iFirst, int iLast) {
        final int s = points.size();
        if (iFirst < 0 || iFirst >= s || iLast < 0 || iLast >= s || iLast < iFirst+1)
            return Double.NaN;
        double dLength = 0;
        KPoint2D p1 = points.get(iFirst), p2;
        for (int i=iFirst+1; i <= iLast; i++) {
            p2 = points.get(i);
            dLength += p1.distTo(p2);
            p1 = p2;
        }
        return dLength;

    }

    @Deprecated
    public static double getLength(List<KPoint2D> lIn) {
        return wrap(lIn, false).getLength();
    }

    @Deprecated
    public static double getLength(List<KPoint2D> lIn, int iFirst, int iLast) {
        return wrap(lIn, false).getLength(iFirst, iLast);
    }

    public double getScaledLength(double xScale, double yScale) {
        double dLength = 0;
        final int size = points.size();
        if (size > 1) {
            KPoint2D p1 = (new KPoint2D()).toProdOf(xScale, yScale, points.get(0)), p2;
            for (int i = 1; i < size; i++) {
                p2 = (new KPoint2D()).toProdOf(xScale, yScale, points.get(i));
                dLength += p1.distTo(p2);
                p1 = p2;
            }
        }
        return dLength;
    }

    @Deprecated
    public static double getScaledLength(List<KPoint2D> lIn, double xScale, double yScale) {
        return wrap(lIn, false).getScaledLength(xScale, yScale);
    }

    public KPolygon2D retracedAndClosedCopy(int extraEndPoints) {
        int c, N = points.size();
        KPolygon2D newPoly = new KPolygon2D(N + 2*extraEndPoints, true);
        KPoint2D pFirst = points.get(0);
        KPoint2D pLast = points.get(N-1);

        newPoly.points.addAll(points.subList(0, N-1));
        // Duplicate the last point
        c = extraEndPoints;
        while (c>0) {
            newPoly.points.add(new KPoint2D(pLast));
            c--;
        }

        // add a copy of the reversed line
        List<KPoint2D> reversed = points.subList(1,N);
        Collections.reverse(reversed);
        newPoly.points.addAll(reversed);
        c = extraEndPoints;
        while (c>0) {
            newPoly.points.add(new KPoint2D(pFirst));
            c--;
        }
        return newPoly;
    }

    @Deprecated
    /** convert the open polygon to a closed polygon (with endpoint replication) */
    static public List<KPoint2D> retraceAndClose(List<KPoint2D> lIn, int extraEndPoints) {
        return wrap(lIn, false).retracedAndClosedCopy(extraEndPoints).points;
    }
    
    public KPolygon2D openedHalfCopy() {
        int N = points.size();
        KPolygon2D newPoly = new KPolygon2D(points.subList(0, N/2), false);
        return newPoly;
    }
    

    public KPolygon2D offsetCopy(KPoint2D offset) {
        KPolygon2D newPoly = new KPolygon2D(points.size(), isClosed);
        for (KPoint2D p : points) {
            newPoly.points.add(KPoint2D.newSumOf(p, offset));
        }
        return newPoly;
    }

    @Deprecated
    static public List<KPoint2D> offset(List<KPoint2D> lIn, KPoint2D offset) {
        return wrap(lIn, false).offsetCopy(offset).points;
    }

    public KPolygon2D noDuplicatesCopy(double minDistance) {
        KPolygon2D newPoly = new KPolygon2D(isClosed);
        Iterator<KPoint2D> t = points.iterator();
        if (!t.hasNext()) {
            return null;
        }
        KPoint2D p1 = t.next(), p2;
        newPoly.points.add(p1);
        boolean bSkip;
        while (t.hasNext()) {
            p2 = t.next();
            bSkip = KPoint2D.distBetween(p1, p2) < minDistance;
            while (bSkip && t.hasNext()) {
                p2 = t.next();
                bSkip = KPoint2D.distBetween(p1, p2) < minDistance;
            }
            if (!bSkip) {
                newPoly.points.add(p2);
            }
            p1 = p2;
        }
        return newPoly;
    }

    /** remove duplicate points (within minDistance from each other) from
     *  a polygon. */
    @Deprecated
    static public List<KPoint2D> removeDuplicates(List<KPoint2D> lIn, double minDistance) {
        return wrap(lIn, false).noDuplicatesCopy(minDistance).points;
    }

    /** copy points from a polygon. */
    @Deprecated
    static public List<KPoint2D> copyPoints(List<KPoint2D> lIn) {
        List<KPoint2D> lOut = new ArrayList<KPoint2D>();
        for (KPoint2D p : lIn) {
            lOut.add(new KPoint2D(p));
        }
        return lOut;
    }

    /** Holds result of a SearchForward or SearchBackward */
    static public class SearchResult {

        public KPoint2D nextPoint;
        public int nextIndex;
        public double distFromPrev;
        public double distToNext;

        public SearchResult() {
            nextPoint = null;
            nextIndex = 0;
            distFromPrev = 0;
            distToNext = 0;
        }

        @Override
        public String toString() {
            return "{nextPoint=" + nextPoint + ",nextIndex=" + nextIndex + ",distFromPrev=" + distFromPrev + ",distToNext=" + distToNext + "}";
        }
    }

    public SearchResult searchForward(int iStartIndex, double dSearchDistance) {
        // returns lpSrc.size() in siNextOUT if we go beyond the end
        int nPoints = points.size();
        SearchResult result = new SearchResult();
        if (iStartIndex < 0) {
            result.nextIndex = -1;
            return result;
        }
        if (iStartIndex >= nPoints) {
            result.nextIndex = nPoints;
            return result;
        }

        KPoint2D pPrev, pNext;
        int iPrev, iNext;
        double dSegLength, dRemainingLength, dAlpha;
        iPrev = iStartIndex;
        iNext = iPrev + 1;
        pPrev = points.get(iPrev);
        pNext = pPrev;
        result.nextIndex = iStartIndex;
        result.nextPoint = new KPoint2D(pNext);
        if (dSearchDistance <= 0) {
            return result;
        }
        dRemainingLength = dSearchDistance;
        result.distToNext = -dSearchDistance;
        while (iNext < nPoints) {
            // keep searching further out from the central point until we
            // have gone the minimum distance
            pNext = points.get(iNext);
            dSegLength = KPoint2D.distBetween(pPrev, pNext);
            result.distFromPrev = dSegLength;
            result.distToNext = dSegLength - dRemainingLength;
            if (dSegLength > dRemainingLength) {
                // the final point is somewhere between pPrev and pNext
                dAlpha = dRemainingLength / dSegLength;
                pNext = new KPoint2D().toInterpolateBetween(dAlpha, pPrev, pNext);
                result.distFromPrev = dRemainingLength;
                break;
            }
            // keep searching
            dRemainingLength -= dSegLength;
            pPrev = pNext;
            iPrev = iNext;
            iNext++;
        }
        result.nextIndex = iNext;
        result.nextPoint.set(pNext);
        return result;
    }

    @Deprecated
    static public SearchResult searchForward(List<KPoint2D> lpSrc, int iStartIndex, double dSearchDistance) {
        return wrap(lpSrc, false).searchForward(iStartIndex, dSearchDistance);
    }

    public SearchResult searchBackward(int iStartIndex, double dSearchDistance) {
        // returns -1 in siNextOUT if we go beyond the end
        int nPoints = points.size();
        SearchResult result = new SearchResult();
        if (iStartIndex < 0) {
            result.nextIndex = -1;
            return result;
        }

        if (iStartIndex >= nPoints) {
            result.nextIndex = nPoints;
            return result;
        }

        KPoint2D pPrev, pNext;
        int iPrev, iNext;
        double dSegLength, dRemainingLength, dAlpha;

        iPrev = iStartIndex;
        iNext = iPrev - 1;
        pPrev = points.get(iPrev);
        pNext = pPrev;
        result.nextIndex = iStartIndex;
        result.nextPoint = new KPoint2D(pNext);
        if (dSearchDistance <= 0) {
            return result;
        }
        dRemainingLength = dSearchDistance;
        result.distToNext = -dSearchDistance;
        while (iNext >= 0 && dRemainingLength > 0) {
            // keep searching further out from the central point until we
            // have gone the minimum distance
            pNext = points.get(iNext);
            dSegLength = KPoint2D.distBetween(pPrev, pNext);
            result.distFromPrev = dSegLength;
            result.distToNext = dSegLength - dRemainingLength;
            if (dSegLength > dRemainingLength) {
                // the final point is somewhere between pPrev and pNext
                dAlpha = dRemainingLength / dSegLength;
                pNext = new KPoint2D().toInterpolateBetween(dAlpha, pPrev, pNext);
                result.distFromPrev = dRemainingLength;
                break;
            }
            // keep searching
            dRemainingLength -= dSegLength;
            pPrev = pNext;
            iPrev = iNext;
            iNext--;
        }
        result.nextIndex = iNext;
        result.nextPoint.set(pNext);
        return result;
    }

    @Deprecated
    static public SearchResult searchBackward(List<KPoint2D> lpSrc, int iStartIndex, double dSearchDistance) {
        return wrap(lpSrc, false).searchBackward(iStartIndex, dSearchDistance);
    }

    public KPolygon2D resampledCopy(double dSampleDist) {
        KPolygon2D newPoly = new KPolygon2D(isClosed);
        int nPoints = points.size();    // number of points in the source vector
        newPoly.points.add(new KPoint2D((KPoint2D) points.get(0)));
        SearchResult sr = searchForward(0, dSampleDist);
        newPoly.points.add(sr.nextPoint);
        while (sr.nextIndex < nPoints) {
            sr = searchForward(sr.nextIndex - 1, dSampleDist + sr.distFromPrev);
            newPoly.points.add(sr.nextPoint);
        }

        return newPoly;

    }

    @Deprecated
    static public List<KPoint2D> resample(List<KPoint2D> lpSrc, double dSampleDist) {
        return wrap(lpSrc, false).resampledCopy(dSampleDist).points;
    }

    public KPolygon2D cutLeftCopy(double dDistance) {
        KPolygon2D newPoly = new KPolygon2D(false);
        int nPoints = points.size();
        SearchResult sr = searchForward(0, dDistance);
        if (sr.nextIndex - 1 > 0 || sr.distFromPrev > 0) {
            // a left half exists
            newPoly.points.addAll(points.subList(0, sr.nextIndex));
            if (sr.nextIndex < nPoints && sr.distFromPrev > 0) {
                newPoly.points.add(sr.nextPoint);
            }
        }
        return newPoly;
    }

    @Deprecated
    static public List<KPoint2D> cutLeft(List<KPoint2D> lpSrc, double dDistance) {
        return wrap(lpSrc, false).cutLeftCopy(dDistance).points;
    }

    public KPolygon2D cutRightCopy(double dDistance) {
        KPolygon2D newPoly = new KPolygon2D(false);
        int nPoints = points.size();
        SearchResult sr = searchForward(0, dDistance);
        if (sr.nextIndex < nPoints) {
            // a right half exists
            if (sr.distToNext > 0) {
                newPoly.points.add(sr.nextPoint);
            }
            newPoly.points.addAll(points.subList(sr.nextIndex, nPoints));
        }
        return newPoly;
    }

    @Deprecated
    static public List<KPoint2D> cutRight(List<KPoint2D> lpSrc, double dDistance) {
        return wrap(lpSrc, false).cutRightCopy(dDistance).points;
    }

    public double signedPolygonArea() {
        int i, len = points.size();
        double dSum = 0;
        KPoint2D pi, pj;
        for (i = 0; i < len; i++) {
            pi = points.get(i);
            pj = points.get((i + 1) % len);
            dSum += pi.x * pj.y - pj.x * pi.y;
        }
        return dSum / 2;
    }

    /** Computes the signed area of a closed polygon defined by a point list. */
    @Deprecated
    static public double signedPolygonArea(List<KPoint2D> aPoly) {
        return wrap(aPoly, true).signedPolygonArea();
    }

    public double polygonArea() {
        return Math.abs(signedPolygonArea());
    }

    /** Computes the area of a closed polygon defined by a point list. */
    @Deprecated
    static public double polygonArea(List<KPoint2D> aPoly) {
        return Math.abs(signedPolygonArea(aPoly));
    }

    public boolean isCounterClockwise() {
        return signedPolygonArea() > 0;
    }

    /** determine if a closed polygon is counterclockwise. */
    @Deprecated
    static public boolean isCounterClockwise(List<KPoint2D> aPoly) {
        return (signedPolygonArea(aPoly) > 0);
    }

    public boolean isClockwise() {
        return signedPolygonArea() < 0;
    }

    /** determine if a closed polygon is clockwise. */
    @Deprecated
    static public boolean isClockwise(List<KPoint2D> aPoly) {
        return (signedPolygonArea(aPoly) < 0);
    }

    public KPoint2D centroid() {
        int i, j, n = points.size();
        double ai, atmp = 0, xtmp = 0, ytmp = 0;
        if (n < 3) {
            return null;
        }
        KPoint2D pi, pj;
        for (i = n-1  , j=0; j < n; i=j, j++) {
            pi = points.get(i);
            pj = points.get(j);
            ai = pi.x * pj.y - pj.x * pi.y;
            atmp += ai;
            xtmp += (pj.x + pi.x) * ai;
            ytmp += (pj.y + pi.y) * ai;
        }
        if (atmp != 0) {
            return new KPoint2D(xtmp / (3 * atmp), ytmp / (3 * atmp));
        }
        return null;
    }

    /** determine the centroid of a polygon. From "Graphics Gems IV". */
    @Deprecated
    static public KPoint2D centroid(List<KPoint2D> aPoly) {
        return wrap(aPoly, true).centroid();
    }

    public boolean pointInPolygon(double x, double y) {
        int i, j, n = points.size();
        KPoint2D pi, pj;
        boolean c = false;
        for (i=0, j=n-1; i<n; j = i++) {
            pi = points.get(i);
            pj = points.get(j);
            if (( ((pi.y<=y) && (y<pj.y)) || ((pj.y<=y) && (y<pi.y))
                ) && (
                  x < (pj.x - pi.x) * (y - pi.y) / (pj.y - pi.y) + pi.x
                )) {
                    c = !c;
            }

        }
        return c;
    }

    /** determine if a point (x,y) is within the polygon. From "Graphics Gems IV". */
    @Deprecated
    static public boolean pointInPolygon(List<KPoint2D> aPoly, double x, double y) {
        return wrap(aPoly, true).pointInPolygon(x, y);
    }

    public boolean containsVertex(double x, double y, double epsilon) {
        KPoint2D testPt = new KPoint2D(x, y);
        for (KPoint2D p : points) {
            if (p.epsilonEquals(testPt, epsilon)) {
                return true;
            }
        }
        return false;
    }

    @Deprecated
    static public boolean containsVertex(List<KPoint2D> aPoly, double x, double y, double epsilon) {
        return wrap(aPoly, false).containsVertex(x, y, epsilon);
    }

    public void unwind() {
        int i,  iNext,  j,  jNext,  len = points.size();
        KPoint2D a,b ,c ,d ;
        // check each line segment
        for (i = 0; i < len - 1; i++) {
            iNext = i + 1;
            a = points.get(i);
            b = points.get(iNext);
            // look for intersections with other line segments
            for (j = iNext; j < len; j++) {
                jNext = (j + 1) % len;
                c = points.get(j);
                d = points.get(jNext);
                if (KPoint2D.intersects(a, b, c, d)) {
                    // there is a self crossing. Unwind it
                    Collections.reverse(points.subList(iNext, j + 1));
                }
            }
        }
    }

    /** Unwind a polygon (remove self-crossings) */
    @Deprecated
    static public void unwind(List<KPoint2D> src) {
        wrap(src, true).unwind();
    }

    public KPolygon2D fourierDescriptors(boolean bForward) {
        int i,  j,  ij,  n = points.size();
        KPolygon2D newPoly = new KPolygon2D(n, true);

        // Euler's identity: e^(i*x) = cos(x) + i*sin(x);
        // Complex multiply:
        //	(Ar + i Ai) (Br + i Bi) = (ArBr - AiBi) + i (ArBi + AiBr)

        double dRe,  dIm,  dSumRe,  dSumIm,  dExpRe,  dExpIm,  dMult;
        KPoint2D pIn;
        dMult = 2.0 * Math.PI / n;
        if (bForward) {
            dMult = -dMult;
        }
        for (j = 0; j < n; j++) {
            dSumRe = dSumIm = 0;
            for (i = 0; i < n; i++) {
                ij = i * j;
                pIn = points.get(i);
                dRe = pIn.x;
                dIm = pIn.y;
                dExpRe = Math.cos(dMult * ij);
                dExpIm = Math.sin(dMult * ij);
                dSumRe += dRe * dExpRe - dIm * dExpIm;
                dSumIm += dRe * dExpIm + dIm * dExpRe;
            }
            if (bForward) {
                dSumRe /= n;
                dSumIm /= n;
            }
            newPoly.points.add(new KPoint2D(dSumRe, dSumIm));
        }
        return newPoly;
    }

    /** compute the forward or inverse Fourier transform of a polygon.
     *  
     * Some information that can be measure from the Fourier Descriptors:
     * <p> 
     * Coefficient z[0] is the x,y position of the center.
     * Coefficients z[-1],z[+1] define the fit ellipse of the pattern.
     * The length of the minor axis of the ellipse is 2(mag(z[+1]) - mag(z[-1])).
     * The length of the major axis of the ellipse is 2(mag(z[+1]) + mag(z[-1])).
     * (The APPROXIMATE angle of the minor axis is arg(z[+1]) - arg(z[-1]) TODO: CHECK THIS MATHEMATICALLY).
     */
    @Deprecated
    static public List<KPoint2D> fourierDescriptors(List<KPoint2D> lIn, boolean bForward) {
        return wrap(lIn, true).fourierDescriptors(bForward).points;
    }

    public KPolygon2D fourierDownsampledCopy(double dFraction) {
        return fourierDownsampledCopy((int)Math.round(points.size()*dFraction/2));
    }

    public KPolygon2D fourierDownsampledCopy(int nSamplePairs) {
        int nOld = points.size();
        if (nSamplePairs > nOld/2)
            nSamplePairs = nOld/2;
        int nNew = 2*nSamplePairs + 1;  // insure an odd number of samples

        // create the fourier descriptors of the closed polygon
        KPolygon2D ftPoly = this.fourierDescriptors(true);
        KPoint2D[] aDftDown = new KPoint2D[nNew];

        // copy the first point (=the x,y offset of the center)
        aDftDown[0] = ftPoly.points.get(0);

        for (int i = 1; i <= nSamplePairs; i++) {
            // copy the lower half
            aDftDown[i] = ftPoly.points.get(i);
            // copy the upper half
            aDftDown[nNew - i] = ftPoly.points.get(nOld - i);
        }
        // take the inverse transform
        KPolygon2D dftDownPoly = wrap(Arrays.asList(aDftDown), true);
        return dftDownPoly.fourierDescriptors(false);
    }

    /** smooth a polygon by removing high-frequency information from the
     *	Fourier transform of the polygon (its fourier descriptors). */
    @Deprecated
    static public List<KPoint2D> fourierDownsample(List<KPoint2D> src, double dFraction) {
        return wrap(src, true).fourierDownsampledCopy(dFraction).points;
    }

    public KPolygon2D fourierSmoothedCopy(double dFraction, double dScale, boolean keepCenter) {
        return fourierSmoothedCopy((int)Math.round(points.size()*dFraction/2), dScale, keepCenter);
    }

    public KPolygon2D fourierSmoothedCopy(int nSamplePairs, double dScale, boolean keepCenter) {
        int nOld = points.size();
        int nNew = nOld/2 + 1;  // insure an odd number of samples
        if (nSamplePairs > nNew/2)
            nSamplePairs = nNew/2;

        // create the fourier descriptors of the closed polygon
        KPolygon2D ftPoly = this.fourierDescriptors(true);
        KPoint2D[] aDftDown = new KPoint2D[nNew];

        // set all the points to zero
        for (int i=0; i<nNew; i++) {
            aDftDown[i] = KPoint2D.ZERO;
        }

        if (keepCenter) {
            // The first point is the x,y offset of the center
            // do not scale it
            aDftDown[0] = ftPoly.points.get(0);
        }

        for (int i = 1; i <= nSamplePairs; i++) {
            // copy the lower half
            aDftDown[i] = ftPoly.points.get(i).scale(dScale);
            // copy the upper half
            aDftDown[nNew - i] = ftPoly.points.get(nOld - i).scale(dScale);
        }
        // take the inverse transform
        KPolygon2D dftDownPoly = wrap(Arrays.asList(aDftDown), true);
        return dftDownPoly.fourierDescriptors(false);
    }

    public KPoint2D getDirection(int startIndex, double searchRadius) {
        int nPoints = points.size();
        KPoint2D pLeft, pRight;
        SearchResult sr;

        if (startIndex > 0) {
            sr = this.searchBackward(startIndex, searchRadius);
            pLeft = sr.nextPoint;
        } else {
            pLeft = points.get(0);
        }

        if (startIndex < nPoints - 1) {
            sr = this.searchForward(startIndex, searchRadius);
            pRight = sr.nextPoint;
        } else {
            pRight = points.get(nPoints - 1);
        }

        KPoint2D vDir = KPoint2D.newDiffOf(pRight, pLeft);
        vDir.norm();
        return vDir;
    }

    @Deprecated
    static public KPoint2D getDirection(List<KPoint2D> lSrc, int startIndex, double searchRadius) {
        return wrap(lSrc, false).getDirection(startIndex, searchRadius);
    }


    public Pair<KPolygon2D, KPolygon2D> split(KPoint2D splitA, KPoint2D splitB) {
        Iterator<KPoint2D> it = points.iterator();
        if (!it.hasNext()) {
            return null;
        }
        KPoint2D p1, p2, psplit = new KPoint2D();
        p1 = it.next();
        double splitDistance = 0;
        while (it.hasNext()) {
            p2 = it.next();
            psplit.toIntersectionOf(p1, p2, splitA, splitB);
            if (!psplit.isNaN()) {
                splitDistance += p1.distTo(psplit);
                return new Pair<KPolygon2D,KPolygon2D>(this.cutLeftCopy(splitDistance),
                        this.cutRightCopy(splitDistance));
            }
            splitDistance += p1.distTo(p2);
            p1 = p2;
        }
        // no split was found
        return null;
    }


    /** Split a polygon into two polygons by cutting with a simple, two point
     * line. If no split was found, returns null. */
    @Deprecated
    static public Pair<List<KPoint2D>, List<KPoint2D>> split(List<KPoint2D> lsrc, KPoint2D splitA, KPoint2D splitB) {
        Pair<KPolygon2D, KPolygon2D> res = wrap(lsrc, false).split(splitA, splitB);
        if (res != null) {
            if (res.left != null && res.right != null) {
                return new Pair<List<KPoint2D>, List<KPoint2D>>(res.left.points, res.right.points);
            }
        }
        return null;
    }

    public GeneralPath toGeneralPath() {
        return toGeneralPath(KPoint2D.ZERO);
    }
    
    public GeneralPath toGeneralPath(KPoint2D ptOff) {
        int nPoints = points.size();
        if (nPoints == 0) {
            return null;
        }
        GeneralPath gp = new GeneralPath(GeneralPath.WIND_NON_ZERO, nPoints);
        KPoint2D p = points.get(0);
        gp.moveTo((float) p.x + ptOff.x, (float) p.y + ptOff.y);
        for (int i = 1; i < nPoints; i++) {
            p = points.get(i);
            gp.lineTo((float) p.x + ptOff.x, (float) p.y + ptOff.y);
        }
        if (isClosed) {
            gp.closePath();
        }
        return gp;
    }

    public List<Shape> toVertices(double radius) {
        List<Shape> vertices = new ArrayList<Shape>(points.size());
        for (KPoint2D p : points) {
            vertices.add(p.toRectangleShape(radius, radius));
        }
        return vertices;
    }

    /** Convert a polygon into a java.awt.geom.GeneralPath for display */
    @Deprecated
    static public GeneralPath toGeneralPath(List<KPoint2D> lpSrc, boolean bClosed) {
        return wrap(lpSrc, bClosed).toGeneralPath();
    }

    public Area toClosedArea() {
        return new Area(toGeneralPath());
    }

    @Deprecated
    static public Area toClosedArea(List<KPoint2D> lpSrc) {
        return new Area(toGeneralPath(lpSrc, true));
    }

    static public List<KPolygon2D> toKPolygons(PathIterator pi, double flatness) {
        List<KPolygon2D> polys = new ArrayList<KPolygon2D>(1);

        double[] coords = new double[6];
        int segType;
        FlatteningPathIterator fpi = new FlatteningPathIterator(pi, flatness);
        KPolygon2D currentPoly = null;

        while (!fpi.isDone()) {
            segType = fpi.currentSegment(coords);
            if (segType == PathIterator.SEG_MOVETO) {
                if (currentPoly != null) {
                    // we have started a new polygon. Store the previous one
                    polys.add(currentPoly);
                }
                currentPoly = new KPolygon2D(false);
                currentPoly.points.add(new KPoint2D(coords[0], coords[1]));
            } else if (segType == PathIterator.SEG_LINETO) {
                if (currentPoly == null) {
                    currentPoly = new KPolygon2D(false);
                }
                currentPoly.points.add(new KPoint2D(coords[0], coords[1]));
            } else if (segType == PathIterator.SEG_CLOSE) {
                if (currentPoly != null) {
                    currentPoly.isClosed = true;
                }
            }
            fpi.next();
        }
        if (!polys.contains(currentPoly)) {
            polys.add(currentPoly);
        }

        return polys;
    }

    public KPoint2D nearestPoint(KPoint2D ptC) {
        int n = points.size();
        if (n < 2)
            return null;
        double dist, minDist = Double.MAX_VALUE;
        KPoint2D ptNearest = new KPoint2D();
        KPoint2D ptBestNearest = new KPoint2D();
        KPoint2D ptA, ptB;
        int nLast = isClosed ? n : n-1;
        ptA = points.get(0);
        for (int i=1; i<=nLast; i++) {
            ptB = (i<n) ? points.get(i) : points.get(0);
            ptNearest.toClosestPoint(ptA, ptB, ptC);
            dist = ptC.distSqTo(ptNearest);
            if (minDist > dist) {
                minDist = dist;
                ptBestNearest.set(ptNearest);
            } else {
                dist = 0;
            }
            ptA = ptB;
        }
        return ptBestNearest;
    }

}
