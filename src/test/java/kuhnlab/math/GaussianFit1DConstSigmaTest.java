/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package kuhnlab.math;

import com.nr.util.COStream;
import java.util.Arrays;
import junit.framework.TestCase;

/**
 *
 * @author jrkuhn
 */
public class GaussianFit1DConstSigmaTest extends TestCase {
    
    public GaussianFit1DConstSigmaTest(String testName) {
        super(testName);
    }

    public void testFitter() {
        System.out.println("findFit");
        COStream cout = new COStream(System.out);
        
        double fx[] = {-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5};
        double fy[] = {27.960765657176072, 0.0, 1.3978744899942424, 
            37.91489043175977, 88.83576186484282, 161.38223266601562, 
            186.04854851322924, 119.44884906754591, 39.53739327907091, 
            1.893653447108413, 0.0};
        double fsd[] = new double[fx.length];
        Arrays.fill(fsd, 1.0);
        
        GaussianFit1DConstSigma fitter = new GaussianFit1DConstSigma();

        int NCOEF = fitter.getNumCoef();
        double coefGuess[] = new double[NCOEF];
        double coef[] = null;
        double fitErr[] = new double[NCOEF];
        
        fitter.setSigma(1.5);
        coefGuess[GaussianFit1DConstSigma.AMPLITUDE] = 4*22;
        coefGuess[GaussianFit1DConstSigma.CENTER] = 0;
        coefGuess[GaussianFit1DConstSigma.OFFSET] = 22;


        cout . setw(6) . print("x") . setw(9) . print("g(x)");
        cout . setw(11) . print("graph:") . endl() . endl();
        cout . fixed() . setprecision(2);
        int i, N=fx.length;
        for (i=0; i<N; i++) {
            int gy = (int)Math.round(fy[i]);
            String txt = COStream.paddedString(gy, '*');
            cout . setw(6) . print(fx[i]) . setw(9) . print(fy[i]) . print(" ") . print(txt) . endl();
        }
        cout . endl();
        
        coef = fitter.findFit(fx, fy, fsd, coefGuess, fitErr);
        
        cout.print("Results:").endl();
        for (i = 0; i < NCOEF; i++) {
            cout.setw(9).print(fitter.getCoefName(i));
        }
        cout.endl();
        cout.fixed().setprecision(5);
        for (i = 0; i < NCOEF; i++) {
            cout.setw(9).print(coef[i]);
        }
        cout.endl();
        cout.print("Uncertainties:").endl();
        for (i = 0; i < NCOEF; i++) {
            cout.setw(9).print(Math.sqrt(fitErr[i]));
        }
        cout.endl().print("Expected results:").endl();
        for (i = 0; i < NCOEF; i++) {
            cout.setw(9).print(fitter.getCoefName(i));
        }
        cout.endl();
        cout.fixed().setprecision(5);
        for (i = 0; i < NCOEF; i++) {
            cout.setw(9).print(coefGuess[i]);
        }
        
        
        
        
    }
}
