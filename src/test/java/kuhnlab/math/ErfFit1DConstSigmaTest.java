/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package kuhnlab.math;

import com.nr.util.COStream;
import junit.framework.TestCase;
import kuhnlab.estimate.EstimateFunction.Estimate;

/**
 *
 * @author jrkuhn
 */
public class ErfFit1DConstSigmaTest extends TestCase {
    
    public ErfFit1DConstSigmaTest(String testName) {
        super(testName);
    }
    
    /**
     * Test of findGaussFit method, of class ErfFit1DConstSigma.
     */
    public void testFindFit() {
        System.out.println("findFit");
        COStream cout = new COStream(System.out);
        int N = 20, NCOEF = ErfFit1DConstSigma.NUM_COEF;
        
        double[] sx = new double[N];
        double[] sy = new double[N];
        double[] ssd = new double[N];
        double[] coefReal = new double[NCOEF];
        double[] coefGuess = new double[NCOEF];
        double[] fitErr = new double[NCOEF];
        
        final double amp = -10, off = 15, xoff = 10, sigma = 2, noise=0.5;
        int i;
        
        ErfFit1DConstSigma fitter = new ErfFit1DConstSigma();
        fitter.setSigma(sigma);

        coefReal[ErfFit1DConstSigma.AMPLITUDE] = amp;
        coefReal[ErfFit1DConstSigma.CENTER] = xoff;
        coefReal[ErfFit1DConstSigma.OFFSET] = off;
        
        coefGuess[ErfFit1DConstSigma.AMPLITUDE] = -1;
        coefGuess[ErfFit1DConstSigma.CENTER] = N/2;
        coefGuess[ErfFit1DConstSigma.OFFSET] = 0;

        for (i=0; i<N; i++) {
            double[] xpt = {i};
            sx[i] = xpt[0];
            Estimate est = fitter.getEstimate(xpt, coefReal);
            sy[i] = est.estimate + Math.random()*noise;
            ssd[i] = 1.0;
        }
        

        cout . setw(6) . print("x") . setw(9) . print("g(x)");
        cout . setw(11) . print("graph:") . endl() . endl();
        cout . fixed() . setprecision(2);
        for (i=0; i<N; i++) {
            int gx = (int)Math.round(sy[i]);
            String txt = COStream.paddedString(gx, '*');
            cout . setw(6) . print(sx[i]) . setw(9) . print(sy[i]) . print(" ") . print(txt) . endl();
        }
        cout . endl();
        
        
        double[] coef = fitter.findFit(sx, sy, ssd, coefGuess, fitErr);
        
        cout.print("Results:").endl();
        for (i = 0; i < NCOEF; i++) {
            cout.setw(9).print(fitter.getCoefName(i));
        }
        cout.endl();
        cout.fixed().setprecision(5);
        for (i = 0; i < NCOEF; i++) {
            cout.setw(9).print(coef[i]);
        }
        cout.endl();
        cout.print("Uncertainties:").endl();
        for (i = 0; i < NCOEF; i++) {
            cout.setw(9).print(Math.sqrt(fitErr[i]));
        }
        cout.endl().print("Expected results:").endl();
        for (i = 0; i < NCOEF; i++) {
            cout.setw(9).print(fitter.getCoefName(i));
        }
        cout.endl();
        cout.fixed().setprecision(5);
        for (i = 0; i < NCOEF; i++) {
            cout.setw(9).print(coefReal[i]);
        }
        
        for (i = 0; i < NCOEF; i++) {
            assertEquals(coefReal[i], coef[i], 1.5*fitErr[i]);
        }
    }
}
